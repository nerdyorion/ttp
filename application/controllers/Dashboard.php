<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct()
    {
            parent::__construct();
            if(!is_logged_in())  // check if logged in
            {
                redirect('/login');
            }
    }

    public function index()
    {
        go_to_dashboard();
    }
}
