<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// use \Firebase\JWT\JWT;

class Home extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Portfolio_model');
        $this->load->model('Service_model');
        $this->load->model('User_model');
    }

    public function index()
    {
        $header['page_title'] = 'Home';

        // $data['mobile_contents'] = $this->Product_model->getRowsFrontEnd(8, 0, 1); // fetching 8 records for homepage
        // $data['music_crbt'] = $this->Product_model->getRowsFrontEnd(8, 0, 2); // fetching 8 records for homepage
        $data['featured'] = $this->User_model->getFeaturedArtist();

        $this->load->view('header', $header);  // load header view
        // $this->load->view('menu');  // load menu view
        $this->load->view('home', $data);  // load content view
    }

    public function view($username)
    {
        // $data['row'] = $this->User_model->getRows(0, 0, $id);
        $data['row'] = $this->User_model->getUserFullProfile($username);

        if(empty($data['row']))
        {
            redirect("/");
        }

        $header['page_title'] = $data['row']['stage_name'];

        $data['username'] = $username;
        $data['portfolio'] = $this->Portfolio_model->getAllRowsByUserID($data['row']['id']);
        $data['services'] = $this->Service_model->getAllRowsByUserID($data['row']['id']);

        $this->load->view('header', $header);  // load header view
        // $this->load->view('menu');  // load menu view
        $this->load->view('artist-profile', $data);  // load content view
    }

    public function services($username)
    {
        // $data['row'] = $this->User_model->getRows(0, 0, $id);
        $data['row'] = $this->User_model->getUserFullProfile($username);

        if(empty($data['row']))
        {
            redirect("/");
        }

        $header['page_title'] = $data['row']['stage_name'];

        $data['username'] = $username;
        $data['services'] = $this->Service_model->getAllRowsByUserID($data['row']['id']);

        $this->load->view('header', $header);  // load header view
        // $this->load->view('menu');  // load menu view
        $this->load->view('artist-services', $data);  // load content view
    }

    public function forgot()
    {
        $header['page_title'] = 'Home';

        $this->form_validation->set_rules('email', 'Email', 'required|valid_email'); 

        if($this->form_validation->run() == FALSE) {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);
        }
        else
        {
            $email = $this->input->post('email');  
            $clean = $this->security->xss_clean($email);
            $userInfo = $this->user_model->getUserInfoByEmail($clean);

            if(!$userInfo){
                $this->session->set_flashdata('error', 'We cant find your email address');
                $this->session->set_flashdata('error_code', 1);
                redirect('login');
            }

            //build token 

            $token = $this->user_model->insertToken($userInfo->id);                        
            $qstring = $this->base64url_encode($token);                  
            $url = base_url() . 'reset_password/token/' . $qstring;
            $link = '<a href="' . $url . '">' . $url . '</a>'; 

            $message = '';                     
            $message .= '<strong>A password reset has been requested for this email account</strong><br>';
            $message .= '<strong>Please click:</strong> ' . $link;             

            echo $message; //send this through mail
            exit;

        }
        $this->load->view('header', $header);  // load header view
        // $this->load->view('menu');  // load menu view
        $this->load->view('home', $data);  // load content view

    }
}