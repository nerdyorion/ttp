<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct()
    {
            parent::__construct();
            if(!is_logged_in())  // check if logged in
            {
                //redirect to login
            }
            else
            {
                go_to_dashboard();
            }
            $this->load->model('User_model');
            // $this->load->helper('url_helper');
    }

    public function index()
    {
        $header['page_title'] = 'Login';

        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');

        $data['page_title'] = 'Login';  // set page title
        $this->load->view('header', $header);  // load header view
        $this->load->view('login', $data);  // load content view

        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'Email', 'required|max_length[255]|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required|max_length[8000]');
    }

    public function secure()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'Email', 'required|max_length[255]|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required|max_length[8000]');

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

            redirect('/login');
        }
        else
        {
            $data['error'] = $this->session->flashdata('error');
            $data['error_code'] = $this->session->flashdata('error_code');
            $data['page_title'] = 'Login';  // set page title
            $data['login'] = $this->User_model->login();  // login
            if(!$data['login'])
            {
                $errors = "Invalid Login!";
                $this->session->set_flashdata('error', $errors);
                $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

                redirect('/login');
            }
            else
            {
                $this->User_model->updateLastLogin($data['login']['id']);  //  update last login

                go_to_dashboard();
            }
        }
    }
}