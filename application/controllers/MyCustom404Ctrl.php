<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MyCustom404Ctrl extends CI_Controller {

    public function __construct()
    {
            parent::__construct();
    }
	public function index()
	{
        $this->output->set_status_header('404');

        $header['page_title'] = 'Page not found';
        $this->load->view('header', $header);  // load header view
		$this->load->view('404');
	}
}
