<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Analytics extends Admin_Controller {

    public function __construct()
    {
            parent::__construct();
            if(!is_logged_in())  // check if logged in
            {
                //redirect to login
                redirect('/admin123/login');
            }
            if(!is_super_admin())  // check if is super admin
            {
                //redirect to 404
                redirect('/admin123/404');
                //show_404();
            }
            $this->load->model('User_model');
            $this->load->model('Analytics_model');

            //$this->load->helper('url_helper');
    }

	public function index()
	{
     $data['error'] = $this->session->flashdata('error');
     $data['error_code'] = $this->session->flashdata('error_code');
     $header['page_title'] = 'analytics';
     $data['rows'] = $this->User_model->getRowsAnalytics();

     $this->load->view($this->config->item('template_dir_admin') . 'header', $header);
     $this->load->view($this->config->item('template_dir_admin') . 'menu');
     $this->load->view($this->config->item('template_dir_admin') . 'analytics', $data);  // load content view
	}

    public function add()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('count_lagos', 'Lagos', 'required');
        $this->form_validation->set_rules('count_north1', 'North 1', 'required');
        $this->form_validation->set_rules('count_north2', 'North 2', 'required');
        $this->form_validation->set_rules('count_south_east', 'South East', 'required');
        $this->form_validation->set_rules('count_south_south', 'South South', 'required');
        $this->form_validation->set_rules('count_south_west', 'South West', 'required');

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);
        }
        else
        {
            $this->Analytics_model->add();
            $this->session->set_flashdata('error', 'Record(s) added successfully.');
            $this->session->set_flashdata('error_code', 0);
        }

        redirect("/admin123/analytics");
    }

    public function edit($id)
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('count_lagos', 'Lagos', 'required');
        $this->form_validation->set_rules('count_north1', 'North 1', 'required');
        $this->form_validation->set_rules('count_north2', 'North 2', 'required');
        $this->form_validation->set_rules('count_south_east', 'South East', 'required');
        $this->form_validation->set_rules('count_south_south', 'South South', 'required');
        $this->form_validation->set_rules('count_south_west', 'South West', 'required');

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

            $data['error'] = $this->session->flashdata('error');
            $data['error_code'] = $this->session->flashdata('error_code');
            $header['page_title'] = 'Update RBT Analytics';  // set page title
            $data['row'] = $this->Analytics_model->getRows($id);
            if(empty($data['row']))
                redirect("/admin123/analytics");
            
            $this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
            $this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
            $this->load->view($this->config->item('template_dir_admin') . 'analytics-edit', $data);  // load content view
        }
        else
        {
            $this->Analytics_model->update($id);
            $this->session->set_flashdata('error_code', 0);
            $this->session->set_flashdata('error', "Record updated successfully!");
            
            redirect("/admin123/analytics");
        }
    }

    public function delete($id)
    {
        $data['row'] = $this->Analytics_model->delete($id);

        $this->session->set_flashdata('error_code', 0);
        $this->session->set_flashdata('error', "Record deleted successfully!");
        redirect('/admin123/analytics', 'refresh');
    }
}
