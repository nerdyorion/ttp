<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bookings extends Admin_Controller {

    public function __construct()
    {
            parent::__construct();
            if(!is_logged_in())  // check if logged in
            {
                //redirect to login
                redirect('/admin123/login');
            }
            if(!is_super_admin())  // check if is super admin
            {
                //redirect to 404
                redirect('/admin123/404');
                //show_404();
            }
            $this->load->model('User_model');
            $this->load->model('Booking_model');
            //$this->load->helper('url_helper');
    }

	public function index()
	{
     $data['error'] = $this->session->flashdata('error');
     $data['error_code'] = $this->session->flashdata('error_code');
     $header['page_title'] = 'Bookings';
     $data['rows'] = $this->Booking_model->getRowsAll(100, 0);

     $this->load->view($this->config->item('template_dir_admin') . 'header', $header);
     $this->load->view($this->config->item('template_dir_admin') . 'menu');
     $this->load->view($this->config->item('template_dir_admin') . 'bookings', $data);  // load content view
	}

    public function view($id = NULL)
    {
        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');

        $header['page_title'] = 'Booking Details';
        $data['row'] = $this->Booking_model->getRowsAll(0, 0, $id);

        $content_industry = $this->content_industry();
        $content_budget = $this->content_budget();
        $content_tier = $this->content_tier();
        $content_campaign_duration = $this->content_campaign_duration();

        $content_industry_id = getArrayKey($content_industry, 'id', trim($data['row']['content_industry_id']));
        $data['content_industry'] = isset($content_industry[$content_industry_id]) ? $content_industry[$content_industry_id]['value'] : '-';

        $content_budget_id = getArrayKey($content_budget, 'id', trim($data['row']['content_budget_id']));
        $data['content_budget'] = isset($content_budget[$content_budget_id]) ? $content_budget[$content_budget_id]['value'] : '-';

        $content_tier_id = getArrayKey($content_tier, 'id', trim($data['row']['content_tier_id']));
        $data['content_tier'] = isset($content_tier[$content_tier_id]) ? $content_tier[$content_tier_id]['value'] : '-';

        $content_campaign_duration_id = getArrayKey($content_campaign_duration, 'id', trim($data['row']['content_campaign_duration_id']));
        $data['content_campaign_duration'] = isset($content_campaign_duration[$content_campaign_duration_id]) ? $content_campaign_duration[$content_campaign_duration_id]['value'] : '-';

        $this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
        $this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
        $this->load->view($this->config->item('template_dir_admin') . 'booking-details', $data);  // load content view
    }

    public function updatePrice()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('booking_id', 'ID', 'required|max_length[255]');
        $this->form_validation->set_rules('booking_cost', 'Booking Price', 'required|max_length[255]');

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);
        }
        else
        {
            $booking_id = (int) trim($this->input->post('booking_id'));
            
            $this->Booking_model->updatePrice();
            $this->session->set_flashdata('error_code', 0);
            $this->session->set_flashdata('error', "Record updated successfully!");

            // get booking_details
            $booking_row = $this->Booking_model->getRowsAll(0, 0, $booking_id);


            // send notification to agent dashboard
            $notification_user_id = $booking_row['agent_id']; // for super admin and admin

            $notification_message = 'Event booking cost added. Please pay on time. [Price: N' . number_format($booking_row['booking_cost']) . ' | Event: ' . $booking_row['event_name'] . ' | Artist: <a href="' . base_url() . '@' . $booking_row['username'] . '">' . $booking_row['stage_name'] . '</a>]';
            add_notification($notification_user_id, $notification_message);
            // -----------------------------------------------------------------

            // send booking price added mail to agent
            $to = $booking_row['agent_email'];
            $to_name = $booking_row['full_name'];
            $subject = "Make Payment for Booking";

            $event_details = "<b>Event Name</b>: " . $booking_row['event_name'] . " <br />";
            $event_details .= '<b>Artist</b>: <a href="' . base_url() . '@' . $booking_row['username'] . '">' . $booking_row['stage_name'] . '</a><br />';
            // $event_details .= "<b>Date</b>: " . date('M d, Y h:i A', strtotime($booking_row['date_created'])) . " <br /><br />";
            $event_details .= "<b>Date</b>: " . $booking_row['event_date'] . ' ' . $booking_row['event_start_time'] . '-' . $booking_row['event_end_time'] . " <br /><br />";
            $event_details .= "<b>Cost</b>: N" . number_format($booking_row['booking_cost']) . " <br />";

            $message = $this->template_booking_price_added($to_name, $event_details);

            sendmail($to, $to_name, $subject, $message);
            // ---------------------------------------
        }

        redirect("/admin123/bookings");
    }

    public function accept()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('booking_id', 'ID', 'required|max_length[255]');

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);
        }
        else
        {            
            $this->Booking_model->accept($this->input->post('booking_id'));
            $this->session->set_flashdata('error_code', 0);
            $this->session->set_flashdata('error', "Record updated successfully!");

            $booking_id = (int) trim($this->input->post('booking_id'));

            // get booking_details
            $booking_row = $this->Booking_model->getRowsAll(0, 0, $booking_id);


            // send notification to agent dashboard
            $notification_user_id = $booking_row['agent_id']; // for super admin and admin

            $notification_message = 'Payment confirmed. [Price: N' . number_format($booking_row['booking_cost']) . ' | Event: ' . $booking_row['event_name'] . ' | Artist: <a href="' . base_url() . '@' . $booking_row['username'] . '">' . $booking_row['stage_name'] . '</a>]';
            add_notification($notification_user_id, $notification_message);
            // -----------------------------------------------------------------

            // send payment confirmed mail to agent
            $to = $booking_row['agent_email'];
            $to_name = $booking_row['full_name'];
            $subject = "Payment Confirmed";

            $event_details = "<b>Event Name</b>: " . $booking_row['event_name'] . " <br />";
            $event_details .= '<b>Artist</b>: <a href="' . base_url() . '@' . $booking_row['username'] . '">' . $booking_row['stage_name'] . '</a><br />';
            // $event_details .= "<b>Date</b>: " . date('M d, Y h:i A', strtotime($booking_row['date_created'])) . " <br /><br />";
            $event_details .= "<b>Date</b>: " . $booking_row['event_date'] . ' ' . $booking_row['event_start_time'] . '-' . $booking_row['event_end_time'] . " <br /><br />";
            $event_details .= "<b>Cost</b>: N" . number_format($booking_row['booking_cost']) . " <br />";

            $message = $this->template_booking_confirmed($to_name, $event_details);

            sendmail($to, $to_name, $subject, $message);
            // ---------------------------------------
        }

        redirect("/admin123/bookings");
    }

    public function delete($id)
    {
        $data['row'] = $this->Booking_model->delete($id);
        redirect('/admin123/bookings', 'refresh');
    }

    private function content_industry()
    {
        $output = array(
            array('id' => 1, 'value' => 'Investment'),
            array('id' => 2, 'value' => 'Politics'),
            array('id' => 3, 'value' => 'Product'),
            array('id' => 4, 'value' => 'Retail'),
            array('id' => 5, 'value' => 'Sales'),
            array('id' => 6, 'value' => 'Technology')
        );
        return $output;
    }

    private function content_budget()
    {
        $output = array(
            array('id' => 1, 'value' => '100K TO 500K'),
            array('id' => 2, 'value' => '500K TO 1M'),
            array('id' => 3, 'value' => '1M TO 3M'),
            array('id' => 4, 'value' => '3M TO 5M'),
            array('id' => 5, 'value' => '5M AND ABOVE')
        );
        return $output;
    }

    private function content_tier()
    {
        $output = array(
            array('id' => 1, 'value' => '1'),
            array('id' => 2, 'value' => '2'),
            array('id' => 3, 'value' => '3')
        );
        return $output;
    }

    private function content_campaign_duration()
    {
        $output = array(
            array('id' => 1, 'value' => '1 week'),
            array('id' => 2, 'value' => '1 month'),
            array('id' => 3, 'value' => '6 months'),
            array('id' => 4, 'value' => '1 year')
        );
        return $output;
    }

    private function template_booking_price_added($to_name, $event_details)
    {
        $url = $this->config->base_url() . 'login';

        return "<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns=\"http://www.w3.org/1999/xhtml\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\">

        <body style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; line-height: 1.6; background-color: #f6f6f6; margin: 0; padding: 0;\" bgcolor=\"#f6f6f6\">

        <table class=\"body-wrap\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0; padding: 0;\" bgcolor=\"#f6f6f6\"><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0;\" valign=\"top\"></td>
        <td class=\"container\" width=\"600\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; width: 100% !important; margin: 0 auto; padding: 0;\" valign=\"top\">
            <div class=\"content\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 10px;\">
                <table class=\"main\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; padding: 0; border: 1px solid #e9e9e9;\" bgcolor=\"#fff\"><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"alert alert-warning\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #4aa3df; margin: 0; padding: 20px;\" align=\"center\" bgcolor=\"#4aa3df\" valign=\"top\">
                            Make Payment for Booking
                        </td>
                    </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-wrap\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;\" valign=\"top\">
                            <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                                        Hello $to_name,
                                    </td>
                                </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                                        You can now make payment for the booking details below. The cost has been added to your dashboard.
                                    </td>
                                </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                                        $event_details
                                    </td>
                                </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                                        Login now to proceed.
                                    </td>
                                </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                                        <a href=\"$url\" class=\"btn-primary\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; color: #FFF; text-decoration: none; line-height: 2; font-weight: bold; text-align: center; cursor: pointer; display: inline-block; border-radius: 5px; text-transform: capitalize; background-color: #348eda; margin: 0; padding: 0; border-color: #348eda; border-style: solid; border-width: 10px 20px;\">Login &raquo;</a>
                                    </td>
                                </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                                        Thanks for choosing The Total Package.
                                    </td>
                                </tr></table></td>
                    </tr></table><div class=\"footer\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 20px;\">
                    <table width=\"100%\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"aligncenter content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;\" align=\"center\" valign=\"top\">
                    <!--TTP is a registered trademark in Nigeria. 
                    <br />No 4, Whales Street,
                    <br />Admiralty Way, Landon,
                    <br />Lagos, Nigeria
                    <br />Phone: 0123456789-->
                    <br />info@ttp.dev, support@ttp.dev
                    <br />&copy; " . date("Y") . " The Total Package.</td>
                        </tr></table></div></div>
                </td>
                <td style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0;\" valign=\"top\"></td>
            </tr></table></body>
        </html>";
    }

    private function template_booking_confirmed($to_name, $event_details)
    {
        $url = $this->config->base_url() . 'login';

        return "<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns=\"http://www.w3.org/1999/xhtml\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\">

        <body style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; line-height: 1.6; background-color: #f6f6f6; margin: 0; padding: 0;\" bgcolor=\"#f6f6f6\">

        <table class=\"body-wrap\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0; padding: 0;\" bgcolor=\"#f6f6f6\"><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0;\" valign=\"top\"></td>
        <td class=\"container\" width=\"600\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; width: 100% !important; margin: 0 auto; padding: 0;\" valign=\"top\">
            <div class=\"content\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 10px;\">
                <table class=\"main\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; padding: 0; border: 1px solid #e9e9e9;\" bgcolor=\"#fff\"><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"alert alert-warning\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #4aa3df; margin: 0; padding: 20px;\" align=\"center\" bgcolor=\"#4aa3df\" valign=\"top\">
                            Payment Confirmed
                        </td>
                    </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-wrap\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;\" valign=\"top\">
                            <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                                        Hello $to_name,
                                    </td>
                                </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                                        Your payment has been confirmed and booking accepted. We would contact you on next steps.
                                    </td>
                                </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                                        $event_details
                                    </td>
                                </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                                        Login now to proceed.
                                    </td>
                                </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                                        <a href=\"$url\" class=\"btn-primary\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; color: #FFF; text-decoration: none; line-height: 2; font-weight: bold; text-align: center; cursor: pointer; display: inline-block; border-radius: 5px; text-transform: capitalize; background-color: #348eda; margin: 0; padding: 0; border-color: #348eda; border-style: solid; border-width: 10px 20px;\">Login &raquo;</a>
                                    </td>
                                </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                                        Thanks for choosing The Total Package.
                                    </td>
                                </tr></table></td>
                    </tr></table><div class=\"footer\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 20px;\">
                    <table width=\"100%\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"aligncenter content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;\" align=\"center\" valign=\"top\">
                    <!--TTP is a registered trademark in Nigeria. 
                    <br />No 4, Whales Street,
                    <br />Admiralty Way, Landon,
                    <br />Lagos, Nigeria
                    <br />Phone: 0123456789-->
                    <br />info@ttp.dev, support@ttp.dev
                    <br />&copy; " . date("Y") . " The Total Package.</td>
                        </tr></table></div></div>
                </td>
                <td style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0;\" valign=\"top\"></td>
            </tr></table></body>
        </html>";
    }
}
