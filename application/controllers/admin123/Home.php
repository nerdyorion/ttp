<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Admin_Controller {

    public function __construct()
    {
        // var_dump("hello"); die;
            parent::__construct();
            // $this->load->library('session');
            if(!is_logged_in())  // check if logged in
            {
                //redirect to login
                redirect('/admin123/login');
            }
            $this->load->model('Booking_model');
    }

	public function index()
	{
        $header['page_title'] = 'Home';
        $data['sn'] = 1;
        $data['bookings_total'] = (int) $this->Booking_model->totalCount('all');
        $data['bookings_paid'] = (int) $this->Booking_model->paid('all');
        $data['bookings_pending'] = (int) $this->Booking_model->pending('all'); // where booking_cost = 0

        $data['events_upcoming'] = (int) $this->Booking_model->upcoming('all');

        $data['last_login'] = $this->session->userdata("user_last_login");
        $data["rows"] = $this->Booking_model->getRowsAllUnpaid(5, 0);
        //$data['rows'] = 'rows';
        $this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
        $this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
        $this->load->view($this->config->item('template_dir_admin') . 'home', $data);  // load content view
	}
}