<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends Admin_Controller {

    public function __construct()
    {
            parent::__construct();
            if(!is_logged_in())  // check if logged in
            {
                //redirect to login
                redirect('/admin123/login');
            }
            $this->load->model('User_model');
            //$this->load->helper('url_helper');
    }

	public function index()
	{
    	$this->load->library('form_validation');
        $this->form_validation->set_rules('full_name', 'Full Name', 'required|max_length[2000]');
        $this->form_validation->set_rules('phone', 'Phone', 'numeric|max_length[13]');
        $this->form_validation->set_rules('email', 'Email', 'required|max_length[255]|valid_email');
        $this->form_validation->set_rules('gender', 'Gender', 'required|max_length[255]');

    	if ($this->form_validation->run() === FALSE)
    	{
        	$errors = str_replace("<p>","", validation_errors());
        	$errors = str_replace("</p>","", trim($errors));
        	$this->session->set_flashdata('error', $errors);
        	$this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

			$data['error'] = $this->session->flashdata('error');
			$data['error_code'] = $this->session->flashdata('error_code');
        	$header['page_title'] = 'Profile';  // set page title
            $data['row'] = $this->User_model->getRows($this->session->userdata('user_id'));  // get user profile
            if(empty($data['row']))
                redirect('/admin123/logout');   // redirect('/account/login', 'refresh');
        	
        	$this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
        	$this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
        	$this->load->view($this->config->item('template_dir_admin') . 'profile', $data);  // load content view
        }
    	else
    	{
			$this->User_model->updateProfile($this->session->userdata('user_id'));  // update profile

            $this->session->set_flashdata('error_code', 0);
            $this->session->set_flashdata('error', "Profile updated successfully!");
            // get new data after updating
            $data['row'] = $this->User_model->getRows($this->session->userdata('user_id'));  // get user profile
            if(empty($data['row']))
                redirect('/admin123/logout');

            $data['error'] = $this->session->flashdata('error');
            $data['error_code'] = $this->session->flashdata('error_code');
            $header['page_title'] = 'Profile';  // set page title

        	$this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
        	$this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
    	    $this->load->view($this->config->item('template_dir_admin') . 'profile', $data);
            //redirect('/profile', 'refresh');
    	}
	}
}
