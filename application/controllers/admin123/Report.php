<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends Admin_Controller {

    public function __construct()
    {
            parent::__construct();
            if(!is_logged_in())  // check if logged in
            {
                //redirect to login
                redirect('/admin123/login');
            }
            $this->load->model('User_model');
            $this->load->model('Booking_model');
            $this->load->library("pagination");

            // Status
            // 1 - Accepted
            // 2 - Rejected
            // 3 - Unpaid
            // 4 - Paid
    }

    public function index()
    {
        $user_id = (int) $this->session->userdata("user_id");

        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');
        $header['page_title'] = 'Report';

        $data['agents'] = $this->User_model->getRowsAgentDropDown();
        $data['talents'] = $this->User_model->getRowsTalentDropDown(); 

        // Pagination
        $artist_id = isset($_GET['artist_id']) ? trim($_GET['artist_id']) : FALSE;
        $agent_id = isset($_GET['agent_id']) ? trim($_GET['agent_id']) : FALSE;

        $start = isset($_GET['start']) ? trim($_GET['start']) : FALSE;
        $end = isset($_GET['end']) ? trim($_GET['end']) : FALSE;

        $start2 = isset($_GET['start2']) ? trim($_GET['start2']) : FALSE;
        $end2 = isset($_GET['end2']) ? trim($_GET['end2']) : FALSE;

        $status = isset($_GET['status']) ? trim($_GET['status']) : FALSE;

        if($artist_id || $agent_id || $start || $end || $start2 || $end2 || $status)
        {
            $config["total_rows"] = $this->Booking_model->filter_record_count($artist_id, $agent_id, $start, $end, $start2, $end2, $status);
        }
        else
        {
            $config["total_rows"] = 0;
            // $config["total_rows"] = $this->Booking_model->record_count();
        }



        $config["base_url"] = base_url() . "admin123/report/index";
        // $config["total_rows"] = $this->Booking_model->record_count();
        $config["per_page"] = 10;
        $config["uri_segment"] = 4;
        $config['use_page_numbers']  = TRUE;
        $config['reuse_query_string']=TRUE;

        $config['first_tag_open'] = $config['last_tag_open']= $config['next_tag_open']= $config['prev_tag_open'] = $config['num_tag_open'] = '<li>';
        $config['first_tag_close'] = $config['last_tag_close']= $config['next_tag_close']= $config['prev_tag_close'] = $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li><span><b>";
        $config['cur_tag_close'] = "</b></span></li>";

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $offset = 0;
        if($page > 1)
        {
            $offset = ($page * $config["per_page"]) - $config["per_page"];
        }

        $data['sn'] = $offset == 0 ? 1 : $offset + 1;
        if($artist_id || $agent_id || $start || $end || $start2 || $end2 || $status)
        {
            $data['rows'] = $this->Booking_model->filter($config["per_page"], $offset, $artist_id, $agent_id, $start, $end, $start2, $end2, $status);
        }
        else
        {
            $data['rows'] = array();
            // $data['rows'] = $this->Booking_model->getRows($config["per_page"], $offset);
        }
        // $data["rows"] = $this->Booking_model->getRows($config["per_page"], $offset);
        $data["links"] = $this->pagination->create_links();

        $this->load->view($this->config->item('template_dir_admin') . 'header', $header);
        $this->load->view($this->config->item('template_dir_admin') . 'menu');
        $this->load->view($this->config->item('template_dir_admin') . 'report', $data);
    }

    public function backup()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('msisdn', 'MSISDN', 'trim|required|max_length[5000]');
        //$this->form_validation->set_rules('network_id', 'Network', 'trim|required|max_length[10]');

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

            $data['error'] = $this->session->flashdata('error');
            $data['error_code'] = $this->session->flashdata('error_code');
            $header['page_title'] = 'Booking Report';
            $data['rows'] = '';
            
            $this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
            $this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
            $this->load->view($this->config->item('template_dir_admin') . 'report', $data);  // load content view
        }
        else
        {
            $data['error'] = $this->session->flashdata('error');
            $data['error_code'] = $this->session->flashdata('error_code');
                        $header['page_title'] = 'Booking Report';
            //$data['rows'] = $this->DND_Number_model->getRowByMSISDN();
            $data['rows'] = $this->DND_Number_model->search();

            $this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
            $this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
            $this->load->view($this->config->item('template_dir_admin') . 'report', $data);
        }
    }
}
