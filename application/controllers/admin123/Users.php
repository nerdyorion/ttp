<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends Admin_Controller {

    public function __construct()
    {
            parent::__construct();
            if(!is_logged_in())  // check if logged in
            {
                //redirect to login
                redirect('/admin123/login');
            }
            if(!is_super_admin())  // check if is super admin
            {
                //redirect to 404
                redirect('/admin123/404');
                //show_404();
            }
            if(is_report_admin())  // check if is dnd_search admin
            {
                //redirect to 404
                redirect('/admin123/404');
                //show_404();
            }
            $this->load->model('User_model');
            $this->load->model('Role_model');
            //$this->load->helper('url_helper');
    }

	public function index()
	{
    	$this->load->library('form_validation');
        $this->form_validation->set_rules('full_name', 'Full Name', 'trim|required|max_length[2000]');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|numeric|max_length[13]');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|max_length[255]|valid_email');
        $this->form_validation->set_rules('gender', 'Gender', 'trim|required|max_length[255]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|max_length[8000]');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|max_length[8000]|matches[password]');

    	if ($this->form_validation->run() === FALSE)
    	{
        	$errors = str_replace("<p>","", validation_errors());
        	$errors = str_replace("</p>","", trim($errors));
        	$this->session->set_flashdata('error', $errors);
        	$this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

			$data['error'] = $this->session->flashdata('error');
			$data['error_code'] = $this->session->flashdata('error_code');
        	$header['page_title'] = 'Users';  // set page title
        	$data['rows'] = $this->User_model->getRows();
            $data['roles'] = $this->Role_model->getRows();
        	
        	$this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
        	$this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
        	$this->load->view($this->config->item('template_dir_admin') . 'users', $data);  // load content view
        }
    	else
    	{
			$data['error'] = $this->session->flashdata('error');
			$data['error_code'] = $this->session->flashdata('error_code');
    	    $header['page_title'] = 'Users';  // set page title
			$this->User_model->add();


            $data['rows'] = $this->User_model->getRows();
            $data['roles'] = $this->Role_model->getRows();
        	$this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
        	$this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
    	    $this->load->view($this->config->item('template_dir_admin') . 'users', $data);
    	}
	}

    public function view($id = NULL)
    {
        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');

        $header['page_title'] = 'User Details';
        $data['row'] = $this->User_model->getRows($id);

        $this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
        $this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
        $this->load->view($this->config->item('template_dir_admin') . 'user-details', $data);  // load content view
    }

    public function edit($id)
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('full_name', 'Full Name', 'required|max_length[2000]');
        $this->form_validation->set_rules('email', 'Email', 'required|max_length[255]|valid_email');
        $this->form_validation->set_rules('gender', 'Gender', 'required|max_length[255]');

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

            $data['error'] = $this->session->flashdata('error');
            $data['error_code'] = $this->session->flashdata('error_code');
            $header['page_title'] = 'Update User';  // set page title
            $data['row'] = $this->User_model->getRows($id);
            $data['roles'] = $this->Role_model->getRows();
            if(empty($data['row']))
                redirect("/admin123/users");
            
            $this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
            $this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
            $this->load->view($this->config->item('template_dir_admin') . 'user-edit', $data);  // load content view
        }
        else
        {
            $header['page_title'] = 'Update User';  // set page title
            $this->User_model->update($id);
            $this->session->set_flashdata('error_code', 0);
            $this->session->set_flashdata('error', "Record updated successfully!");
            // get new data after updating
            $data['row'] = $this->User_model->getRows($id);
            $data['roles'] = $this->Role_model->getRows();
            if(empty($data['row']))
                redirect("/admin123/users");

            $data['error'] = $this->session->flashdata('error');
            $data['error_code'] = $this->session->flashdata('error_code');

            $this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
            $this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
            $this->load->view($this->config->item('template_dir_admin') . 'user-edit', $data);

            //redirect('/users/edit/' . $id, 'refresh');
        }
    }

    public function delete($id)
    {
        $data['row'] = $this->User_model->delete($id);

        $this->session->set_flashdata('error_code', 0);
        $this->session->set_flashdata('error', "Record deleted successfully!");
        redirect('/admin123/users', 'refresh');
    }
}
