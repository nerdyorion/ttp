<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calendar extends Agent_Controller {

    public function __construct()
    {
            parent::__construct();
            if(!is_logged_in())  // check if logged in
            {
                //redirect to login
                redirect('/login');
            }
            $this->load->helper("url");
            $this->load->model('Booking_model');
            $this->load->library("pagination");
            // $this->naviHref = htmlentities($_SERVER['PHP_SELF']);
            $this->naviHref = base_url() . 'agent/calendar';
    }


	public function index()
	{
        $user_id = (int) $this->session->userdata("user_id");
        $username = $this->session->userdata("user_username");

        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');
        $header['page_title'] = 'Bookings';
        $data['username'] = $username;

        // Pagination
        $config["base_url"] = base_url() . "agent/bookings/index";
        $config["total_rows"] = $this->Booking_model->record_count("agent", $user_id);
        $config["per_page"] = 10;
        $config["uri_segment"] = 4;
        $config['use_page_numbers']  = TRUE;

        $config['first_tag_open'] = $config['last_tag_open']= $config['next_tag_open']= $config['prev_tag_open'] = $config['num_tag_open'] = '<li>';
        $config['first_tag_close'] = $config['last_tag_close']= $config['next_tag_close']= $config['prev_tag_close'] = $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li><span><b>";
        $config['cur_tag_close'] = "</b></span></li>";

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $offset = 0;
        if($page > 1)
        {
            $offset = ($page * $config["per_page"]) - $config["per_page"];
        }

        $data['sn'] = $offset == 0 ? 1 : $offset + 1;
        $data["rows"] = $this->Booking_model->getRowsByAgentIDToday($config["per_page"], $offset, $user_id);
        $data["links"] = $this->pagination->create_links();

        // Calendar

      $year = null;
      $month = null;
      if (null == $year && isset($_GET['year'])) {
        $year = htmlentities($_GET['year'], ENT_QUOTES);
      } elseif (null == $year) {
        $year = date("Y", time());
      }
      if ((!is_numeric($year)) || ($year == "")) {
        $year = date("Y", time());
      }
      if (null == $month && isset($_GET['month'])) {
        $month = htmlentities($_GET['month'], ENT_QUOTES);
      } elseif (null == $month) {
        $month = date("m", time());
      }
      if ((!is_numeric($month)) || ($month == "")) {
        $month = date("m", time());
      }
      $this->currentYear = $year;
      $this->currentMonth = $month;
      $this->daysInMonth = $this->_daysInMonth($month, $year);
      $content = '<div id="calendar">' . "\r\n" . '<div class="calendar_box">' . "\r\n" . $this->_createNavi() . "\r\n" . '</div>' . "\r\n" . '<div class="calendar_content">' . "\r\n" . '<div class="calendar_label">' . "\r\n" . $this->_createLabels() . '</div>' . "\r\n";
      $content .= '<div class="calendar_clear"></div>' . "\r\n";
      $content .= '<div class="calendar_dates">' . "\r\n";
      $weeksInMonth = $this->_weeksInMonth($month, $year);
      // Create weeks in a month
      for ($i = 0; $i < $weeksInMonth; $i++) {
        // Create days in a week
        for ($j = 1; $j <= 7; $j++) {
          $content .= $this->_showDay($i * 7 + $j);
        }
      }
      $content .= '</div>' . "\r\n";
      $content .= '<div class="calendar_clear"></div>' . "\r\n";
      $content .= '</div>' . "\r\n";
      $content .= '</div>' . "\r\n";
      // return $content;
      $data['calendar'] = $content;

       // Calendar

        $this->load->view($this->config->item('template_dir_agent') . 'header', $header);
        $this->load->view($this->config->item('template_dir_agent') . 'menu');
        $this->load->view($this->config->item('template_dir_agent') . 'calendar', $data);
	}

    public function delete($id)
    {
        $this->Booking_model->deleteByAgentID($id);

        $this->session->set_flashdata('error_code', 0);
        $this->session->set_flashdata('error', "Record deleted successfully!");

        redirect('/agent/bookings', 'refresh');
    }

    /********************* PROPERTY ********************/
    private $dayLabels = array("Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun");
    private $currentYear = 0;
    private $currentMonth = 0;
    private $currentDay = 0;
    private $currentDate = null;
    private $daysInMonth = 0;
    private $naviHref = null;
    /********************* PUBLIC **********************/
    /**
    ** Print out the calendar
    **/
    /*
    public function show() {
      $year = null;
      $month = null;
      if (null == $year && isset($_GET['year'])) {
        $year = htmlentities($_GET['year'], ENT_QUOTES);
      } elseif (null == $year) {
        $year = date("Y", time());
      }
      if ((!is_numeric($year)) || ($year == "")) {
        $year = date("Y", time());
      }
      if (null == $month && isset($_GET['month'])) {
        $month = htmlentities($_GET['month'], ENT_QUOTES);
      } elseif (null == $month) {
        $month = date("m", time());
      }
      if ((!is_numeric($month)) || ($month == "")) {
        $month = date("m", time());
      }
      $this->currentYear = $year;
      $this->currentMonth = $month;
      $this->daysInMonth = $this->_daysInMonth($month, $year);
      $content = '<div id="calendar">' . "\r\n" . '<div class="calendar_box">' . "\r\n" . $this->_createNavi() . "\r\n" . '</div>' . "\r\n" . '<div class="calendar_content">' . "\r\n" . '<div class="calendar_label">' . "\r\n" . $this->_createLabels() . '</div>' . "\r\n";
      $content .= '<div class="calendar_clear"></div>' . "\r\n";
      $content .= '<div class="calendar_dates">' . "\r\n";
      $weeksInMonth = $this->_weeksInMonth($month, $year);
      // Create weeks in a month
      for ($i = 0; $i < $weeksInMonth; $i++) {
        // Create days in a week
        for ($j = 1; $j <= 7; $j++) {
          $content .= $this->_showDay($i * 7 + $j);
        }
      }
      $content .= '</div>' . "\r\n";
      $content .= '<div class="calendar_clear"></div>' . "\r\n";
      $content .= '</div>' . "\r\n";
      $content .= '</div>' . "\r\n";
      return $content;
    }
    */
    /********************* PRIVATE **********************/ 
    /**
    ** Create the calendar days
    **/
    private function _showDay($cellNumber) {

      if ($this->currentDay == 0) {
        $firstDayOfTheWeek = date('N', strtotime($this->currentYear . '-' . $this->currentMonth . '-01'));
        if (intval($cellNumber) == intval($firstDayOfTheWeek)) {
          $this->currentDay = 1;
        }
      }
      if (($this->currentDay != 0) && ($this->currentDay <= $this->daysInMonth)) {
        $this->currentDate = date('Y-m-d', strtotime($this->currentYear . '-' . $this->currentMonth . '-' . ($this->currentDay)));
        $cellContent = $this->currentDay;
        $this->currentDay++;
      } else {
        $this->currentDate = null;
        $cellContent = null;
      }

      // added by tayo
      $year = null;
      $month = null;
      if (null == $year && isset($_GET['year'])) {
        $year = htmlentities($_GET['year'], ENT_QUOTES);
      } elseif (null == $year) {
        $year = date("Y", time());
      }
      if ((!is_numeric($year)) || ($year == "")) {
        $year = date("Y", time());
      }
      if (null == $month && isset($_GET['month'])) {
        $month = htmlentities($_GET['month'], ENT_QUOTES);
      } elseif (null == $month) {
        $month = date("m", time());
      }
      if ((!is_numeric($month)) || ($month == "")) {
        $month = date("m", time());
      }
      // added by tayo
      $day = strlen($cellContent) == 1 ? '0' . $cellContent : $cellContent;

      $today_day = date("d");
      $today_mon = date("m");
      $today_yea = date("Y");
      $class_day = ($cellContent == $today_day && $this->currentMonth == $today_mon && $this->currentYear == $today_yea ? "calendar_today" : "calendar_days");
      return '<div class="' . $class_day . '" data-date="' . $year . '-' . $month . '-' . $day . '">' . $cellContent . '</div>' . "\r\n";
    }
    /**
    ** Create navigation
    **/
    private function _createNavi() {
      $nextMonth = $this->currentMonth == 12 ? 1 : intval($this->currentMonth)+1;
      $nextYear = $this->currentMonth == 12 ? intval($this->currentYear)+1 : $this->currentYear;
      $preMonth = $this->currentMonth == 1 ? 12 : intval($this->currentMonth)-1;
      $preYear = $this->currentMonth == 1 ? intval($this->currentYear)-1 : $this->currentYear;
      return '<div class="calendar_header">' . "\r\n" . '<a class="calendar_prev" href="' . $this->naviHref . '?month=' . sprintf('%02d', $preMonth) . '&amp;year=' . $preYear.'">Prev</a>' . "\r\n" . '<span class="calendar_title">' . date('Y F', strtotime($this->currentYear . '-' . $this->currentMonth . '-1')) . '</span>' . "\r\n" . '<a class="calendar_next" href="' . $this->naviHref . '?month=' . sprintf("%02d", $nextMonth) . '&amp;year=' . $nextYear . '">Next</a>' . "\r\n"  . '</div>';
    }
    /**
    ** Create calendar week labels
    **/
    private function _createLabels() {
      $content = '';
      foreach ($this->dayLabels as $index => $label) {
        $content .= '<div class="calendar_names">' . $label . '</div>' . "\r\n";
      }
      return $content;
    }
    /**
    ** Calculate number of weeks in a particular month
    **/
    private function _weeksInMonth($month = null, $year = null) {
      if (null == ($year)) {
        $year = date("Y", time());
      }
      if (null == ($month)) {
        $month = date("m", time());
      }
      // Find number of days in this month
      $daysInMonths = $this->_daysInMonth($month, $year);
      $numOfweeks = ($daysInMonths % 7 == 0 ? 0 : 1) + intval($daysInMonths / 7);
      $monthEndingDay = date('N',strtotime($year . '-' . $month . '-' . $daysInMonths));
      $monthStartDay = date('N',strtotime($year . '-' . $month . '-01'));
      if ($monthEndingDay < $monthStartDay) {
        $numOfweeks++;
      }
      return $numOfweeks;
    }
    /**
    ** Calculate number of days in a particular month
    **/
    private function _daysInMonth($month = null, $year = null) {
      if (null == ($year)) $year = date("Y",time());
      if (null == ($month)) $month = date("m",time());
      return date('t', strtotime($year . '-' . $month . '-01'));
    }
  // $calendar = new Calendar();
  // echo $calendar->show();
}
