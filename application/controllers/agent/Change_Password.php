<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Change_Password extends Artist_Controller {

    public function __construct()
    {
            parent::__construct();
            if(!is_logged_in())  // check if logged in
            {
                //redirect to login
                redirect('/login');
            }
            $this->load->model('User_model');
            //$this->load->helper('url_helper');
    }

	public function index()
	{
    	$this->load->library('form_validation');
        $this->form_validation->set_rules('current_password', 'Current Password', 'required|max_length[8000]');
        $this->form_validation->set_rules('new_password', 'New Password', 'required|max_length[8000]');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|max_length[8000]|matches[new_password]');

    	if ($this->form_validation->run() === FALSE)
    	{
        	$errors = str_replace("<p>","", validation_errors());
        	$errors = str_replace("</p>","", trim($errors));
        	$this->session->set_flashdata('error', $errors);
        	$this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

			$data['error'] = $this->session->flashdata('error');
			$data['error_code'] = $this->session->flashdata('error_code');
        	$header['page_title'] = 'Change Password';  // set page title
        	$data['rows'] = '';
        	
        	$this->load->view($this->config->item('template_dir_artist') . 'header', $header);  // load header view
        	$this->load->view($this->config->item('template_dir_artist') . 'menu');  // load menu view
        	$this->load->view($this->config->item('template_dir_artist') . 'change-password', $data);  // load content view
        }
    	else
    	{
            $changed = $this->User_model->changePassword($this->session->userdata('user_id'));
            if(!$changed)
            {
                $this->session->set_flashdata('error', "Invalid Password!");
                $this->session->set_flashdata('error_code', 1);
            }
            else {
                $this->session->set_flashdata('error', "Password Changed Successfully!");
                $this->session->set_flashdata('error_code', 0);
            }
			$data['error'] = $this->session->flashdata('error');
			$data['error_code'] = $this->session->flashdata('error_code');
    	    $header['page_title'] = 'Change Password';  // set page title

        	$this->load->view($this->config->item('template_dir_artist') . 'header', $header);  // load header view
        	$this->load->view($this->config->item('template_dir_artist') . 'menu');  // load menu view
    	    $this->load->view($this->config->item('template_dir_artist') . 'change-password', $data);
    	}
	}
}
