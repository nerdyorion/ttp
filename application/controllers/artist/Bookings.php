<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bookings extends Artist_Controller {

    public function __construct()
    {
            parent::__construct();
            if(!is_logged_in())  // check if logged in
            {
                //redirect to login
                redirect('/login');
            }
            $this->load->helper("url");
            $this->load->model('Booking_model');
            $this->load->model('User_model');
            $this->load->library("pagination");
    }

    public function index()
    {
        $user_id = (int) $this->session->userdata("user_id");
        $username = $this->session->userdata("user_username");

        $data['user_row'] = $this->User_model->getUserFullProfile($username);

        if(empty($data['user_row']))
        {
            redirect("/");
        }

        $data['content_industry'] = $this->content_industry();
        $data['content_budget'] = $this->content_budget();
        $data['content_tier'] = $this->content_tier();
        $data['content_campaign_duration'] = $this->content_campaign_duration();

        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');
        $header['page_title'] = 'Bookings';
        $data['username'] = $username;

        // Pagination
        $config["base_url"] = base_url() . "artist/bookings/index";
        $config["total_rows"] = $this->Booking_model->record_count("talent", $user_id); // var_dump($config["total_rows"]); die;
        $config["per_page"] = 10;
        $config["uri_segment"] = 4;
        $config['use_page_numbers']  = TRUE;

        $config['first_tag_open'] = $config['last_tag_open']= $config['next_tag_open']= $config['prev_tag_open'] = $config['num_tag_open'] = '<li>';
        $config['first_tag_close'] = $config['last_tag_close']= $config['next_tag_close']= $config['prev_tag_close'] = $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li><span><b>";
        $config['cur_tag_close'] = "</b></span></li>";

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $offset = 0;
        if($page > 1)
        {
            $offset = ($page * $config["per_page"]) - $config["per_page"];
        }

        $data['sn'] = $offset == 0 ? 1 : $offset + 1;
        $data["rows"] = $this->Booking_model->getRowsByTalentID($config["per_page"], $offset, $user_id);
        $data["links"] = $this->pagination->create_links();

        $this->load->view($this->config->item('template_dir_artist') . 'header', $header);
        $this->load->view($this->config->item('template_dir_artist') . 'menu');
        $this->load->view($this->config->item('template_dir_artist') . 'bookings', $data);
    }

    public function view($id = NULL)
    {
        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');

        $header['page_title'] = 'Booking Details';
        $data['row'] = $this->Booking_model->getRowsAll(0, 0, $id);

        $content_industry = $this->content_industry();
        $content_budget = $this->content_budget();
        $content_tier = $this->content_tier();
        $content_campaign_duration = $this->content_campaign_duration();

        $content_industry_id = getArrayKey($content_industry, 'id', trim($data['row']['content_industry_id']));
        $data['content_industry'] = isset($content_industry[$content_industry_id]) ? $content_industry[$content_industry_id]['value'] : '-';

        $content_budget_id = getArrayKey($content_budget, 'id', trim($data['row']['content_budget_id']));
        $data['content_budget'] = isset($content_budget[$content_budget_id]) ? $content_budget[$content_budget_id]['value'] : '-';

        $content_tier_id = getArrayKey($content_tier, 'id', trim($data['row']['content_tier_id']));
        $data['content_tier'] = isset($content_tier[$content_tier_id]) ? $content_tier[$content_tier_id]['value'] : '-';

        $content_campaign_duration_id = getArrayKey($content_campaign_duration, 'id', trim($data['row']['content_campaign_duration_id']));
        $data['content_campaign_duration'] = isset($content_campaign_duration[$content_campaign_duration_id]) ? $content_campaign_duration[$content_campaign_duration_id]['value'] : '-';

        $this->load->view($this->config->item('template_dir_artist') . 'header', $header);  // load header view
        $this->load->view($this->config->item('template_dir_artist') . 'menu');  // load menu view
        $this->load->view($this->config->item('template_dir_artist') . 'booking-details', $data);  // load content view
    }

    public function byDate($date)
    {
        $user_id = (int) $this->session->userdata("user_id");
        if(strlen($date) == 10)
        {
            $rows = $this->Booking_model->getRowsByTalentIDByDate($user_id, $date);
            if(empty($rows))
            {
                echo '';
            }
            else
            {
                $table_rows = '';
                $sn = 1;
                foreach ($rows as $row) {
                    if($row['external_booking'] == 1)
                    {
                        $table_rows .= '<tr><td colspan="4" align="center" id="sn">Bookings are disabled for this day.
                            <p><a href="artist/calendar/enable/' . $row['event_date'] . '" class="disable-link">Click here to enable bookings for this day.</a></p></td></tr>';
                    }
                    else
                    {
                        $table_rows .= '<tr>';
                        $table_rows .= '<td id="sn">' . $sn++ . '</td>';
                        $table_rows .= '<td title="' . $row['event_name'] . '">' . $row['event_name'] . '</td>';

                        $status1 = $row['external_booking'] == 1 ? '<a href="javascript:void(0);" class="bg-success" style="color: #ffffff;">&nbsp;DISABLED&nbsp;</a>' : '';

                        $status2 = $row['accepted'] == 1 ? '<a href="javascript:void(0);" class="bg-success" style="color: #ffffff;">&nbsp;Accepted&nbsp;</a>' : '';

                        $status3 = $row['rejected'] == 1 ? '<a href="javascript:void(0);" class="bg-danger" style="color: #ffffff;">&nbsp;Rejected&nbsp;</a>' : '';

                        $status4 = $row['rejected'] != 1 && $row['external_booking'] != 1 && $row['paid'] == 0 ? '<a href="javascript:void(0);" class="bg-danger" style="color: #ffffff;">&nbsp;Unpaid&nbsp;</a>' : '';

                        $status5 = $row['paid'] == 1 && $row['accepted'] == 0 ? '<a href="javascript:void(0);" class="bg-info" style="color: #ffffff;">&nbsp;Paid&nbsp;</a>' : '';

                        $table_rows .= '<td>' . $status1 . ' ' . $status2  . ' ' . $status3 . ' ' . $status4 . '</td>';
                        $table_rows .= '<td title="' . $row['event_start_time'] . '-' . $row['event_end_time'] . '">' . $row['event_start_time'] . '-' . $row['event_end_time'] . '</td>';
                        $table_rows .= '</tr>';
                    }
                }
                echo $table_rows;
            }
        }
    }

    public function delete($id)
    {
        $this->Booking_model->deleteByTalentID($id);

        $this->session->set_flashdata('error_code', 0);
        $this->session->set_flashdata('error', "Record deleted successfully!");

        redirect('/artist/bookings', 'refresh');
    }

    private function content_industry()
    {
        $output = array(
            array('id' => 1, 'value' => 'Investment'),
            array('id' => 2, 'value' => 'Politics'),
            array('id' => 3, 'value' => 'Product'),
            array('id' => 4, 'value' => 'Retail'),
            array('id' => 5, 'value' => 'Sales'),
            array('id' => 6, 'value' => 'Technology')
        );
        return $output;
    }

    private function content_budget()
    {
        $output = array(
            array('id' => 1, 'value' => '100K TO 500K'),
            array('id' => 2, 'value' => '500K TO 1M'),
            array('id' => 3, 'value' => '1M TO 3M'),
            array('id' => 4, 'value' => '3M TO 5M'),
            array('id' => 5, 'value' => '5M AND ABOVE')
        );
        return $output;
    }

    private function content_tier()
    {
        $output = array(
            array('id' => 1, 'value' => '1'),
            array('id' => 2, 'value' => '2'),
            array('id' => 3, 'value' => '3')
        );
        return $output;
    }

    private function content_campaign_duration()
    {
        $output = array(
            array('id' => 1, 'value' => '1 week'),
            array('id' => 2, 'value' => '1 month'),
            array('id' => 3, 'value' => '6 months'),
            array('id' => 4, 'value' => '1 year')
        );
        return $output;
    }
}
