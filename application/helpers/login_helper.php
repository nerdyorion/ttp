<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function is_logged_in() {
    // Get current CodeIgniter instance
    $CI =& get_instance(); 
    // $CI->load->library('session');
    // var_dump($CI->session->userdata); die;
    // We need to use $CI->session instead of $this->session
    $user_id = $CI->session->userdata('user_id');
    $user_full_name = $CI->session->userdata('user_full_name');
    $user_role_id = $CI->session->userdata('user_role_id');
    $user_role_name = $CI->session->userdata('user_role_name');

    // var_dump($user_id); echo "<br />";
    // var_dump($user_full_name); echo "<br />";
    // var_dump($user_role_id); echo "<br />";
    // var_dump($user_role_name); echo "<br />";
    // die;

    // var_dump($this->session->userdata('user_id')); echo "<br />";
    // var_dump($this->session->userdata('user_full_name')); echo "<br />";
    // var_dump($this->session->userdata('user_role_id')); echo "<br />";
    // var_dump($this->session->userdata('user_role_name')); echo "<br />";
    // die;

    // return true;
    

    if (!isset($user_id) || !isset($user_full_name) || !isset($user_role_id) || !isset($user_role_name)) 
    {
        return false;
    }
    else
    {
        return true;
    }
    
}
function is_super_admin() {
    // Get current CodeIgniter instance
    $CI =& get_instance();
    // We need to use $CI->session instead of $this->session
    $user_id = $CI->session->userdata('user_id');
    $user_full_name = $CI->session->userdata('user_full_name');
    $user_role_id = $CI->session->userdata('user_role_id');
    $user_role_name = $CI->session->userdata('user_role_name');
    if (!isset($user_id) || !isset($user_full_name) || !isset($user_role_id) || !isset($user_role_name)) 
    {
        return false;
    }
    else
    {
        if(($user_role_id == 1) && (strtoupper($user_role_name) == "SUPER ADMINISTRATOR"))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
function is_report_admin() {
    // Get current CodeIgniter instance
    $CI =& get_instance();
    // We need to use $CI->session instead of $this->session
    $user_id = $CI->session->userdata('user_id');
    $user_full_name = $CI->session->userdata('user_full_name');
    $user_role_id = $CI->session->userdata('user_role_id');
    $user_role_name = $CI->session->userdata('user_role_name');
    if (!isset($user_id) || !isset($user_full_name) || !isset($user_role_id) || !isset($user_role_name)) 
    {
        return false;
    }
    else
    {
        if(($user_role_id == 3) && (strtoupper($user_role_name) == "REPORT ADMIN"))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
function is_agent() {
    // Get current CodeIgniter instance
    $CI =& get_instance();
    // We need to use $CI->session instead of $this->session
    $user_id = $CI->session->userdata('user_id');
    $user_full_name = $CI->session->userdata('user_full_name');
    $user_role_id = (int) $CI->session->userdata('user_role_id');
    $user_role_name = $CI->session->userdata('user_role_name');
    if (!isset($user_id) || !isset($user_full_name) || !isset($user_role_id) || !isset($user_role_name)) 
    {
        return false;
    }
    else
    {
        if(($user_role_id == 4) && (strtoupper($user_role_name) == "BOOKING AGENT"))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

function go_to_dashboard()
{
    // Get current CodeIgniter instance
    $CI =& get_instance();

    if($CI->session->userdata("user_role_id") == 3)
    {
        // artist
        if(is_null($CI->session->userdata("user_last_login"))) // first time login, goto complete profile
        {
            redirect('/artist/profile');
        }
        else // goto dashboard
        {
            redirect('/artist');
        }
    }
    elseif($CI->session->userdata("user_role_id") == 4)
    {
        // agent
        if(is_null($CI->session->userdata("user_last_login"))) // first time login, goto complete profile
        {
            redirect('/agent/profile');
        }
        else // goto dashboard
        {
            redirect('/agent');
        }
    }
    elseif($CI->session->userdata("user_role_id") == 1 || $CI->session->userdata("user_role_id") == 2)
    {
        // super admin / admin
        redirect('/admin123');
    }
    else
    {
        // invalid user role
        $errors = "Authentication error! Please <a href='contact'>contact support</a>.";
        $CI->session->set_flashdata('error', $errors);
        $CI->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

        $CI->session->unset_userdata('user_id');
        $CI->session->unset_userdata('user_full_name');
        $CI->session->unset_userdata('user_role_id');
        $CI->session->unset_userdata('user_role_name');
        redirect('/login', 'refresh');
    }
}
?>