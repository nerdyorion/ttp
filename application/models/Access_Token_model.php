<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Access_Token_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function getRows($id = FALSE)
    {
        if ($id === FALSE)
        {
            //$query = $this->db->get('dnd_access_tokens');
            //return $query->result_array();
            $query = $this->db->order_by('access_tokens.full_name', 'ASC');
            $this->db->select('access_tokens.id, access_tokens.role_id, access_tokens.first_name, access_tokens.last_name, access_tokens.full_name, access_tokens.stage_name, access_tokens.email, access_tokens.username, access_tokens.gender, access_tokens.country_id, access_tokens.category_id, access_tokens.category_social_influencer_id, access_tokens.genre_gospel, access_tokens.genre_hiphop, access_tokens.genre_rap, access_tokens.genre_afro_pop, access_tokens.genre_fuji, access_tokens.genre_r_and_b, access_tokens.genre_jazz, access_tokens.phone, access_tokens.city, access_tokens.address, access_tokens.landline, access_tokens.company, access_tokens.position, access_tokens.website, access_tokens.bio, access_tokens.bank_name, access_tokens.account_type, access_tokens.account_name, access_tokens.account_no, access_tokens.branch_code, access_tokens.image_url, access_tokens.instagram_url, access_tokens.twitter_url, access_tokens.facebook_url, access_tokens.booking_cost, access_tokens.featured, access_tokens.last_login, access_tokens.is_suspended, roles.name AS role_name');
            $this->db->from('access_tokens'); 
            $this->db->join('roles', 'access_tokens.role_id = roles.id', 'left');
            $this->db->where('access_tokens.role_id', 1);
            $this->db->or_where('access_tokens.role_id', 2);
            // $query = $this->db->limit(20);
            $query = $this->db->get(); //echo $this->db->last_query(); die;

            return $query->result_array();
        }
        $this->db->select('access_tokens.id, access_tokens.role_id, access_tokens.first_name, access_tokens.last_name, access_tokens.full_name, access_tokens.stage_name, access_tokens.email, access_tokens.username, access_tokens.gender, access_tokens.country_id, access_tokens.category_id, access_tokens.category_social_influencer_id, access_tokens.genre_gospel, access_tokens.genre_hiphop, access_tokens.genre_rap, access_tokens.genre_afro_pop, access_tokens.genre_fuji, access_tokens.genre_r_and_b, access_tokens.genre_jazz, access_tokens.phone, access_tokens.city, access_tokens.address, access_tokens.landline, access_tokens.company, access_tokens.position, access_tokens.website, access_tokens.bio, access_tokens.bank_name, access_tokens.account_type, access_tokens.account_name, access_tokens.account_no, access_tokens.branch_code, access_tokens.image_url, access_tokens.instagram_url, access_tokens.twitter_url, access_tokens.facebook_url, access_tokens.booking_cost, access_tokens.featured, access_tokens.last_login, access_tokens.is_suspended, roles.name AS role_name, access_tokens.token AS ig_access_token');
        $this->db->from('access_tokens'); 
        $this->db->join('roles', 'access_tokens.role_id = roles.id', 'left');
        $this->db->join('access_tokens', 'access_tokens.id = access_tokens.user_id', 'left');
        $this->db->where('access_tokens.id', $id); 
        $query = $this->db->limit(1);
        $query = $this->db->get();    //echo $this->db->last_query(); die;
        return $query->row_array();
    }
    
    public function add($user_id, $token, $ig_user_id, $payload)
    {   
        $user_id = (int) $user_id;

        // $this->db->delete('tokens', array('user_id' => (int) $user_id));
        $this->delete($user_id);
        
        $data = array(
                'user_id' => $user_id,
                'token' => $token,
                'ig_user_id' => $ig_user_id,
                'payload' => $payload
            );
        $this->db->insert('access_tokens', $data);
        return $token . $user_id;
        
    }

    public function delete($user_id)
    {
        $this->db->delete('access_tokens', array('user_id' => (int) $user_id));
    }
}