<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Artist_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function record_count($category_id = FALSE) {
        if($category_id !== FALSE)
        {
            $this->db->where('category_id', (int) $category_id);
            $this->db->where('role_id', '3');
            $this->db->from('users');
            return $this->db->count_all_results();
        }
        else
        {
            $this->db->where('role_id', '3');
            $this->db->from('users');
            return $this->db->count_all_results();
        }
    }

    public function filter_record_count($artist_name = FALSE, $category = FALSE, $from = FALSE, $to = FALSE, $location = FALSE, $budget = FALSE, $genre_gospel = FALSE, $genre_hiphop = FALSE, $genre_rap = FALSE, $genre_afro_pop = FALSE, $genre_fuji = FALSE, $genre_r_and_b = FALSE, $genre_jazz = FALSE) {

        $sql = "SELECT COUNT(users.id) AS count FROM users LEFT OUTER JOIN rbt_analytics ON users.id = rbt_analytics.user_id WHERE (users.role_id = '3')";

        $where = '';

        if($artist_name !== FALSE)
        {
            $artist_name = '%' . filter_var($artist_name, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
            $where .= " AND ((users.full_name LIKE '". $artist_name . "') OR (users.stage_name LIKE '". $artist_name . "'))";
        }

        if($category !== FALSE)
        {
            $category = (int) $category;
            if($category != 0)
            {
                $where .= " AND (users.category_id = '". $category . "')";
            }
        }

        if($from !== FALSE && $to !== FALSE)
        {
            $from = (int) $from;
            $to = (int) $to;
            $where .= " AND (users.booking_cost >= '". $from . "' AND users.booking_cost <= '". $to . "')";
        }

        if($location !== FALSE)
        {
            $location = (int) $location;
            if($location != 0)
            {
                switch ($location) {
                    case 1:
                        $where .= " AND (rbt_analytics.count_lagos IS NOT NULL)";
                        break;
                    
                    case 2:
                        $where .= " AND (rbt_analytics.count_north1 IS NOT NULL)";
                        break;
                    
                    case 3:
                        $where .= " AND (rbt_analytics.count_north2 IS NOT NULL)";
                        break;
                    
                    case 4:
                        $where .= " AND (rbt_analytics.count_south_east IS NOT NULL)";
                        break;
                    
                    case 5:
                        $where .= " AND (rbt_analytics.count_south_south IS NOT NULL)";
                        break;
                    
                    case 6:
                        $where .= " AND (rbt_analytics.count_south_west IS NOT NULL)";
                        break;
                    
                    default:
                        # code...
                        break;
                }
            }
        }

        if($budget !== FALSE)
        {
            $budget = (int) $budget;
            if($budget != 0)
            {
                switch ($budget) {
                    case 1: // 100K TO 500K
                        $from = 100000;
                        $to = 500000;
                        $where .= " AND (users.booking_cost >= '". $from . "' AND users.booking_cost <= '". $to . "')";
                        break;
                    
                    case 2: // 500K TO 1M
                        $from = 500000;
                        $to = 1000000;
                        $where .= " AND (users.booking_cost >= '". $from . "' AND users.booking_cost <= '". $to . "')";
                        break;
                    
                    case 3: // 1M TO 3M
                        $from = 1000000;
                        $to = 3000000;
                        $where .= " AND (users.booking_cost >= '". $from . "' AND users.booking_cost <= '". $to . "')";
                        break;
                    
                    case 4: // 3M TO 5M
                        $from = 3000000;
                        $to = 5000000;
                        $where .= " AND (users.booking_cost >= '". $from . "' AND users.booking_cost <= '". $to . "')";
                        break;
                    
                    case 5: // 5M AND ABOVE
                        $from = 5000000;
                        $where .= " AND (users.booking_cost >= '". $from . "')";
                        break;
                    
                    default:
                        # nothing
                        break;
                }
            }
        }

        if($genre_gospel !== FALSE)
        {
            $genre_gospel = (int) $genre_gospel;
            if($genre_gospel != 0)
            {
                $where .= " AND (users.genre_gospel = '1')";
            }
        }

        if($genre_hiphop !== FALSE)
        {
            $genre_hiphop = (int) $genre_hiphop;
            if($genre_hiphop != 0)
            {
                $where .= " AND (users.genre_hiphop = '1')";
            }
        }

        if($genre_rap !== FALSE)
        {
            $genre_rap = (int) $genre_rap;
            if($genre_rap != 0)
            {
                $where .= " AND (users.genre_rap = '1')";
            }
        }

        if($genre_afro_pop !== FALSE)
        {
            $genre_afro_pop = (int) $genre_afro_pop;
            if($genre_afro_pop != 0)
            {
                $where .= " AND (users.genre_afro_pop = '1')";
            }
        }

        if($genre_fuji !== FALSE)
        {
            $genre_fuji = (int) $genre_fuji;
            if($genre_fuji != 0)
            {
                $where .= " AND (users.genre_fuji = '1')";
            }
        }

        if($genre_r_and_b !== FALSE)
        {
            $genre_r_and_b = (int) $genre_r_and_b;
            if($genre_r_and_b != 0)
            {
                $where .= " AND (users.genre_r_and_b = '1')";
            }
        }

        if($genre_jazz !== FALSE)
        {
            $genre_jazz = (int) $genre_jazz;
            if($genre_jazz != 0)
            {
                $where .= " AND (users.genre_jazz = '1')";
            }
        }
        
        $sql = $sql . $where;

        // echo $sql; die;

        $query = $this->db->query($sql);
        return $query->row_array()['count'];
    }

    public function filter($limit, $offset, $artist_name = FALSE, $category = FALSE, $from = FALSE, $to = FALSE, $location = FALSE, $budget = FALSE, $genre_gospel = FALSE, $genre_hiphop = FALSE, $genre_rap = FALSE, $genre_afro_pop = FALSE, $genre_fuji = FALSE, $genre_r_and_b = FALSE, $genre_jazz = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        $sql = "SELECT users.id, users.username, users.stage_name, users.image_url, users.first_name, users.last_name, categories_talent.category_name AS category_name, users.booking_cost  FROM users LEFT JOIN categories_talent ON users.category_id = categories_talent.id LEFT OUTER JOIN rbt_analytics ON users.id = rbt_analytics.user_id WHERE (users.role_id = '3') ";

        $where = '';

        if($artist_name !== FALSE)
        {
            $artist_name = '%' . filter_var($artist_name, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
            $where .= " AND ((users.full_name LIKE '". $artist_name . "') OR (users.stage_name LIKE '". $artist_name . "'))";
        }

        if($category !== FALSE)
        {
            $category = (int) $category;
            if($category != 0)
            {
                $where .= " AND (users.category_id = '". $category . "')";
            }
        }

        if($from !== FALSE && $to !== FALSE)
        {
            $from = (int) $from;
            $to = (int) $to;
            $where .= " AND (users.booking_cost >= '". $from . "' AND users.booking_cost <= '". $to . "')";
        }

        if($location !== FALSE)
        {
            $location = (int) $location;
            if($location != 0)
            {
                switch ($location) {
                    case 1:
                        $where .= " AND (rbt_analytics.count_lagos IS NOT NULL)";
                        break;
                    
                    case 2:
                        $where .= " AND (rbt_analytics.count_north1 IS NOT NULL)";
                        break;
                    
                    case 3:
                        $where .= " AND (rbt_analytics.count_north2 IS NOT NULL)";
                        break;
                    
                    case 4:
                        $where .= " AND (rbt_analytics.count_south_east IS NOT NULL)";
                        break;
                    
                    case 5:
                        $where .= " AND (rbt_analytics.count_south_south IS NOT NULL)";
                        break;
                    
                    case 6:
                        $where .= " AND (rbt_analytics.count_south_west IS NOT NULL)";
                        break;
                    
                    default:
                        # code...
                        break;
                }
            }
        }

        if($budget !== FALSE)
        {
            $budget = (int) $budget;
            if($budget != 0)
            {
                switch ($budget) {
                    case 1: // 100K TO 500K
                        $from = 100000;
                        $to = 500000;
                        $where .= " AND (users.booking_cost >= '". $from . "' AND users.booking_cost <= '". $to . "')";
                        break;
                    
                    case 2: // 500K TO 1M
                        $from = 500000;
                        $to = 1000000;
                        $where .= " AND (users.booking_cost >= '". $from . "' AND users.booking_cost <= '". $to . "')";
                        break;
                    
                    case 3: // 1M TO 3M
                        $from = 1000000;
                        $to = 3000000;
                        $where .= " AND (users.booking_cost >= '". $from . "' AND users.booking_cost <= '". $to . "')";
                        break;
                    
                    case 4: // 3M TO 5M
                        $from = 3000000;
                        $to = 5000000;
                        $where .= " AND (users.booking_cost >= '". $from . "' AND users.booking_cost <= '". $to . "')";
                        break;
                    
                    case 5: // 5M AND ABOVE
                        $from = 5000000;
                        $where .= " AND (users.booking_cost >= '". $from . "')";
                        break;
                    
                    default:
                        # nothing
                        break;
                }
            }
        }

        if($genre_gospel !== FALSE)
        {
            $genre_gospel = (int) $genre_gospel;
            if($genre_gospel != 0)
            {
                $where .= " AND (users.genre_gospel = '1')";
            }
        }

        if($genre_hiphop !== FALSE)
        {
            $genre_hiphop = (int) $genre_hiphop;
            if($genre_hiphop != 0)
            {
                $where .= " AND (users.genre_hiphop = '1')";
            }
        }

        if($genre_rap !== FALSE)
        {
            $genre_rap = (int) $genre_rap;
            if($genre_rap != 0)
            {
                $where .= " AND (users.genre_rap = '1')";
            }
        }

        if($genre_afro_pop !== FALSE)
        {
            $genre_afro_pop = (int) $genre_afro_pop;
            if($genre_afro_pop != 0)
            {
                $where .= " AND (users.genre_afro_pop = '1')";
            }
        }

        if($genre_fuji !== FALSE)
        {
            $genre_fuji = (int) $genre_fuji;
            if($genre_fuji != 0)
            {
                $where .= " AND (users.genre_fuji = '1')";
            }
        }

        if($genre_r_and_b !== FALSE)
        {
            $genre_r_and_b = (int) $genre_r_and_b;
            if($genre_r_and_b != 0)
            {
                $where .= " AND (users.genre_r_and_b = '1')";
            }
        }

        if($genre_jazz !== FALSE)
        {
            $genre_jazz = (int) $genre_jazz;
            if($genre_jazz != 0)
            {
                $where .= " AND (users.genre_jazz = '1')";
            }
        }
        
        $sql = $sql . $where . " ORDER BY users.stage_name LIMIT $offset, $limit";

        $query = $this->db->query($sql); //echo $sql; die;
        return $query->result_array();
    }

    public function get_all_slug()
    {
        $sql = "SELECT id, username FROM users ORDER BY username ASC";
        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function getRows($limit, $offset, $id = FALSE)
    {
        if ($id === FALSE)
        {   
            $limit = (int) $limit;
            $offset = (int) $offset;

            $sql = "SELECT users.id, users.stage_name, users.username, users.image_url, users.first_name, users.last_name, categories.name AS category_name, users.price, users.duration, (SELECT COUNT(id) FROM subscriptions WHERE subscriptions.product_id = users.id AND subscriptions.successful = 1) AS count_subscriptions, users.active  FROM users LEFT JOIN categories ON users.category_id = categories.id ORDER BY users.name ASC LIMIT $offset, $limit";

            $query = $this->db->query($sql);

            return $query->result_array();
        }
        $this->db->select('users.id, users.category_id, users.slug, users.name, users.slug, users.image_url, users.image_url_thumb, users.description, users.price, users.duration, users.mtn_service_code, users.mtn_service_keyword, users.airtel_service_code, users.airtel_service_keyword, users.glo_service_code, users.glo_service_keyword, users.emts_service_code, users.emts_service_keyword, users.ntel_service_code, users.ntel_service_keyword, users.active, categories.name AS category_name');
        $this->db->from('users'); 
        $this->db->join('categories', 'users.category_id = categories.id', 'left');
        $this->db->where('users.id', (int) $id); 
        $query = $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getRowsDropDown()
    {
        $this->db->select('id, stage_name, username');
        $this->db->from('users');
        $this->db->where('role_id', 3);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getAllRows()
    {
        $sql = "SELECT users.id, users.slug, users.image_url, users.image_url_thumb, users.name, categories.name AS category_name, users.price, users.duration, (SELECT COUNT(id) FROM subscriptions WHERE subscriptions.product_id = users.id AND subscriptions.successful = 1) AS count_subscriptions, users.active  FROM users LEFT JOIN categories ON users.category_id = categories.id WHERE users.role_id = '3' ORDER BY users.name ASC";

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function getRowsFrontEnd($limit, $offset, $category_id = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        if ($category_id === FALSE)
        {
            $sql = "SELECT users.id, users.username, users.stage_name, users.image_url, users.first_name, users.last_name, categories_talent.category_name AS category_name, users.booking_cost  FROM users LEFT JOIN categories_talent ON users.category_id = categories_talent.id WHERE users.role_id = 3 ORDER BY users.stage_name LIMIT $offset, $limit";

            $query = $this->db->query($sql); // var_dump($query); die();

            return $query->result_array();
        }

        $category_id = (int) $category_id;

        $sql = "SELECT users.id, users.slug, users.image_url, users.image_url_thumb, users.name, users.description, categories_talent.category_name AS category_name, users.price, users.duration, users.mtn_service_code, users.mtn_service_keyword, users.airtel_service_code, users.airtel_service_keyword, users.glo_service_code, users.glo_service_keyword, users.emts_service_code, users.emts_service_keyword, users.ntel_service_code, users.ntel_service_keyword, (SELECT COUNT(id) FROM subscriptions WHERE subscriptions.product_id = users.id AND subscriptions.successful = 1) AS count_subscriptions, users.active  FROM users LEFT JOIN categories_talent ON users.category_id = categories_talent.id WHERE users.category_id = '$category_id' AND users.active = 1 ORDER BY users.stage_name LIMIT $offset, $limit";
        $query = $this->db->query($sql); // var_dump($query); die();

        return $query->result_array();
    }

    public function getRowsFrontEndSingleCategory($limit, $offset, $category_id)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;
        $category_id = (int) $category_id;

        $sql = "SELECT users.id, users.slug, users.image_url, users.image_url_thumb, users.name, users.description, categories.name AS category_name, users.price, users.duration, users.mtn_service_code, users.mtn_service_keyword, users.airtel_service_code, users.airtel_service_keyword, users.glo_service_code, users.glo_service_keyword, users.emts_service_code, users.emts_service_keyword, users.ntel_service_code, users.ntel_service_keyword, (SELECT COUNT(id) FROM subscriptions WHERE subscriptions.product_id = users.id AND subscriptions.successful = 1) AS count_subscriptions, users.active  FROM users LEFT JOIN categories ON users.category_id = categories.id WHERE users.category_id = '$category_id' AND users.active = 1 ORDER BY users.name LIMIT $offset, $limit";
        $query = $this->db->query($sql); // var_dump($query); die();

        return $query->result_array();
    }

    public function add()
    {
        $created_by = (int) $this->session->userdata('user_id');

        $data = array(
            'category_id' => (int) $this->input->post('category_id'),
            'name' => trim($this->input->post('name')),
            'slug' => trim($this->input->post('slug')),
            'image_url' => $this->input->post('image_url'),
            'image_url_thumb' => $this->input->post('image_url_thumb'),
            'price' => (int) $this->input->post('price'),
            'duration' => trim($this->input->post('duration')),
            'description' => trim($this->input->post('description')),
            'mtn_service_code' => trim($this->input->post('mtn_service_code')),
            'mtn_service_keyword' => trim($this->input->post('mtn_service_keyword')),
            'airtel_service_code' => trim($this->input->post('airtel_service_code')),
            'airtel_service_keyword' => trim($this->input->post('airtel_service_keyword')),
            'glo_service_code' => trim($this->input->post('glo_service_code')),
            'glo_service_keyword' => trim($this->input->post('glo_service_keyword')),
            'emts_service_code' => trim($this->input->post('emts_service_code')),
            'emts_service_keyword' => trim($this->input->post('emts_service_keyword')),
            'ntel_service_code' => trim($this->input->post('ntel_service_code')),
            'ntel_service_keyword' => trim($this->input->post('ntel_service_keyword')),
            'created_by' => $created_by
        );

        $this->db->insert('users', $data);
    }

    public function delete($id)
    {
        $this->db->delete('users', array('id' => (int) $id));
    }

    public function update($id)
    {

        $data = array(
            'category_id' => (int) $this->input->post('category_id'),
            'name' => trim($this->input->post('name')),
            'image_url' => $this->input->post('image_url'),
            'image_url_thumb' => $this->input->post('image_url_thumb'),
            'price' => (int) $this->input->post('price'),
            'duration' => trim($this->input->post('duration')),
            'description' => trim($this->input->post('description')),
            'mtn_service_code' => trim($this->input->post('mtn_service_code')),
            'mtn_service_keyword' => trim($this->input->post('mtn_service_keyword')),
            'airtel_service_code' => trim($this->input->post('airtel_service_code')),
            'airtel_service_keyword' => trim($this->input->post('airtel_service_keyword')),
            'glo_service_code' => trim($this->input->post('glo_service_code')),
            'glo_service_keyword' => trim($this->input->post('glo_service_keyword')),
            'emts_service_code' => trim($this->input->post('emts_service_code')),
            'emts_service_keyword' => trim($this->input->post('emts_service_keyword')),
            'ntel_service_code' => trim($this->input->post('ntel_service_code')),
            'ntel_service_keyword' => trim($this->input->post('ntel_service_keyword'))
        );
        $this->db->where('id', (int) $id);
        $this->db->update('users', $data);
    }

    public function updateWithoutImage($id)
    {

        $data = array(
            'category_id' => (int) $this->input->post('category_id'),
            'name' => trim($this->input->post('name')),
            'price' => (int) $this->input->post('price'),
            'duration' => trim($this->input->post('duration')),
            'description' => trim($this->input->post('description')),
            'mtn_service_code' => trim($this->input->post('mtn_service_code')),
            'mtn_service_keyword' => trim($this->input->post('mtn_service_keyword')),
            'airtel_service_code' => trim($this->input->post('airtel_service_code')),
            'airtel_service_keyword' => trim($this->input->post('airtel_service_keyword')),
            'glo_service_code' => trim($this->input->post('glo_service_code')),
            'glo_service_keyword' => trim($this->input->post('glo_service_keyword')),
            'emts_service_code' => trim($this->input->post('emts_service_code')),
            'emts_service_keyword' => trim($this->input->post('emts_service_keyword')),
            'ntel_service_code' => trim($this->input->post('ntel_service_code')),
            'ntel_service_keyword' => trim($this->input->post('ntel_service_keyword'))
        );
        $this->db->where('id', (int) $id);
        $this->db->update('users', $data);
    }

    public function enable($id)
    {
        $data = array(
            'active' => 1
        );
        $this->db->where('id', (int) $id);
        $this->db->update('users', $data);
    }

    public function disable($id)
    {
        $data = array(
            'active' => 0
        );
        $this->db->where('id', (int) $id);
        $this->db->update('users', $data);
    }

    public function slugExists($slug, $id = FALSE)
    {
        if ($id === FALSE)
        {
            $this->db->select('id, slug');
            $this->db->from('users');
            $this->db->like('slug', $slug, 'after');
            $query = $this->db->order_by('id', 'DESC');
            $query = $this->db->limit(1);
            $query = $this->db->get();

            $row = $query->row_array();

            if(empty($row))
            {
                return false;
            }
            else
            {
                return $row['slug'];
            }
        }

        $this->db->select('id, slug');
        $this->db->from('users');
        $this->db->where('id !=', $id); 
        $this->db->like('slug', $slug, 'after');
        $query = $this->db->order_by('id', 'DESC');
        $query = $this->db->limit(1);
        $query = $this->db->get();

        $row = $query->row_array();

        if(empty($row))
        {
            return false;
        }
        else
        {
            return $row['slug'];
        }
    }
}