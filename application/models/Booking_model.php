<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Booking_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function today($type = "all", $user_id = FALSE)
    {
        if($user_id !== FALSE)
        {
            $user_id = (int) $user_id;

            if($type == "agent")
            {
                $sql = "SELECT COUNT(id) AS count FROM `bookings` WHERE DATE(`date_created`) = CURDATE() AND agent_id = '$user_id' AND bookings.external_booking != 1";
            }
            if($type == "talent")
            {
                $sql = "SELECT COUNT(id) AS count FROM `bookings` WHERE DATE(`date_created`) = CURDATE() AND talent_id = '$user_id' AND bookings.external_booking != 1";
            }

            $query = $this->db->query($sql);
            $row = $query->row_array();
            return $row['count'];
        }

        $sql = "SELECT COUNT(id) AS count FROM `bookings` WHERE DATE(`date_created`) = CURDATE() AND bookings.external_booking != 1";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        return $row['count'];
    }

    public function thisWeek($type = "all", $user_id = FALSE)
    {
        if($user_id !== FALSE)
        {
            $user_id = (int) $user_id;

            if($type == "agent")
            {
                $sql = "SELECT COUNT(id) AS count FROM `bookings` WHERE YEARWEEK(date_created) = YEARWEEK(CURRENT_TIMESTAMP) AND agent_id = '$user_id' AND bookings.external_booking != 1";
            }
            if($type == "talent")
            {
                $sql = "SELECT COUNT(id) AS count FROM `bookings` WHERE YEARWEEK(date_created) = YEARWEEK(CURRENT_TIMESTAMP) AND talent_id = '$user_id' AND bookings.external_booking != 1";
            }
            
            $query = $this->db->query($sql);
            $row = $query->row_array();
            return $row['count'];
        }

        $sql = "SELECT COUNT(id) AS count FROM `bookings` WHERE YEARWEEK(date_created) = YEARWEEK(CURRENT_TIMESTAMP) AND bookings.external_booking != 1";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        return $row['count'];
    }

    public function thisMonth($type = "all", $user_id = FALSE)
    {
        if($user_id !== FALSE)
        {
            $user_id = (int) $user_id;

            if($type == "agent")
            {
                $sql = "SELECT COUNT(id) AS count FROM `bookings` WHERE (MONTH(date_created) = MONTH(CURRENT_DATE) AND YEAR(date_created) = YEAR(CURRENT_DATE)) AND (agent_id = '$user_id') AND (bookings.external_booking != 1)";
            }
            if($type == "talent")
            {
                $sql = "SELECT COUNT(id) AS count FROM `bookings` WHERE (MONTH(date_created) = MONTH(CURRENT_DATE) AND YEAR(date_created) = YEAR(CURRENT_DATE)) AND (talent_id = '$user_id') AND (bookings.external_booking != 1)";
            }
            
            $query = $this->db->query($sql);
            $row = $query->row_array();
            return $row['count'];
        }

        $sql = "SELECT COUNT(id) AS count FROM `bookings` WHERE (MONTH(date_created) = MONTH(CURRENT_DATE) AND YEAR(date_created) = YEAR(CURRENT_DATE)) AND (bookings.external_booking != 1)";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        return $row['count'];
    }

    public function thisYear($type = "all", $user_id = FALSE)
    {
        if($user_id !== FALSE)
        {
            $user_id = (int) $user_id;

            if($type == "agent")
            {
                $sql = "SELECT COUNT(id) AS count FROM `bookings` WHERE YEAR(date_created) = YEAR(CURRENT_DATE) AND agent_id = '$user_id' AND bookings.external_booking != 1";
            }
            if($type == "talent")
            {
                $sql = "SELECT COUNT(id) AS count FROM `bookings` WHERE YEAR(date_created) = YEAR(CURRENT_DATE) AND talent_id = '$user_id' AND bookings.external_booking != 1";
            }
            
            $query = $this->db->query($sql);
            $row = $query->row_array();
            return $row['count'];
        }

        $sql = "SELECT COUNT(id) AS count FROM `bookings` WHERE YEAR(date_created) = YEAR(CURRENT_DATE) AND bookings.external_booking != 1";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        return $row['count'];
    }

    public function totalCount($type = "all", $user_id = FALSE)
    {
        if($user_id !== FALSE)
        {
            $user_id = (int) $user_id;

            if($type == "agent")
            {
                $sql = "SELECT COUNT(id) AS count FROM `bookings` WHERE agent_id = '$user_id' AND bookings.external_booking != 1";
            }
            if($type == "talent")
            {
                $sql = "SELECT COUNT(id) AS count FROM `bookings` WHERE talent_id = '$user_id' AND bookings.external_booking != 1";
            }
            
            $query = $this->db->query($sql);
            $row = $query->row_array();
            return $row['count'];
        }

        $sql = "SELECT COUNT(id) AS count FROM `bookings` WHERE bookings.external_booking != 1";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        return $row['count'];
    }

    public function paid($type = "all", $user_id = FALSE)
    {
        if($user_id !== FALSE)
        {
            $user_id = (int) $user_id;

            if($type == "agent")
            {
                $sql = "SELECT COUNT(id) AS count FROM `bookings` WHERE paid = '1' AND agent_id = '$user_id' AND bookings.external_booking != 1";
            }
            if($type == "talent")
            {
                $sql = "SELECT COUNT(id) AS count FROM `bookings` WHERE paid = '1' AND talent_id = '$user_id' AND bookings.external_booking != 1";
            }

            $query = $this->db->query($sql);
            $row = $query->row_array();
            return $row['count'];
        }

        $sql = "SELECT COUNT(id) AS count FROM `bookings` WHERE paid = '1' AND bookings.external_booking != 1";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        return $row['count'];
    }

    public function pending($type = "all", $user_id = FALSE) // i.e where booking_cost = 0
    {
        if($user_id !== FALSE)
        {
            $user_id = (int) $user_id;

            if($type == "agent")
            {
                $sql = "SELECT COUNT(id) AS count FROM `bookings` WHERE booking_cost = '0' AND agent_id = '$user_id' AND bookings.external_booking != 1";
            }
            if($type == "talent")
            {
                $sql = "SELECT COUNT(id) AS count FROM `bookings` WHERE booking_cost = '0' AND talent_id = '$user_id' AND bookings.external_booking != 1";
            }

            $query = $this->db->query($sql);
            $row = $query->row_array();
            return $row['count'];
        }

        $sql = "SELECT COUNT(id) AS count FROM `bookings` WHERE booking_cost = '0' AND bookings.external_booking != 1";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        return $row['count'];
    }

    public function upcoming($type = "all", $user_id = FALSE) // where event_date >= today
    {
        if($user_id !== FALSE)
        {
            $user_id = (int) $user_id;

            if($type == "agent")
            {
                $sql = "SELECT COUNT(id) AS count FROM `bookings` WHERE DATE(`event_date`) >= CURDATE() AND agent_id = '$user_id' AND bookings.external_booking != 1";
            }
            if($type == "talent")
            {
                $sql = "SELECT COUNT(id) AS count FROM `bookings` WHERE DATE(`event_date`) >= CURDATE() AND talent_id = '$user_id' AND bookings.external_booking != 1";
            }

            $query = $this->db->query($sql);
            $row = $query->row_array();
            return $row['count'];
        }

        $sql = "SELECT COUNT(id) AS count FROM `bookings` WHERE DATE(`event_date`) >= CURDATE() AND bookings.external_booking != 1";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        return $row['count'];
    }

    public function upcoming_paid($type = "all", $user_id = FALSE) // where event_date >= today and paid = 1
    {
        if($user_id !== FALSE)
        {
            $user_id = (int) $user_id;

            if($type == "agent")
            {
                $sql = "SELECT COUNT(id) AS count FROM `bookings` WHERE (DATE(`event_date`) >= CURDATE()) AND (paid = '1') AND (agent_id = '$user_id') AND (bookings.external_booking != 1)";
            }
            if($type == "talent")
            {
                $sql = "SELECT COUNT(id) AS count FROM `bookings` WHERE (DATE(`event_date`) >= CURDATE()) AND (paid = '1') AND (talent_id = '$user_id') AND (bookings.external_booking != 1)";
            }

            $query = $this->db->query($sql);
            $row = $query->row_array();
            return $row['count'];
        }

        $sql = "SELECT COUNT(id) AS count FROM `bookings` WHERE DATE(`event_date`) >= CURDATE() AND paid = '1' AND bookings.external_booking != 1";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        return $row['count'];
    }

    public function filter_record_count($artist_id = FALSE, $agent_id = FALSE, $start = FALSE, $end, $start2 = FALSE, $end2 = FALSE, $status = FALSE)
    {
        $this->db->reset_query();

        $sql = "SELECT COUNT(bookings.id) AS count FROM bookings LEFT JOIN users a ON bookings.agent_id = a.id LEFT JOIN users t ON bookings.talent_id = t.id WHERE (bookings.external_booking != 1)";

        $where = '';

        if($artist_id !== FALSE)
        {

            $artist_id = (int) $artist_id;
            if($artist_id != 0)
            {
                $where .= " AND (bookings.talent_id = '". $artist_id . "')";
            }
        }

        if($agent_id !== FALSE)
        {

            $agent_id = (int) $agent_id;
            if($agent_id != 0)
            {
                $where .= " AND (bookings.agent_id = '". $agent_id . "')";
            }
        }

        if($start !== FALSE && $end !== FALSE) // event_date
        {
            if(!empty($start2) && !empty($end2))
            {
                $start = filter_var($start, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
                $end = filter_var($end, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

                if($start == $end)
                {
                    $where .= " AND (DATE(bookings.event_date) = '". $start . "')";
                }
                else
                {
                    $where .= " AND ((DATE(bookings.event_date) >= '". $start . "') AND (DATE(bookings.event_date) <= '". $end . "'))";
                }
            }
        }

        if($start2 !== FALSE && $end2 !== FALSE) // date_booked i.e date_created
        {
            if(!empty($start2) && !empty($end2))
            {
                $start2 = filter_var($start2, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
                $end2 = filter_var($end2, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

                if($start2 == $end2)
                {
                    $where .= " AND (DATE(bookings.date_created) = '". $start2 . "')";
                }
                else
                {
                    $where .= " AND ((DATE(bookings.date_created) >= '". $start2 . "') AND (DATE(bookings.date_created) <= '". $end2 . "'))";
                }
            }
        }

        if($status !== FALSE)
        {

            $status = (int) $status;
            if($status == 1) // accepted
            {
                $where .= " AND (bookings.accepted = '1')";
            }
            elseif($status == 2) // rejected
            {
                $where .= " AND (bookings.rejected = '1')";
            }
            elseif($status == 3) // unpaid
            {
                $where .= " AND ((bookings.rejected != '1') AND (bookings.paid = '0'))";
            }
            elseif($status == 4) // paid
            {
                $where .= " AND ((bookings.paid = '1') AND (bookings.accepted = '0'))";
            }
            else
            {}
        }
        
        // $where = ltrim($where, ' AND');
        $sql = $sql . $where;

        // echo $sql; die;

        $query = $this->db->query($sql);
        return $query->row_array()['count'];
    }

    public function filter($limit, $offset, $artist_id = FALSE, $agent_id = FALSE, $start = FALSE, $end, $start2 = FALSE, $end2 = FALSE, $status = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        $sql = "SELECT bookings.id, bookings.talent_id, bookings.agent_id, bookings.event_name, bookings.event_date, bookings.event_start_time, bookings.event_end_time, bookings.content_brief, bookings.content_industry_id, bookings.content_budget_id, bookings.content_tier_id, bookings.content_campaign_duration_id, bookings.booking_cost, bookings.accepted, bookings.rejected, bookings.paid, bookings.payment_transaction_id, bookings.feedback, bookings.rating, bookings.external_booking, bookings.date_created, a.full_name, a.email AS agent_email, t.stage_name, t.username FROM bookings LEFT JOIN users a ON bookings.agent_id = a.id LEFT JOIN users t ON bookings.talent_id = t.id WHERE (bookings.external_booking != 1)";

        $where = '';

        if($artist_id !== FALSE)
        {

            $artist_id = (int) $artist_id;
            if($artist_id != 0)
            {
                $where .= " AND (bookings.talent_id = '". $artist_id . "')";
            }
        }

        if($agent_id !== FALSE)
        {

            $agent_id = (int) $agent_id;
            if($agent_id != 0)
            {
                $where .= " AND (bookings.agent_id = '". $agent_id . "')";
            }
        }

        if($start !== FALSE && $end !== FALSE) // event_date
        {
            if(!empty($start2) && !empty($end2))
            {
                $start = filter_var($start, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
                $end = filter_var($end, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

                if($start == $end)
                {
                    $where .= " AND (DATE(bookings.event_date) = '". $start . "')";
                }
                else
                {
                    $where .= " AND ((DATE(bookings.event_date) >= '". $start . "') AND (DATE(bookings.event_date) <= '". $end . "'))";
                }
            }
        }

        if($start2 !== FALSE && $end2 !== FALSE) // date_booked i.e date_created
        {
            if(!empty($start2) && !empty($end2))
            {
                $start2 = filter_var($start2, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
                $end2 = filter_var($end2, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

                if($start2 == $end2)
                {
                    $where .= " AND (DATE(bookings.date_created) = '". $start2 . "')";
                }
                else
                {
                    $where .= " AND ((DATE(bookings.date_created) >= '". $start2 . "') AND (DATE(bookings.date_created) <= '". $end2 . "'))";
                }
            }
        }

        if($status !== FALSE)
        {

            $status = (int) $status;
            if($status == 1) // accepted
            {
                $where .= " AND (bookings.accepted = '1')";
            }
            elseif($status == 2) // rejected
            {
                $where .= " AND (bookings.rejected = '1')";
            }
            elseif($status == 3) // unpaid
            {
                $where .= " AND ((bookings.rejected != '1') AND (bookings.paid = '0'))";
            }
            elseif($status == 4) // paid
            {
                $where .= " AND ((bookings.paid = '1') AND (bookings.accepted = '0'))";
            }
            else
            {}
        }
        
        // $where = ltrim($where, ' AND');
        $sql = $sql . $where . " ORDER BY bookings.date_created DESC LIMIT $offset, $limit";

        // echo $sql; die;

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function record_count($type = "all", $user_id = FALSE)
    {
        if($user_id !== FALSE)
        {
            if($type == "agent")
            {
                $this->db->where('agent_id', (int) $user_id);
            }
            if($type == "talent")
            {
                $this->db->where('talent_id', (int) $user_id);
            }
            $this->db->where('external_booking !=', 1);
            $this->db->from('bookings');
            return $this->db->count_all_results();
        }
        $this->db->where('external_booking !=', 1);
        return $this->db->count_all("bookings");
    }

    public function getRowsByAgentID($limit, $offset, $user_id)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;
        $user_id = (int) $user_id;

        $sql = "SELECT bookings.id, bookings.talent_id, bookings.event_name, bookings.event_date, bookings.event_start_time, bookings.event_end_time, bookings.content_brief, bookings.content_industry_id, bookings.content_budget_id, bookings.content_tier_id, bookings.content_campaign_duration_id, bookings.booking_cost, bookings.accepted, bookings.rejected, bookings.paid, bookings.payment_transaction_id, bookings.feedback, bookings.rating, bookings.external_booking, bookings.date_created , users.full_name AS full_name, users.stage_name, users.username FROM bookings LEFT JOIN users ON bookings.talent_id = users.id WHERE bookings.agent_id = '$user_id' AND bookings.external_booking != 1 ORDER BY bookings.date_created DESC LIMIT $offset, $limit";

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function getRowsByAgentIDUnpaid($limit, $offset, $user_id)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;
        $user_id = (int) $user_id;

        $sql = "SELECT bookings.id, bookings.talent_id, bookings.event_name, bookings.event_date, bookings.event_start_time, bookings.event_end_time, bookings.content_brief, bookings.content_industry_id, bookings.content_budget_id, bookings.content_tier_id, bookings.content_campaign_duration_id, bookings.booking_cost, bookings.accepted, bookings.rejected, bookings.paid, bookings.payment_transaction_id, bookings.feedback, bookings.rating, bookings.external_booking, bookings.date_created , users.full_name AS full_name, users.stage_name, users.username FROM bookings LEFT JOIN users ON bookings.talent_id = users.id WHERE (bookings.agent_id = '$user_id') AND (bookings.external_booking != 1) AND (bookings.paid = 0) ORDER BY bookings.date_created DESC LIMIT $offset, $limit";

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function getRowsAll($limit, $offset, $id = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        if($id === FALSE)
        {
            $sql = "SELECT bookings.id, bookings.talent_id, bookings.agent_id, bookings.event_name, bookings.event_date, bookings.event_start_time, bookings.event_end_time, bookings.content_brief, bookings.content_industry_id, bookings.content_budget_id, bookings.content_tier_id, bookings.content_campaign_duration_id, bookings.booking_cost, bookings.accepted, bookings.rejected, bookings.paid, bookings.payment_transaction_id, bookings.feedback, bookings.rating, bookings.external_booking, bookings.date_created, a.full_name, a.email AS agent_email, t.stage_name, t.username FROM bookings LEFT JOIN users a ON bookings.agent_id = a.id LEFT JOIN users t ON bookings.talent_id = t.id WHERE bookings.external_booking != 1 ORDER BY bookings.date_created DESC LIMIT $offset, $limit";

            $query = $this->db->query($sql);
            return $query->result_array();
        }

        $id = (int) $id;

        $sql = "SELECT bookings.id, bookings.talent_id, bookings.agent_id, bookings.event_name, bookings.event_date, bookings.event_start_time, bookings.event_end_time, bookings.event_location, bookings.event_description, bookings.content_brief, bookings.content_industry_id, bookings.content_budget_id, bookings.content_tier_id, bookings.content_campaign_duration_id, bookings.booking_cost, bookings.accepted, bookings.rejected, bookings.paid, bookings.payment_transaction_id, bookings.feedback, bookings.rating, bookings.external_booking, bookings.date_created, a.full_name, a.email AS agent_email, t.stage_name, t.username FROM bookings LEFT JOIN users a ON bookings.agent_id = a.id LEFT JOIN users t ON bookings.talent_id = t.id WHERE (bookings.external_booking != 1) AND (bookings.id = '$id') LIMIT 1";

        $query = $this->db->query($sql);
        return $query->row_array();
    }

    public function getRowsAllUnpaid($limit, $offset)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        $sql = "SELECT bookings.id, bookings.talent_id, bookings.event_name, bookings.event_date, bookings.event_start_time, bookings.event_end_time, bookings.booking_cost, bookings.accepted, bookings.rejected, bookings.paid, bookings.payment_transaction_id, bookings.feedback, bookings.rating, bookings.external_booking, bookings.date_created , a.full_name, t.stage_name, t.username FROM bookings LEFT JOIN users a ON bookings.agent_id = a.id LEFT JOIN users t ON bookings.talent_id = t.id WHERE bookings.external_booking != 1 AND bookings.paid = 0 ORDER BY bookings.date_created DESC LIMIT $offset, $limit";

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function getRowsByTalentID($limit, $offset, $user_id)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;
        $user_id = (int) $user_id;

        $sql = "SELECT bookings.id, bookings.talent_id, bookings.event_name, bookings.event_date, bookings.event_start_time, bookings.event_end_time, bookings.content_brief, bookings.content_industry_id, bookings.content_budget_id, bookings.content_tier_id, bookings.content_campaign_duration_id, bookings.booking_cost, bookings.accepted, bookings.rejected, bookings.paid, bookings.payment_transaction_id, bookings.feedback, bookings.rating, bookings.external_booking, bookings.date_created , users.full_name AS full_name, users.stage_name, users.username FROM bookings LEFT JOIN users ON bookings.agent_id = users.id WHERE bookings.talent_id = '$user_id' AND bookings.external_booking != 1 ORDER BY bookings.date_created DESC LIMIT $offset, $limit";

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function getRowsByAgentIDToday($limit, $offset, $user_id)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;
        $user_id = (int) $user_id;

        $sql = "SELECT bookings.id, bookings.talent_id, bookings.event_name, bookings.event_date, bookings.event_start_time, bookings.event_end_time, bookings.content_brief, bookings.content_industry_id, bookings.content_budget_id, bookings.content_tier_id, bookings.content_campaign_duration_id, bookings.booking_cost, bookings.accepted, bookings.rejected, bookings.paid, bookings.payment_transaction_id, bookings.feedback, bookings.rating, bookings.external_booking, bookings.date_created , users.full_name AS full_name, users.stage_name, users.username FROM bookings LEFT JOIN users ON bookings.talent_id = users.id WHERE (bookings.agent_id = '$user_id') AND (DATE(bookings.event_date) = CURDATE()) ORDER BY bookings.date_created DESC LIMIT $offset, $limit";

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function getRowsByTalentIDToday($limit, $offset, $user_id)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;
        $user_id = (int) $user_id;

        $sql = "SELECT bookings.id, bookings.talent_id, bookings.event_name, bookings.event_date, bookings.event_start_time, bookings.event_end_time, bookings.content_brief, bookings.content_industry_id, bookings.content_budget_id, bookings.content_tier_id, bookings.content_campaign_duration_id, bookings.booking_cost, bookings.accepted, bookings.rejected, bookings.paid, bookings.payment_transaction_id, bookings.feedback, bookings.rating, bookings.external_booking, bookings.date_created , users.full_name AS full_name, users.stage_name, users.username FROM bookings LEFT JOIN users ON bookings.talent_id = users.id WHERE (bookings.talent_id = '$user_id') AND (DATE(bookings.event_date) = CURDATE()) ORDER BY bookings.date_created DESC LIMIT $offset, $limit";

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function getRowsByAgentIDByDate($user_id, $date)
    {
        $user_id = (int) $user_id;
        $date = filter_var(strtolower(trim($date)), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

        $sql = "SELECT bookings.id, bookings.talent_id, bookings.event_name, bookings.event_date, bookings.event_start_time, bookings.event_end_time, bookings.content_brief, bookings.content_industry_id, bookings.content_budget_id, bookings.content_tier_id, bookings.content_campaign_duration_id, bookings.booking_cost, bookings.accepted, bookings.rejected, bookings.paid, bookings.payment_transaction_id, bookings.feedback, bookings.rating, bookings.external_booking, bookings.date_created , users.full_name AS full_name, users.stage_name, users.username FROM bookings LEFT JOIN users ON bookings.talent_id = users.id WHERE (bookings.agent_id = '$user_id') AND (DATE(bookings.event_date) = DATE('$date')) ORDER BY bookings.date_created DESC"; // var_dump($sql); die;

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function getRowsByTalentIDByDate($user_id, $date)
    {
        $user_id = (int) $user_id;
        $date = filter_var(strtolower(trim($date)), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

        $sql = "SELECT bookings.id, bookings.talent_id, bookings.event_name, bookings.event_date, bookings.event_start_time, bookings.event_end_time, bookings.content_brief, bookings.content_industry_id, bookings.content_budget_id, bookings.content_tier_id, bookings.content_campaign_duration_id, bookings.booking_cost, bookings.accepted, bookings.rejected, bookings.paid, bookings.payment_transaction_id, bookings.feedback, bookings.rating, bookings.external_booking, bookings.date_created , users.full_name AS full_name, users.stage_name, users.username FROM bookings LEFT JOIN users ON bookings.talent_id = users.id WHERE (bookings.talent_id = '$user_id') AND (DATE(bookings.event_date) = DATE('$date')) ORDER BY bookings.date_created DESC";

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function disableByDate($user_id, $date)
    {
        $user_id = (int) $user_id;
        $date = filter_var(strtolower(trim($date)), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

        $agent_id = 0;

        $data = array(
            'agent_id' => $agent_id,
            'talent_id' => $user_id,
            'event_name' => 'N/A',
            'event_date' => $date,
            'event_start_time' => 'N/A',
            'event_end_time' => 'N/A',
            'event_location' => 'N/A',
            'event_description' => 'N/A',
            'external_booking' => 1
        );

        $this->db->insert('bookings', $data);
    }

    public function enableByDate($user_id, $date)
    {
        $user_id = (int) $user_id;
        $date = filter_var(strtolower(trim($date)), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

        
        $this->db->delete('bookings', array('talent_id' => $user_id, 'event_date' => $date));
    }

    public function isNotAvailable($talent_id, $date)
    {
        $talent_id = (int) $talent_id;
        $date = filter_var(trim($date), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

        $this->db->select('id');
        $this->db->from('bookings');
        $this->db->where('talent_id', $talent_id); 
        $this->db->where('event_date', $date); 
        $query = $this->db->limit(1);
        $query = $this->db->get();
        $row = $query->row_array();
        if(empty($row))
        {
            // booking does not exist; hence available
            return false;
        }
        else
        {
            // booking exist; hence not available
            return true;
        }
    }

    public function makePayment($id, $transaction_id)
    {
        $id = (int) $id;
        $transaction_id = filter_var(trim($transaction_id), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

        $data = array(
            'payment_transaction_id' => $transaction_id,
            'paid' => 1
        );
        $this->db->where('id', $id);
        $this->db->update('bookings', $data);
    }

    public function accept($id)
    {
        $id = (int) $id;

        $data = array(
            'accepted' => 1
        );
        $this->db->where('id', $id);
        $this->db->update('bookings', $data);
    }


    public function getRows($id = FALSE)
    {
        if ($id === FALSE)
        {
            //$query = $this->db->get('dnd_bookings');
            //return $query->result_array();
            $query = $this->db->order_by('full_name', 'ASC');
            $this->db->select('bookings.id, bookings.first_name, bookings.last_name, bookings.full_name, bookings.external_booking, bookings.stage_name, bookings.email, bookings.username, bookings.gender, bookings.country_id, bookings.category_id, bookings.phone, bookings.city, bookings.landline, bookings.company, bookings.position, bookings.website, bookings.bio, bookings.bank_name, bookings.account_type, bookings.account_name, bookings.account_no, bookings.branch_code, bookings.image_url, bookings.instagram_url, bookings.twitter_url, bookings.facebook_url, bookings.booking_cost, bookings.last_login, roles.name AS role_name, countries.name AS country_name, categories_talent.category_name AS artist_category, categories_agent.category_name AS agent_category');
            $this->db->from('bookings'); 
            $this->db->join('roles', 'bookings.role_id = roles.id', 'left');
            $this->db->join('countries', 'bookings.country_id = countries.id', 'left');
            $this->db->join('categories_talent', 'bookings.category_id = categories_talent.id', 'left');
            $this->db->join('categories_agent', 'bookings.category_id = categories_agent.id', 'left');
            $query = $this->db->limit(20);
            $query = $this->db->get();

            return $query->result_array();
        }
        $this->db->select('bookings.id, bookings.first_name, bookings.last_name, bookings.full_name, bookings.external_booking, bookings.stage_name, bookings.email, bookings.username, bookings.gender, bookings.country_id, bookings.category_id, bookings.phone, bookings.city, bookings.landline, bookings.company, bookings.position, bookings.website, bookings.bio, bookings.bank_name, bookings.account_type, bookings.account_name, bookings.account_no, bookings.branch_code, bookings.image_url, bookings.instagram_url, bookings.twitter_url, bookings.facebook_url, bookings.booking_cost, bookings.last_login, roles.name AS role_name, countries.name AS country_name, categories_talent.category_name AS artist_category, categories_agent.category_name AS agent_category');
        $this->db->from('bookings'); 
        $this->db->join('roles', 'bookings.role_id = roles.id', 'left');
        $this->db->join('countries', 'bookings.country_id = countries.id', 'left');
        $this->db->join('categories_talent', 'bookings.category_id = categories_talent.id', 'left');
        $this->db->join('categories_agent', 'bookings.category_id = categories_agent.id', 'left');
        $this->db->where('bookings.id', $id); 
        $query = $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getRowByUsername($username)
    {
        $username = filter_var(strtolower(trim($username)), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

        $this->db->select('bookings.id, bookings.first_name, bookings.last_name, bookings.full_name, bookings.external_booking, bookings.stage_name, bookings.email, bookings.username, bookings.gender, bookings.country_id, bookings.category_id, bookings.phone, bookings.city, bookings.landline, bookings.company, bookings.position, bookings.website, bookings.bio, bookings.bank_name, bookings.account_type, bookings.account_name, bookings.account_no, bookings.branch_code, bookings.image_url, bookings.instagram_url, bookings.twitter_url, bookings.facebook_url, bookings.booking_cost, bookings.last_login, roles.name AS role_name, countries.name AS country_name, categories_talent.category_name AS artist_category, categories_agent.category_name AS agent_category');
        $this->db->from('bookings'); 
        $this->db->join('roles', 'bookings.role_id = roles.id', 'left');
        $this->db->join('countries', 'bookings.country_id = countries.id', 'left');
        $this->db->join('categories_talent', 'bookings.category_id = categories_talent.id', 'left');
        $this->db->join('categories_agent', 'bookings.category_id = categories_agent.id', 'left');
        $this->db->where('bookings.username', $username); 
        $query = $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function add()
    {
        $agent_id = (int) $this->session->userdata('user_id');

        $data = array(
            'agent_id' => $agent_id,
            'talent_id' => (int) $this->input->post('talent_id'),
            'event_name' => trim($this->input->post('event_name')),
            'event_date' => trim($this->input->post('event_date')),
            'event_start_time' => trim($this->input->post('event_start_time')),
            'event_end_time' => trim($this->input->post('event_end_time')),
            'event_location' => trim($this->input->post('event_location')),
            'event_description' => trim($this->input->post('event_description'))
        );

        $this->db->insert('bookings', $data);
    }

    public function addCC() // content creator
    {
        $agent_id = (int) $this->session->userdata('user_id');

        $data = array(
            'agent_id' => $agent_id,
            'talent_id' => (int) $this->input->post('talent_id'),
            'content_brief' => trim($this->input->post('content_brief')),
            'content_industry_id' => (int) trim($this->input->post('content_industry'))
        );

        $this->db->insert('bookings', $data);
    }

    public function addA() // amplifier
    {
        $agent_id = (int) $this->session->userdata('user_id');

        $data = array(
            'agent_id' => $agent_id,
            'talent_id' => (int) $this->input->post('talent_id'),
            'content_industry_id' => (int) trim($this->input->post('content_industry')),
            'content_budget_id' => (int) trim($this->input->post('content_budget')),
            'content_tier_id' => (int) trim($this->input->post('content_tier')),
            'content_campaign_duration_id' => (int) trim($this->input->post('content_campaign_duration'))
        );

        $this->db->insert('bookings', $data);
    }

    public function addSignup()
    {
        $role_id = (int) $this->input->post('role_id');
        $stage_name = NULL;
        if ($role_id == 3) // stage name for artist alone
        {
            $stage_name = trim($this->input->post('stage_name'));
        }

        $raw_password = $this->input->post('password');
        $password = password_hash($raw_password, PASSWORD_BCRYPT);

        $data = array(
            'role_id' => $role_id,
            'stage_name' => $stage_name,
            'first_name' => ucfirst(trim($this->input->post('first_name'))),
            'last_name' => ucfirst(trim($this->input->post('last_name'))),
            'full_name' => ucfirst(trim($this->input->post('first_name'))) . ' ' . ucfirst(trim($this->input->post('last_name'))),
            'email' => strtolower(trim($this->input->post('email'))),
            'username' => strtolower(trim($this->input->post('username'))),
            'category_id' => (int) $this->input->post('category_id'),
            'country_id' => (int) $this->input->post('country_id'),
            'password' => $password
        );

        $this->db->insert('bookings', $data);
    }

    public function delete($id)
    {
        $this->db->delete('bookings', array('id' => (int) $id));
    }

    public function deleteByAgentID($id)
    {
        $this->db->delete('bookings', array('id' => (int) $id, 'agent_id' => (int) $this->session->userdata("user_id")));
    }

    public function deleteByTalentID($id)
    {
        $this->db->delete('bookings', array('id' => (int) $id, 'talent_id' => (int) $this->session->userdata("user_id")));
    }

    public function updateProfile($id)
    {
        $data = array(
            'full_name' => ucwords(trim($this->input->post('full_name'))),
            'email' => strtolower(trim($this->input->post('email'))),
            'gender' => trim($this->input->post('gender')),
            'phone' => trim($this->input->post('phone')),
            'address' => trim($this->input->post('address'))
        );
        $this->db->where('id', $id);
        $this->db->update('bookings', $data);
    }

    public function updatePrice()
    {
        $data = array(
            'booking_cost' => (int) trim($this->input->post('booking_cost'))
        );
        $this->db->where('id', (int) trim($this->input->post('booking_id')));
        $this->db->update('bookings', $data);
    }

    public function update($id)
    {

        $data = array(
            'role_id' => (int) $this->input->post('role_id'),
            'full_name' => ucwords(trim($this->input->post('full_name'))),
            'email' => strtolower(trim($this->input->post('email'))),
            'phone' => trim($this->input->post('phone')),
            'gender' => trim($this->input->post('gender')),
            'address' => trim($this->input->post('address'))
        );
        $this->db->where('id', (int) $id);
        $this->db->update('bookings', $data);
    }
}