<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Category_Talent_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function record_count() {
        return $this->db->count_all("categories_talent");
    }

    public function getRows($id = FALSE)
    {
        if ($id === FALSE)
        {
            $this->db->order_by('category_name', 'ASC');
            $this->db->select('id, category_name');
            $this->db->from('categories_talent'); 
            $query = $this->db->get();
            
            return $query->result_array();
        }
        $this->db->select('category_name');
        $this->db->from('categories_talent');
        $this->db->where('id', $id); 
        $query = $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function add()
    {
        $created_by = (int) $this->session->userdata('user_id');

        $data = array(
            'category_name' => trim($this->input->post('name')),
            'created_by' => $created_by
        );

        $this->db->insert('categories_talent', $data);
    }

    public function delete($id)
    {
        $this->db->delete('categories_talent', array('id' => (int) $id));
    }

    public function update($id)
    {

        $data = array(
            'category_name' => trim($this->input->post('name'))
        );
        $this->db->where('id', (int) $id);
        $this->db->update('categories_talent', $data);
    }
}