<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Notification_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function record_count($user_id)
    {
        $this->db->where('user_id', (int) $user_id);
        $this->db->from('notifications');
        return $this->db->count_all_results();
    }

    public function getRows($limit, $offset, $user_id)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;
        $user_id = (int) $user_id;

        $sql = "SELECT notifications.id, notifications.user_id, notifications.message, notifications.is_read, notifications.date_created FROM notifications WHERE notifications.user_id = '$user_id' ORDER BY notifications.date_created DESC LIMIT $offset, $limit";

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function getRowsUnRead($user_id)
    {
        $query = $this->db->order_by('notifications.date_created', 'DESC');
        $this->db->select('notifications.id, notifications.user_id, notifications.message, notifications.is_read, notifications.date_created');
        $this->db->from('notifications'); 
        $this->db->where('notifications.user_id', (int) $user_id); 
        $this->db->where('notifications.is_read', 0); 
        $query = $this->db->limit(25);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function markRead($user_id)
    {
        $data = array(
            'is_read' => 1,
        );
        $this->db->where('user_id', (int) $user_id);
        $this->db->update('notifications', $data);
    }

    public function add($user_id, $message)
    {
        $data = array(
            'user_id' => (int) $user_id,
            'message' => trim($message)
        );

        $this->db->insert('notifications', $data);
    }



















    public function getRowByTalentID($id)
    {
        $this->db->select('notifications.id, users.full_name, users.username, users.stage_name, notifications.user_id, notifications.count_lagos, notifications.count_north1, notifications.count_north2, notifications.count_south_east, notifications.count_south_south, notifications.count_south_west, notifications.count_total');
        $this->db->from('notifications'); 
        $this->db->join('users', 'notifications.user_id = users.id', 'left');
        $this->db->where('notifications.user_id', (int) $id); 
        $query = $this->db->limit(1);
        $query = $this->db->get();    //echo $this->db->last_query(); die;
        return $query->row_array();
    }

    public function update($id)
    {
        $count_lagos = (int) $this->input->post('count_lagos');
        $count_north1 = (int) $this->input->post('count_north1');
        $count_north2 = (int) $this->input->post('count_north2');
        $count_south_east = (int) $this->input->post('count_south_east');
        $count_south_south = (int) $this->input->post('count_south_south');
        $count_south_west = (int) $this->input->post('count_south_west');
        $count_total = $count_lagos + $count_north1 + $count_north2 + $count_south_east + $count_south_south + $count_south_west;

        $data = array(
            'count_lagos' => $count_lagos,
            'count_north1' => $count_north1,
            'count_north2' => $count_north2,
            'count_south_east' => $count_south_east,
            'count_south_south' => $count_south_south,
            'count_south_west' => $count_south_west,
            'count_total' => $count_total,
        );
        $this->db->where('id', (int) $id);
        $this->db->update('notifications', $data);
    }

    public function delete($id)
    {
        $this->db->delete('notifications', array('id' => (int) $id));
    }
}