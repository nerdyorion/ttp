<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Subscription_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function today($success = FALSE)
    {
        if($success !== FALSE)
        {
            $sql = "SELECT COUNT(id) AS count FROM `subscriptions` WHERE DATE(`date_created`) = CURDATE() AND successful = '$success'";
            $query = $this->db->query($sql);
            $row = $query->row_array();
            return $row['count'];
        }

        $sql = "SELECT COUNT(id) AS count FROM `subscriptions` WHERE DATE(`date_created`) = CURDATE()";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        return $row['count'];
    }

    public function record_count($success = FALSE) {
        if($success !== FALSE)
        {
            $this->db->where('successful', (int) $success);
            $this->db->from('subscriptions');
            return $this->db->count_all_results();
        }
        return $this->db->count_all("subscriptions");
    }

    public function record_count_filter($products, $start, $end, $msisdn, $successful) {
        if( (empty($products)) && (empty($start)) && (empty($end)) && (empty($msisdn)) && ($successful == '-1') )
        {
            return $this->db->count_all("subscriptions");
        }
        else
        {
            $sql = "SELECT count(subscriptions.id) AS count FROM subscriptions LEFT JOIN products ON subscriptions.product_id = products.id WHERE";

            if(!empty($products))
            {
                $sql .= " (subscriptions.product_id IN ($products)) AND";
            }
            
            if(!empty($start) && !empty($end))
            {
                $start = $start . ' 00:00:00';
                $end = $end . ' 23:59:59';

                $sql .= " (subscriptions.date_created > '$start' AND subscriptions.date_created < '$end') AND";
            }

            if(!empty($msisdn))
            {
                $sql .= " (subscriptions.phone = '$msisdn') AND";
            }
            
            if($successful != '-1')
            {
                $sql .= " (subscriptions.successful = '$successful') AND";
            }
            $sql = rtrim(trim($sql), 'AND');  // var_dump($sql); die();

            $query = $this->db->query($sql);

            $row = $query->row_array();
            return $row['count'];
        }
    }

    public function getRows($limit, $offset, $success = FALSE, $id = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        if ($id !== FALSE)
        {
            $id = (int) $id;

            $sql = "SELECT subscriptions.id, subscriptions.product_id, products.name, subscriptions.phone, subscriptions.via, subscriptions.successful, subscriptions.error_reason, subscriptions.date_created, products.active  FROM subscriptions LEFT JOIN products ON subscriptions.product_id = products.id WHERE subscriptions.id = '$id'";

            $query = $this->db->query($sql); // var_dump($query); die();

            return $query->result_array();
        }
        if ($success !== FALSE)
        {
            $success = (int) $success;

            $sql = "SELECT subscriptions.id, subscriptions.product_id, products.name, subscriptions.phone, subscriptions.via, subscriptions.successful, subscriptions.error_reason, subscriptions.date_created, products.active  FROM subscriptions LEFT JOIN products ON subscriptions.product_id = products.id WHERE subscriptions.successful = '$success' ORDER BY subscriptions.id DESC LIMIT $offset, $limit";

            $query = $this->db->query($sql); // var_dump($query); die();

            return $query->result_array();
        }

        $sql = "SELECT subscriptions.id, subscriptions.product_id, products.name, subscriptions.phone, subscriptions.via, subscriptions.successful, subscriptions.error_reason, subscriptions.date_created, products.active  FROM subscriptions LEFT JOIN products ON subscriptions.product_id = products.id ORDER BY subscriptions.id DESC LIMIT $offset, $limit";

        $query = $this->db->query($sql); // var_dump($query); die();

        return $query->result_array();
    }

    public function getRowsFilter($limit, $offset, $products, $start, $end, $msisdn, $successful)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;
        if( (empty($products)) && (empty($start)) && (empty($end)) && (empty($msisdn)) && ($successful == '-1') )
        {
            $sql = "SELECT subscriptions.id, subscriptions.product_id, products.name, subscriptions.phone, subscriptions.via, subscriptions.successful, subscriptions.error_reason, subscriptions.date_created, products.active  FROM subscriptions LEFT JOIN products ON subscriptions.product_id = products.id ORDER BY subscriptions.id DESC LIMIT $offset, $limit";

            $query = $this->db->query($sql); // var_dump($query); die();

            return $query->result_array();
        }
        else
        {
            $sql = "SELECT subscriptions.id, subscriptions.product_id, products.name, subscriptions.phone, subscriptions.via, subscriptions.successful, subscriptions.error_reason, subscriptions.date_created, products.active  FROM subscriptions LEFT JOIN products ON subscriptions.product_id = products.id WHERE";

            if(!empty($products))
            {
                $sql .= " (subscriptions.product_id IN ($products)) AND";
            }

            if(!empty($start) && !empty($end))
            {
                $start = $start . ' 00:00:00';
                $end = $end . ' 23:59:59';

                $sql .= " (subscriptions.date_created >= '$start' AND subscriptions.date_created <= '$end') AND";
            }

            if(!empty($msisdn))
            {
                $sql .= " (subscriptions.phone = '$msisdn') AND";
            }

            if($successful != '-1')
            {
                $sql .= " (subscriptions.successful = '$successful') AND";
            }
                
            $sql = rtrim(trim($sql), 'AND');

            $sql .= " ORDER BY subscriptions.id DESC LIMIT $offset, $limit;";  // var_dump($sql); die();

            $query = $this->db->query($sql);

            return $query->result_array();
        }
    }

    public function add($product_id, $phone, $via, $successful, $error_reason)
    {
        $data = array(
            'product_id' => (int) $product_id,
            'phone' => trim($phone),
            'via' => trim($via),
            'successful' => (int) $successful,
            'error_reason' => trim($error_reason)
        );

        $this->db->insert('subscriptions', $data);
    }

    public function delete($id)
    {
        $this->db->delete('subscriptions', array('id' => (int) $id));
    }

    public function deleteProductSubscription($product_id)
    {
        $this->db->delete('subscriptions', array('product_id' => (int) $product_id));
    }
}