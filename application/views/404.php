  <style type="text/css">
    del:after {
      border-color: #F05522;
    }
  </style>
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <!-- <img src="https://placehold.it/1200x400?text=IMAGE" alt="Image"> -->
        <img src="assets/images/register-banner.png" alt="Image" />
        <div class="carousel-caption">
          <h3>US Uber</h3>
          <!-- <p style="color: #111;">daily...</p> -->
          <p>Book Artists, Artist? Get Booked...</p>
        </div>      
      </div>

    </div>
    
    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>

  <div style="width: 70%; margin: auto;">

    <div class="clearfix"></div>

    <div class="container-fluid text-center bg-grey">
      <!-- <h2>MOBILE CONTENTS</h2> -->
      <!-- <h4>What we have created</h4> -->
      <div class="row text-center">
          <section id="wrapper" class="error-page">
            <div class="error-box">
              <div class="error-body text-center">
                <h1>404</h1>
                <h3 class="text-uppercase">Page Not Found !</h3>
                <p class="text-muted m-t-30 m-b-30">YOU SEEM TO BE TRYING TO FIND YOUR WAY HOME</p>
                <a href="<?php echo base_url(); ?>" class="btn btn-primary btn-rounded waves-effect waves-light m-b-40">Back to home</a> </div>
              </div>
            </section>
        </div>

      </div>

      <div class="clearfix"></div>

    </div>


    <?php $this->load->view('footer'); echo "\n"; ?>

  </body>
  </html>