<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<base href="<?php echo base_url();?>">
<link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.ico">
<title>404 | <?php echo $this->config->item('app_name'); ?></title>
<!-- Bootstrap CSS -->
<link href="assets/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shiv and Respond.js IE8 support -->
<!--[if lt IE 9]>
        <script src="assets/https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="assets/https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
</head>
<body >
<section id="wrapper" class="error-page">
  <div class="error-box">
    <div class="error-body text-center">
      <h1>404</h1>
      <h3 class="text-uppercase">Page Not Found !</h3>
      <p class="text-muted m-t-30 m-b-30">YOU SEEM TO BE TRYING TO FIND YOUR WAY HOME</p>
      <a href="<?php echo base_url() . "dashboard/"; ?>" class="btn btn-primary btn-rounded waves-effect waves-light m-b-40">Back to home</a> </div>
    <footer class="footer text-center"><?php echo date('Y'); ?> &copy; Credits: <a href="http://ciitmaur.com" target="_blank">Ciitmaur</a>.</footer>
  </div>
</section>
</body>

</html>
