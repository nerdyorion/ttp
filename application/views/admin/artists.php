  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Artists</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "admin123/"; ?>">Dashboard</a></li>
            <li class="active">Artists</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- row -->
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
          <div class="white-box">
            <!--<h3>Artists</h3>-->
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#all" aria-controls="all" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-view-list"></i></span><span class="hidden-xs"> View All</span></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="all"> 
                <div class="col-md-12">
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Stage Name</th>
                          <th>Email</th>
                          <th>Category</th>
                          <th>Booking Cost</th>
                          <th>Gender</th>
                          <th>Last Login</th>
                          <th class="text-nowrap">Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php if(empty($rows)): ?>
                        <tr>
                          <td colspan="6" align="center">No data returned.</td>
                        </tr>
                      <?php else: ?>
                      <?php $sn = 1; foreach ($rows as $row): ?>
                        <tr>
                          <td><?php echo $sn++; ?></td>
                          <td><a href="<?php echo base_url() . '@' . $row['username']; ?>" data-toggle="tooltip" data-original-title="View <?php echo $row['stage_name']; ?>" target="_blank"><?php echo $row['stage_name']; ?></a></td>
                          <td><a href="mailto:<?php echo $row['email']; ?>"><?php echo $row['email']; ?></a></td>
                          <td><?php echo dashIfEmpty($row['category_name']); ?>  <?php echo strtoupper($row['category_name']) == "SOCIAL INFLUENCER" ? '(' . $row['artist_category_social_influencer'] . ')' : ''; ?></td>
                          <td><?php echo number_format($row['booking_cost']); ?></td>
                          <td><?php echo empty($row['gender']) ? '-' : $row['gender']; ?></td>
                          <td><?php echo $row['last_login'] == "0000-00-00 00:00:00" || $row['last_login'] == "" ? "-" : date('M d, Y h:i A', strtotime($row['last_login'])); ?></td>
                          <!--<td><?php echo date('M d, Y h:i A', strtotime($row['last_login'])) == "Jan 01, 1970 12:00 AM" ? "-" : date('M d, Y h:i A', strtotime($row['last_login'])); ?></td>-->
                          <td class="text-nowrap">
                            <!-- <a href="admin123/users/edit/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>  -->
                            <a href="admin123/users/delete/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Delete"> <span class="text-danger"><i class="fa fa-close text-danger m-r-10" onclick="if(confirm('Are you sure you want to delete this record?')) return true; else return false;"></i></span> </a> 
                            <a href="admin123/artists/switchto/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Manage Account"> <i class="fa fa-sign-in text-inverse m-r-10"></i> </a> 
                            <!-- <a href="admin123/users/view/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="View"> <i class="fa fa-folder-open text-inverse m-r-10"></i> </a>  -->
                            <span data-toggle="modal" data-target="#updateBookingCostModal" class="open-booking-cost-dialog" data-id="<?php echo $row['id']; ?>" data-artist-id="<?php echo $row['id']; ?>"><a href="javascript:void(0);" data-toggle="tooltip" data-original-title="Update Booking Cost"> <i class="fa fa-money text-inverse m-r-10"></i> </a></span>
                            <?php if($row['featured'] == 0): ?>
                              <a href="admin123/artists/feature/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Set as Featured"> <i class="fa fa-trophy text-inverse m-r-10"></i> </a> 
                            <?php else: ?>
                              <a href="admin123/artists/unfeature/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Remove Featured"> <i class="fa fa-trophy fa-rotate-90 text-inverse m-r-10"></i> </a> 
                            <?php endif; ?>
                            <span data-toggle="modal" data-target="#addRBTDataModal" class="open-rbt-dialog" data-id="<?php echo $row['id']; ?>" data-artist-id="<?php echo $row['id']; ?>"><a href="javascript:void(0);" data-toggle="tooltip" data-original-title="Add RBT Analytics Data"> <i class="fa fa-bar-chart text-inverse m-r-10"></i> </a></span>
                          </td>
                        </tr>
                      <?php endforeach; ?>
                      <?php endif; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
                <!--
                <div class="col-md-3 pull-right">
                  <p><a href="?page=2">Pagination</a>.</p>
                </div>
                -->
                <!-- Modal -->
                <div id="addRBTDataModal" class="modal fade" role="dialog">
                  <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                      <?php echo form_open('/admin123/artists/addUpdateRBT', 'class="form-horizontal", role="form"'); ?>
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add / Update RBT Analytics Data</h4>
                      </div>
                      <div class="modal-body white-box">
                        <!-- <p>Some text in the modal.</p> -->
                        <div class="form-group">
                          <label for="count_lagos" class="col-sm-3 control-label">Lagos: <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                            <input type="number" class="form-control" name="count_lagos" min="0" id="count_lagos" value="" placeholder="" required="required">
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="count_north1" class="col-sm-3 control-label">North 1: <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                            <input type="number" class="form-control" name="count_north1" min="0" id="count_north1" value="" placeholder="" required="required">
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="count_north2" class="col-sm-3 control-label">North 2: <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                            <input type="number" class="form-control" name="count_north2" min="0" id="count_north2" value="" placeholder="" required="required">
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="count_south_east" class="col-sm-3 control-label">South East: <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                            <input type="number" class="form-control" name="count_south_east" min="0" id="count_south_east" value="" placeholder="" required="required">
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="count_south_south" class="col-sm-3 control-label">South South: <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                            <input type="number" class="form-control" name="count_south_south" min="0" id="count_south_south" value="" placeholder="" required="required">
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="count_south_west" class="col-sm-3 control-label">South West: <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                            <input type="number" class="form-control" name="count_south_west" min="0" id="count_south_west" value="" placeholder="" required="required">
                          </div>
                        </div>

                        <input type="hidden" name="user_id" id="user_id" value="" />
                      </div>
                      <div class="modal-footer">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-save" aria-hidden="true"></i> Submit</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>

                <!-- Modal -->
                <div id="updateBookingCostModal" class="modal fade" role="dialog">
                  <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                      <?php echo form_open('/admin123/artists/updateBookingCost', 'class="form-horizontal", role="form"'); ?>
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Update Booking Cost</h4>
                      </div>
                      <div class="modal-body white-box">
                        <!-- <p>Some text in the modal.</p> -->
                        <div class="form-group">
                          <label for="booking_cost" class="col-sm-3 control-label">Price: <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                            <input type="number" class="form-control" name="booking_cost" min="0" id="booking_cost" value="" placeholder="" required="required">
                          </div>
                        </div>

                        <input type="hidden" name="user_id" id="user_id2" value="" />
                      </div>
                      <div class="modal-footer">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-save" aria-hidden="true"></i> Submit</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>


                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
      </div> 
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_admin') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n";  // load footer view ?>
<script src="assets/js/jasny-bootstrap.js"></script>
<script type="text/javascript">
  $(document).on("click", ".open-rbt-dialog", function () {
   var user_id = $(this).data('id');
   $(".modal-body #user_id").val( user_id );
   });

  $(document).on("click", ".open-booking-cost-dialog", function () {
   var user_id = $(this).data('id');
   $(".modal-body #user_id2").val( user_id );
   });


  function validate()
  {
    var password = document.getElementById("password").value;
    var confirm_password = document.getElementById("confirm_password").value;
    var gender = document.getElementById("gender").value;
    var role_id = document.getElementById("role_id").value;
    if(password != confirm_password ){
      alert('Passwords do not match.');
      document.getElementById("confirm_password").focus();
      return false;
    }
    else if(gender == 0 ){
      alert('Please specify gender.');
      return false;
    }
    else if(role_id == 0 ){
      alert('Please specify role.');
      return false;
    }
    else {
      return true;
    }
  }
</script>

</body>
</html>