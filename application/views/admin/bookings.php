  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Bookings</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "admin123/"; ?>">Dashboard</a></li>
            <li class="active">Bookings</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- row -->
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
          <div class="white-box">
            <!--<h3>Artists</h3>-->
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#all" aria-controls="all" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-view-list"></i></span><span class="hidden-xs"> View All</span></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="all"> 
                <div class="col-md-12">
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Agent</th>
                          <th>Event</th>
                          <th>Date</th>
                          <th>Artist</th>
                          <th>Booking Cost (N)</th>
                          <th>Status</th>
                          <th class="text-nowrap">Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php if(empty($rows)): ?>
                        <tr>
                          <td colspan="6" align="center">No data returned.</td>
                        </tr>
                      <?php else: ?>
                      <?php $sn = 1; foreach ($rows as $row): ?>
                        <tr>
                          <td><?php echo $sn++; ?></td>
                          <td title="<?php echo $row['full_name']; ?>"><?php echo $row['full_name']; ?></td>
                          <td title="<?php echo dashIfEmpty($row['event_name']); ?>"><?php echo dashIfEmpty($row['event_name']); ?></td>
                          <td title="<?php echo $row['event_date'] . ' ' . $row['event_start_time'] . '-' . $row['event_end_time']; ?>"><?php echo $row['event_date'] . ' ' . $row['event_start_time'] . '-' . $row['event_end_time']; ?></td>
                          <td><a href="<?php echo base_url() . '@' . $row['username']; ?>" data-toggle="tooltip" data-original-title="View <?php echo $row['stage_name']; ?>" target="_blank"><?php echo $row['stage_name']; ?></a></td>
                          <td title="<?php echo $row['booking_cost'] == 0 ? 'Pending' : number_format($row['booking_cost']); ?>"><?php echo $row['booking_cost'] == 0 ? '<a href="javascript:void(0);" class="bg-warning biga" style="color: #ffffff;">&nbsp;Pending&nbsp;</a>' : number_format($row['booking_cost']); ?></td>
                          <td>
                            <?php echo $row['accepted'] == 1 ? '<a href="javascript:void(0);" class="bg-success biga" style="color: #ffffff;">&nbsp;Accepted&nbsp;</a>' : ''; ?> 
                            <?php echo $row['rejected'] == 1 ? '<a href="javascript:void(0);" class="bg-danger biga" style="color: #ffffff;">&nbsp;Rejected&nbsp;</a>' : ''; ?> 
                            <?php echo $row['rejected'] != 1 && $row['paid'] == 0 ? '<a href="javascript:void(0);" class="bg-danger biga" style="color: #ffffff;">&nbsp;Unpaid&nbsp;</a>' : ''; ?>
                            <?php echo $row['paid'] == 1 && $row['accepted'] == 0 ? '<a href="javascript:void(0);" class="bg-info biga" style="color: #ffffff;">&nbsp;Paid&nbsp;</a>' : ''; ?>
                          </td>
                          <td class="text-nowrap">
                            <a href="admin123/users/delete/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Delete"> <span class="text-danger"><i class="fa fa-close text-danger m-r-10" onclick="if(confirm('Are you sure you want to delete this record?')) return true; else return false;"></i></span> </a> 
                            <a href="admin123/bookings/view/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="View"> <i class="fa fa-folder-open text-inverse m-r-10"></i> </a> 
                            <?php if($row['paid'] == 0): ?>
                              <span data-toggle="modal" data-target="#setPriceModal" class="open-price-dialog" data-id="<?php echo $row['id']; ?>"><a href="javascript:void(0);" data-toggle="tooltip" data-original-title="Set Agreed Price"> <i class="fa fa-money text-inverse m-r-10"></i> </a></span>
                            <?php else: ?>
                              <span data-toggle="modal" data-target="#confirmPaymentModal" class="open-payment-dialog" data-id="<?php echo $row['id']; ?>" data-transaction-id="<?php echo $row['payment_transaction_id']; ?>"><a href="javascript:void(0);" data-toggle="tooltip" data-original-title="Confirm Payment"> <i class="fa fa-check-circle text-inverse m-r-10"></i> </a></span>
                            <?php endif; ?>
                          </td>
                        </tr>
                      <?php endforeach; ?>
                      <?php endif; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
                <!--
                <div class="col-md-3 pull-right">
                  <p><a href="?page=2">Pagination</a>.</p>
                </div>
                -->

      <!-- Modal -->
      <div id="setPriceModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <?php echo form_open('/admin123/bookings/updatePrice', 'class="form-horizontal", role="form"'); ?>
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Set Agreed Price</h4>
            </div>
            <div class="modal-body white-box">
              <!-- <p>Some text in the modal.</p> -->
              <div class="form-group">
                <label for="booking_cost" class="col-sm-2 control-label">Price: <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                      <input type="number" class="form-control" name="booking_cost" min="0" step="5000" id="booking_cost" value="" placeholder="" required="required">
                      <input type="hidden" name="booking_id" id="booking_id" value="" />
                    </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-primary"><i class="fa fa-save" aria-hidden="true"></i> Submit</button>
              <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>
            </div>
          </form>
        </div>

      </div>
    </div>

      <!-- Modal -->
      <div id="confirmPaymentModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <?php echo form_open('/admin123/bookings/accept', 'class="form-horizontal", role="form"'); ?>
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Confirm Payment &amp; Approve Booking</h4>
            </div>
            <div class="modal-body white-box">
              <!-- <p>Some text in the modal.</p> -->
              <div class="form-group">
                <label for="payment_transaction_id" class="col-sm-2 control-label">Transaction ID: <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" name="payment_transaction_id" id="payment_transaction_id" value="" placeholder="" required="required" disabled="disabled">
                      <input type="hidden" name="booking_id" id="booking_id" value="" />
                    </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-primary"><i class="fa fa-save" aria-hidden="true"></i> Submit</button>
              <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>
            </div>
          </form>
        </div>

      </div>
    </div>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
      </div> 
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_admin') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n";  // load footer view ?>
<script src="assets/js/jasny-bootstrap.js"></script>
<script type="text/javascript">
  $(document).on("click", ".open-price-dialog", function () {
   var booking_id = $(this).data('id');
   $(".modal-body #booking_id").val( booking_id );
   });

  $(document).on("click", ".open-payment-dialog", function () {
   var booking_id = $(this).data('id');
   var payment_transaction_id = $(this).data('transaction-id');
   $(".modal-body #booking_id").val( booking_id );
   $(".modal-body #payment_transaction_id").val( payment_transaction_id );
   });

  function validate()
  {
    var password = document.getElementById("password").value;
    var confirm_password = document.getElementById("confirm_password").value;
    var gender = document.getElementById("gender").value;
    var role_id = document.getElementById("role_id").value;
    if(password != confirm_password ){
      alert('Passwords do not match.');
      document.getElementById("confirm_password").focus();
      return false;
    }
    else if(gender == 0 ){
      alert('Please specify gender.');
      return false;
    }
    else if(role_id == 0 ){
      alert('Please specify role.');
      return false;
    }
    else {
      return true;
    }
  }
</script>

</body>
</html>