  <div class="navbar-default sidebar nicescroll" role="navigation">
    <div class="sidebar-nav navbar-collapse ">
      <ul class="nav" id="side-menu">
        <li class="nav-small-cap">Menu</li>
        <li class=""> <a href="<?php echo base_url() . "admin123/"; ?>" class="waves-effect"><i class="fa fa-dashboard"></i> Dashboard</a> </li>
        <?php if(($_SESSION['user_role_id'] == 3) && (strtoupper($_SESSION['user_role_name']) == "REPORT ADMIN")): ?>
          <li> <a href="admin123/Subscriptions" class="waves-effect"><i class="ti-bar-chart fa-bar-chart"></i> Subscriptions</a> </li>
        <?php else: ?>
          <li> <a href="admin123/agents" class="waves-effect"><i class="ti-user fa fa-user" aria-hidden="true"></i> Agents</a> </li>
          <li> <a href="admin123/artists" class="waves-effect"><i class="ti-user fa fa-user" aria-hidden="true"></i> Artists</a> </li>
          <li> <a href="admin123/bookings" class="waves-effect"><i class="ti-location-pin fa-map-marker" aria-hidden="true"></i> Bookings</a> </li>
          <li> <a href="admin123/analytics" class="waves-effect"><i class="ti-bar-chart fa-bar-chart"></i> Analytics (RBT)</a> </li>
          <li> <a href="admin123/report" class="waves-effect"><i class="ti-stats-up fa-chart-line"></i> Report (Booking)</a> </li>
        <?php endif; ?>
        <?php if(($_SESSION['user_role_id'] == 1) && (strtoupper($_SESSION['user_role_name']) == "SUPER ADMINISTRATOR")): ?>
          <li> <a href="admin123/users" class="waves-effect"><i class="ti-user fa-user"></i> Users (Admin)</a> </li>
        <?php endif; ?>
      </ul>
    </div>
    <!-- /.sidebar-collapse -->
  </div>