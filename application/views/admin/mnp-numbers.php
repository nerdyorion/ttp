<?php 
//ini_set('max_execution_time', 600); //600 seconds = 10 minutes
set_time_limit(0);
?>  
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Upload MNP Numbers</h4>
          <ol class="breadcrumb">
            <li><a href="./">Dashboard</a></li>
            <li><a class="active">Upload MNP Numbers</a></li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      <!-- row -->
      <div class="row">
        <div class="col-sm-12">
          <div class="white-box">
            <div class="row">
              <div class="col-md-12">
                <?php echo form_open_multipart('MNP-Numbers/create', 'class="form-horizontal", onsubmit="return validate();"'); ?>
                <!--<form class="form-horizontal" action="<?php echo current_url(); ?>" enctype="multipart/form-data" method="post">-->
                  <div class="form-group">
                    <label for="csv_file" class="col-sm-3 control-label">File (CSV): <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                      <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                        <div class="form-control" data-trigger="fileinput">
                          <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                          <span class="fileinput-filename"></span>
                        </div>
                        <span class="input-group-addon btn btn-default btn-file">
                        <span class="fileinput-new">Select file</span>
                        <span class="fileinput-exists">Change</span>
                        <input type="file" name="csv_file" id="csv_file"></span>
                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="date_format" class="col-sm-3 control-label">Date Format: <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                      <select class="form-control" name="date_format" id="date_format" required="required">
                        <option value="1">yyyy-mm-dd</option>
                        <option value="2">dd/mm/yyyy</option>
                        <option value="3" selected="selected">mm/dd/yyyy</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group m-b-0">
                    <div class="col-sm-offset-3 col-sm-9">
                      <button type="submit" class="btn btn-info waves-effect waves-light">Upload CSV</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        
      </div>
      <!-- row -->
      <div class="row">
        <div class="col-sm-12">
          <div class="white-box">
            <h3>MNP Numbers (Last 20)</h3>
            <!--<p class="text-muted m-b-20">Lorem<code>ipsum.table-bordered</code>.</p>-->
            <div class="table-responsive">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>MSISDN</th>
                    <th>Donor ID</th>
                    <th>Recipient ID</th>
                    <th>Timestamp</th>
                  </tr>
                </thead>
                <tbody>
                <?php if(empty($rows)): ?>
                  <tr>
                    <td colspan="5" align="center">No data returned.</td>
                  </tr>
                <?php else: ?>
                <?php $sn = 1; foreach ($rows as $row): ?>
                  <tr>
                    <td><?php echo $sn++; ?></td>
                    <td><?php echo $row['msisdn']; ?></td>
                    <td><?php echo $row['donor_id']; ?></td>
                    <td><?php echo $row['recipient_id']; ?></td>
                    <td><?php echo date('M d, Y h:i A', strtotime($row['date_time_stamp'])); ?></td>
                  </tr>
                <?php endforeach; ?>
                <?php endif; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_admin') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n";  // load footer view ?>
<script src="assets/js/jasny-bootstrap.js"></script>
<script type="text/javascript">
  function validate()
  {
    if(document.getElementById("csv_file").files.length == 0 ){
      alert('Please upload csv.');
      return false;
    }
    else {
      var fileName = $("#csv_file").val();
      if((fileName.lastIndexOf("csv")===fileName.length-3) || (fileName.lastIndexOf("txt")===fileName.length-3))
      {
        //alert("OK");
        return true;
      }
      else
      {
        alert("Please upload valid CSV file.");
        return false;
      }
    }
  }
</script>

</body>
</html>
