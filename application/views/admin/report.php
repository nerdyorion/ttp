<!-- Custom CSS -->
<!-- <link href="assets/css/admin/bootstrap-datepicker.min.css" rel="stylesheet"> -->

<link href="assets/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />

<!-- Page Content -->
<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row bg-title">
      <div class="col-lg-12">
        <h4 class="page-title">Booking Report</h4>
        <ol class="breadcrumb">
          <li><a href="./">Dashboard</a></li>
          <li class="active">Booking Report</li>
        </ol>
      </div>
      <!-- /.col-lg-12 -->
    </div>
    <!-- row -->
    <div class="row">
      <div class="col-sm-12">
        <div class="white-box">
          <div class="row">
            <div class="col-md-12">
              <?php echo form_open_multipart('/admin123/report', 'class="form-horizontal", method="get", onsubmit="return validate();"'); ?>
              <!--<form class="form-horizontal" action="<?php echo current_url(); ?>" enctype="multipart/form-data" method="post">-->
                <div class="form-group">
                  <label for="artist_id" class="col-sm-2 control-label">Artist:</label>
                  <div class="col-sm-5">
                    <select class="form-control" name="artist_id" id="artist_id">
                      <option value="0">-- All --</option>
                      <?php foreach ($talents as $talent): ?>
                        <option value="<?php echo $talent['id']; ?>" <?php $artist_id = isset($_GET['artist_id']) ? trim($_GET['artist_id']) : '-'; echo $artist_id == $talent['id'] ? 'selected="selected"' : ''; ?>><?php echo $talent['stage_name'] . ' (' . $talent['full_name'] . ')'; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="agent_id" class="col-sm-2 control-label">Agent:</label>
                  <div class="col-sm-5">
                    <select class="form-control" name="agent_id" id="agent_id">
                      <option value="0">-- All --</option>
                      <?php foreach ($agents as $agent): ?>
                        <option value="<?php echo $agent['id']; ?>" <?php $agent_id = isset($_GET['agent_id']) ? trim($_GET['agent_id']) : '-'; echo $agent_id == $agent['id'] ? 'selected="selected"' : ''; ?>><?php echo $agent['full_name']; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="start" class="col-sm-2 control-label">Event Date:</label>
                  <div class="col-sm-5">
                    <div class="input-daterange input-group" id="date-range">
                      <input type="text" class="form-control" name="start" value="<?php echo isset($_GET['start']) ? trim($_GET['start']) : ''; ?>">
                      <span class="input-group-addon bg-info b-0 text-white">to</span>
                      <input type="text" class="form-control" name="end" value="<?php echo isset($_GET['end']) ? trim($_GET['end']) : ''; ?>">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="start2" class="col-sm-2 control-label">Date Booked:</label>
                  <div class="col-sm-5">
                    <div class="input-daterange input-group" id="date-range2">
                      <input type="text" class="form-control" name="start2" value="<?php echo isset($_GET['start2']) ? trim($_GET['start2']) : ''; ?>">
                      <span class="input-group-addon bg-info b-0 text-white">to</span>
                      <input type="text" class="form-control" name="end2" value="<?php echo isset($_GET['end2']) ? trim($_GET['end2']) : ''; ?>">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="status" class="col-sm-2 control-label">Status:</label>
                  <div class="col-sm-5">
                    <select class="form-control" name="status" id="status">
                      <option value="0">-- All --</option>
                      <option value="1" <?php $status = isset($_GET['status']) ? trim($_GET['status']) : '-'; echo $status == "1" ? 'selected="selected"' : ''; ?>>Accepted</option>
                      <!-- <option value="2" <?php $status = isset($_GET['status']) ? trim($_GET['status']) : '-'; echo $status == "2" ? 'selected="selected"' : ''; ?>>Rejected</option> -->
                      <option value="3" <?php $status = isset($_GET['status']) ? trim($_GET['status']) : '-'; echo $status == "3" ? 'selected="selected"' : ''; ?>>Unpaid</option>
                      <option value="4" <?php $status = isset($_GET['status']) ? trim($_GET['status']) : '-'; echo $status == "4" ? 'selected="selected"' : ''; ?>>Paid</option>
                    </select>
                  </div>
                </div>
                <div class="form-group m-b-0">
                  <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-info waves-effect waves-light">Generate</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

    </div>
    <!-- row -->
    <div class="row">
      <div class="col-sm-12">
        <div class="white-box">
          <h3>Result</h3>
          <!--<p class="text-muted m-b-20">Lorem<code>ipsum.table-bordered</code>.</p>-->
          <div class="table-responsive">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Agent</th>
                  <th>Event</th>
                  <th>Date</th>
                  <th>Artist</th>
                  <th>Booking Cost (N)</th>
                  <th>Status</th>
                  <th class="text-nowrap">Actions</th>
                </tr>
              </thead>
              <tbody>
                <?php if(empty($rows)): ?>
                  <tr>
                    <td colspan="8" align="center">No data returned.</td>
                  </tr>
                <?php else: ?>
                  <?php $sn = 1; foreach ($rows as $row): ?>
                  <tr>
                    <td><?php echo $sn++; ?></td>
                    <td title="<?php echo $row['full_name']; ?>"><?php echo $row['full_name']; ?></td>
                    <td title="<?php echo dashIfEmpty($row['event_name']); ?>"><?php echo dashIfEmpty($row['event_name']); ?></td>
                    <td title="<?php echo $row['event_date'] . ' ' . $row['event_start_time'] . '-' . $row['event_end_time']; ?>"><?php echo $row['event_date'] . ' ' . $row['event_start_time'] . '-' . $row['event_end_time']; ?></td>
                    <td><a href="<?php echo base_url() . '@' . $row['username']; ?>" data-toggle="tooltip" data-original-title="View <?php echo $row['stage_name']; ?>" target="_blank"><?php echo $row['stage_name']; ?></a></td>
                    <td title="<?php echo $row['booking_cost'] == 0 ? 'Pending' : number_format($row['booking_cost']); ?>"><?php echo $row['booking_cost'] == 0 ? '<a href="javascript:void(0);" class="bg-warning biga" style="color: #ffffff;">&nbsp;Pending&nbsp;</a>' : number_format($row['booking_cost']); ?></td>
                    <td>
                      <?php echo $row['accepted'] == 1 ? '<a href="javascript:void(0);" class="bg-success biga" style="color: #ffffff;">&nbsp;Accepted&nbsp;</a>' : ''; ?> 
                      <?php echo $row['rejected'] == 1 ? '<a href="javascript:void(0);" class="bg-danger biga" style="color: #ffffff;">&nbsp;Rejected&nbsp;</a>' : ''; ?> 
                      <?php echo $row['rejected'] != 1 && $row['paid'] == 0 ? '<a href="javascript:void(0);" class="bg-danger biga" style="color: #ffffff;">&nbsp;Unpaid&nbsp;</a>' : ''; ?>
                      <?php echo $row['paid'] == 1 && $row['accepted'] == 0 ? '<a href="javascript:void(0);" class="bg-info biga" style="color: #ffffff;">&nbsp;Paid&nbsp;</a>' : ''; ?>
                    </td>
                    <td class="text-nowrap">
                      <a href="admin123/bookings/view/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="View"> <i class="fa fa-folder-open text-inverse m-r-10"></i> </a> 
                    </td>
                  </tr>
                <?php endforeach; ?>
              <?php endif; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_admin') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n";  // load footer view ?>
<script src="assets/js/jasny-bootstrap.js"></script>
<script src="assets/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
 jQuery(document).ready(function() {
  // Date Picker
  // jQuery('.mydatepicker, #datepicker2').datepicker();
  // jQuery('#datepicker-autoclose').datepicker({
  //   autoclose: true,
  //   todayHighlight: true
  // });

  jQuery('#date-range').datepicker({
    toggleActive: true
  });

  jQuery('#date-range2').datepicker({
    toggleActive: true
  });

});

/*
  function validate()
  {
    if((document.getElementById("network_id").value == '0') || (document.getElementById("network_id").value == null)){
      alert('Please select network.');
      return false;
    }
    else {
      return true;
    }
  }
  */
</script>

</body>
</html>
