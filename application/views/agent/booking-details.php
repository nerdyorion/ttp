  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Booking Details</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "agent/"; ?>">Dashboard</a></li>
            <li><a href="<?php echo base_url() . "agent/"; ?>bookings">Bookings</a></li>
            <li class="active">Details</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- row -->
      <div class="row">
        <div class="col-md-12">
          <div class="white-box">
            <div class="table-responsive">
              <table class="table table-bordered table-striped">
                <tbody>
                  <!-- General -->
                  <tr>
                    <th>Status</th>
                    <td>
                      <?php echo $row['accepted'] == 1 ? '<a href="javascript:void(0);" class="bg-success" style="color: #ffffff;">&nbsp;Accepted&nbsp;</a>' : ''; ?> 
                      <?php echo $row['rejected'] == 1 ? '<a href="javascript:void(0);" class="bg-danger" style="color: #ffffff;">&nbsp;Rejected&nbsp;</a>' : ''; ?> 
                      <?php echo $row['rejected'] != 1 && $row['paid'] == 0 ? '<a href="javascript:void(0);" class="bg-danger" style="color: #ffffff;">&nbsp;Unpaid&nbsp;</a>' : ''; ?>
                      <?php echo $row['paid'] == 1 && $row['accepted'] == 0 ? '<a href="javascript:void(0);" class="bg-info" style="color: #ffffff;">&nbsp;Paid&nbsp;</a>' : ''; ?>
                      </td>
                  </tr>
                  <tr>
                    <th>Artist</th>
                    <td><a href="<?php echo base_url() . '@' . $row['username']; ?>" data-toggle="tooltip" data-original-title="View <?php echo $row['stage_name']; ?>" target="_blank"><?php echo $row['stage_name']; ?></a>
                    </td>
                  </tr>
                  <tr>
                    <th>Booking Cost</th>
                    <td><?php echo $row['booking_cost'] == 0 ? '<a href="javascript:void(0);" class="bg-warning" style="color: #ffffff;">&nbsp;Pending&nbsp;</a>' : 'N' . number_format($row['booking_cost']); ?></td>
                  </tr>
                  <?php if(!empty($row['content_brief'])): ?> <!-- Content Creator Booking -->
                  <tr>
                    <th>Content Brief</th>
                    <td><?php echo $row['content_brief']; ?></td>
                  </tr>
                  <tr>
                    <th>Content Industry</th>
                    <td><?php echo $content_industry; ?></td>
                  </tr>
                  <tr>
                    <th>Requested on</th>
                    <td><?php echo $row['date_created'] == "0000-00-00 00:00:00" ? "-" : date('M d, Y h:i A', strtotime($row['date_created'])); ?></td>
                  </tr>

                  <?php elseif(!empty($row['content_tier_id'])): ?>   <!-- Content Amplifier Booking -->
                  <tr>
                    <th>Content Industry</th>
                    <td><?php echo $content_industry; ?></td>
                  </tr>
                  <tr>
                    <th>Content Budget</th>
                    <td><?php echo $content_budget; ?></td>
                  </tr>
                  <tr>
                    <th>Content Tier</th>
                    <td><?php echo $content_tier; ?></td>
                  </tr>
                  <tr>
                    <th>Campaign Duration</th>
                    <td><?php echo $content_campaign_duration; ?></td>
                  </tr>
                  <tr>
                    <th>Requested on</th>
                    <td><?php echo $row['date_created'] == "0000-00-00 00:00:00" ? "-" : date('M d, Y h:i A', strtotime($row['date_created'])); ?></td>
                  </tr>
                  <?php else: ?>   <!-- Others Booking -->
                  <tr>
                    <th>Event</th>
                    <td><?php echo $row['event_name']; ?></td>
                  </tr>
                  <tr>
                    <th>Date</th>
                    <td><?php echo $row['event_date'] . ' ' . $row['event_start_time'] . '-' . $row['event_end_time']; ?></td>
                  </tr>
                  <tr>
                    <th>Event Location</th>
                    <td><?php echo $row['event_location']; ?></td>
                  </tr>
                  <tr>
                    <th>Event Description</th>
                    <td><?php echo $row['event_description']; ?></td>
                  </tr>
                  <?php endif; ?>
                </tbody>
              </table>
            </div>
            <p class="text-center">
              <button type="button" class="btn btn-info waves-effect waves-light center" onclick="history.go(-1); return false;">&larr; Back</button>
            </p>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_admin') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n"; ?>
<script src="assets/js/jasny-bootstrap.js"></script>

</body>
</html>
