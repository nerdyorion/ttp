  <style type="text/css">
  table.table.table-bordered tbody tr td{
   vertical-align: middle;
 }
</style>
<!-- Page Content -->
<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row bg-title">
      <div class="col-lg-12">
        <h4 class="page-title">Bookings</h4>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url() . "agent/"; ?>">Dashboard</a></li>
          <li class="active">Bookings</li>
        </ol>
      </div>
      <!-- /.col-lg-12 -->
    </div>

    <!-- row -->
    <div class="row">
      <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
        <div class="white-box">
          <!--<h3>Products</h3>-->
          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#all" aria-controls="all" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-view-list"></i></span><span class="hidden-xs"> View All</span></a></li>
          </ul>

          <!-- Tab panes -->
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="all"> 
              <div class="col-md-12">
                <div class="table-responsive">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Event</th>
                        <th>Date</th>
                        <th>Artist</th>
                        <th>Booking Cost (N)</th>
                        <th>Status</th>
                        <th class="text-nowrap">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php if(empty($rows)): ?>
                        <tr>
                          <td colspan="5" align="center">No data returned.</td>
                        </tr>
                      <?php else: ?>
                        <?php foreach ($rows as $row): ?>
                          <tr>
                            <td><?php echo $sn++; ?></td>
                            <td title="<?php echo dashIfEmpty($row['event_name']); ?>"><?php echo dashIfEmpty($row['event_name']); ?></td>
                            <td title="<?php echo $row['event_date'] . ' ' . $row['event_start_time'] . '-' . $row['event_end_time']; ?>"><?php echo $row['event_date'] . ' ' . $row['event_start_time'] . '-' . $row['event_end_time']; ?></td>
                            <td><a href="<?php echo base_url() . '@' . $row['username']; ?>" data-toggle="tooltip" data-original-title="View <?php echo $row['stage_name']; ?>" target="_blank"><?php echo $row['stage_name']; ?></a></td>
                            <td title="<?php echo $row['booking_cost'] == 0 ? 'Pending' : number_format($row['booking_cost']); ?>"><?php echo $row['booking_cost'] == 0 ? '<a href="javascript:void(0);" class="bg-warning biga" style="color: #ffffff;">&nbsp;Pending&nbsp;</a>' : number_format($row['booking_cost']); ?></td>
                            <td>
                              <?php echo $row['accepted'] == 1 ? '<a href="javascript:void(0);" class="bg-success biga" style="color: #ffffff;">&nbsp;Accepted&nbsp;</a>' : ''; ?> 
                              <?php echo $row['rejected'] == 1 ? '<a href="javascript:void(0);" class="bg-danger biga" style="color: #ffffff;">&nbsp;Rejected&nbsp;</a>' : ''; ?> 
                              <?php echo $row['rejected'] != 1 && $row['paid'] == 0 ? '<a href="javascript:void(0);" class="bg-danger biga" style="color: #ffffff;">&nbsp;Unpaid&nbsp;</a>' : ''; ?>
                              <?php echo $row['paid'] == 1 && $row['accepted'] == 0 ? '<a href="javascript:void(0);" class="bg-info biga" style="color: #ffffff;">&nbsp;Paid&nbsp;</a>' : ''; ?>
                            </td>
                            <td class="text-nowrap">
                              <a href="agent/bookings/delete/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Delete"> <span class="text-danger"><i class="fa fa-trash text-danger m-r-10" onclick="if(confirm('Are you sure you want to delete this record?')) return true; else return false;"></i></span> </a> 
                            <a href="agent/bookings/view/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="View"> <i class="fa fa-folder-open text-inverse m-r-10"></i> </a> 
                              <?php if($row['booking_cost'] != 0 && $row['paid'] == 0): ?>
                                <span data-toggle="modal" data-target="#paymentModal" class="open-payment-dialog" data-id="<?php echo $row['id']; ?>" data-email="<?php echo $this->session->userdata('user_email'); ?>" data-booking-cost="<?php echo $row['booking_cost']; ?>"><a href="javascript:void(0);" data-toggle="tooltip" data-original-title="Make Payment"> <i class="fa fa-check-circle text-inverse m-r-10"></i> </a></span>
                              <?php endif; ?>
                            </td>
                          </tr>
                        <?php endforeach; ?>
                      <?php endif; ?>
                    </tbody>
                  </table>
                </div>
                <!-- <p><?php //echo $links; ?></p> -->
              </div>


              <!-- Modal -->
              <div id="paymentModal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Make Payment</h4>
                    </div>
                    <div class="modal-body white-box">
                      <!-- Nav tabs -->
                      <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#bankTransfer" aria-controls="bankTransfer" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-view-list"></i></span><span class="hidden-xs"> Bank Transfer</span></a></li>
                        <li role="presentation"><a href="#payOnline" aria-controls="payOnline" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-plus"></i></span> <span class="hidden-xs">Pay Online</span></a></li>
                      </ul>

                      <!-- Tab panes -->
                      <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="bankTransfer"> 
                          <div class="col-md-12">
                            <!-- <p>Some text in the modal.</p> -->
                            <p>All Bank Transfer payments to be made to this account:</p>
                            <p><b>Bank</b>: GTB</p>
                            <p><b>Account Name</b>: US Uber</p>
                            <p><b>Account No.</b>: 0901112312</p>

                            <br />

                            <?php echo form_open('agent/bookings/pay', 'class="form-horizontal"'); ?>
                            <div class="form-group">
                              <label for="payment_transaction_id" class="col-sm-2 control-label">Transaction ID: <span class="text-danger">*</span></label>
                              <div class="col-sm-9">
                                <input type="text" class="form-control" name="payment_transaction_id" id="payment_transaction_id" value="" placeholder="" required="required">
                                <input type="hidden" name="booking_id" id="booking_id" value="" />
                              </div>
                            </div>
                            <div class="form-group m-b-0">
                              <div class="col-sm-offset-5 col-sm-9">
                                <br />
                                <button type="submit" class="btn btn-primary"><i class="fa fa-save" aria-hidden="true"></i> Submit</button>
                              </div>
                            </div>
                          </form>
                        </div>

                        <div class="clearfix"></div>
                      </div>

                      <div role="tabpanel" class="tab-pane" id="payOnline">
                        <div class="col-md-12">
                          <form>
                            <script src="https://js.paystack.co/v1/inline.js"></script>
                            <button type="button" class="btn-sm btn-info waves-effect waves-light" onclick="payWithPaystack()"> Pay Now </button> 
                            <input type="hidden" name="email" id="email" value="" />
                            <input type="hidden" name="booking_cost" id="booking_cost" value="" />
                          </form>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>
                  </div>
                </div>

              </div>
            </div>
            <div class="col-md-3 pull-right pagination">
              <p><?php echo $links; ?></p>
            </div>

            <div class="clearfix"></div>
            <em>*All paid bookings would have their status changed to "Accepted" once payment is confirmed.</em>
          </div>
        </div>
      </div>
    </div>
  </div> 
  <!-- /.row -->
</div>
<!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_agent') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n"; ?>
<script src="assets/js/jasny-bootstrap.js"></script>
<script type="text/javascript">
  
  $(document).on("click", ".open-payment-dialog", function () {
   var booking_id = $(this).data('id');
   var email = $(this).data('email');
   var booking_cost = $(this).data('booking-cost');

   $(".modal-body #booking_id").val( booking_id );
   $(".modal-body #email").val( email );
   $(".modal-body #booking_cost").val( booking_cost );
 });


  function payWithPaystack(){
    var handler = PaystackPop.setup({
      key: 'pk_test_3c9cd7e6c2d1d11470456da73e66dbd871b84cda',
      // email: '<?php // echo $this->session->userdata('user_email'); ?>',
      // amount: <?php // echo $row['booking_cost'] * 100; ?>,
      email: $(".modal-body #email").val(),
      amount: $(".modal-body #booking_cost").val() * 100,
      metadata: {
        custom_fields: [
        {
          display_name: "Mobile Number",
          variable_name: "mobile_number",
          value: "+2348012345678"
        }
        ]
      },
      callback: function(response){
        alert('success. transaction ref is ' + response.reference);
      },
      onClose: function(){
        // alert('window closed')
      }
    });
    handler.openIframe();
  }
</script>

</body>
</html>