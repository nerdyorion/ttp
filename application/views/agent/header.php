<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <base href="<?php echo base_url();?>">
  <meta name="description" content="">
  <meta name="author" content="">
  <!-- <link rel="icon" type="image/png" sizes="16x16" href="assets/agent/images/favicon.ico"> -->
  <link href="assets/agent/images/favicon.png" rel="shortcut icon" type="image/png" />
  <title><?php echo $this->config->item('app_name'); ?> | <?php echo $page_title; ?></title>
  <!-- Bootstrap Core CSS -->
  <link href="assets/agent/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  <!-- Menu CSS -->
  <link href="assets/agent/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
  <!-- Menu CSS -->
  <!--<link href="assets/agent/bower_components/morrisjs/morris.css" rel="stylesheet">-->
  <link rel="stylesheet" href="assets/agent/bower_components/dropify/dist/css/dropify.min.css">
  <!-- Custom CSS -->
  <link href="assets/agent/css/style.css" rel="stylesheet">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="assets/agent/https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="assets/agent/https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>
  <!-- Preloader -->
  <div class="preloader">
    <div class="cssload-speeding-wheel"></div>
  </div>
  <div id="wrapper">
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" style="margin-bottom: 0">
      <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
        <div class="top-left-part"><a class="logo" href="<?php echo base_url() . "agent/"; ?>"><i class="fa fa-home" style="display: inline; color:#ffffff; "></i>&nbsp;<span class="hidden-xs">AGENTS</span></a></div>
        <ul class="nav navbar-top-links navbar-left hidden-xs">
          <li><a href="javascript:void(0);" class="open-close hidden-xs waves-effect waves-light"><i class="icon-arrow-left-circle ti-menu"></i></a></li>
          
          <!-- Notifications -->
          <li class="dropdown">
            <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#">
              <i class="icon-bell"></i> 
              <?php $notifications = notifications(); ?>
              <?php $notifications_count = count($notifications); ?>
              <?php if($notifications_count != 0): ?>
                <span class="badge badge-xs badge-warning"><?php echo $notifications_count; ?></span>
              <?php endif; ?>
            </a>
            <ul class="dropdown-menu nicescroll mailbox">
              <li>
                <div class="drop-title">You have <?php echo $notifications_count == 0 ? 'no' : $notifications_count; ?> new message(s)<?php echo $notifications_count == 1 ? '' : 's'; ?></div>
              </li>
              <li>
                <div class="message-center">
                  <?php if($notifications_count == 0): ?>
                  <?php else: ?>
                    <?php foreach($notifications as $notification): ?>
                      <a href="agent/notifications">
                        <div class="user-img">
                          <img src="assets/images/users/icon.png" alt="user" class="img-circle" />
                          <span class="profile-status"></span>
                        </div>
                        <div class="mail-contnet">
                          <span class="mail-desc"><?php echo strip_tags($notification['message']); ?> </span>
                          <span class="time"><?php echo time_elapsed_string($notification['date_created']); ?></span>
                        </div>
                      </a>
                    <?php endforeach; ?>
                  <?php endif; ?>
                </div>
              </li>
              <li> <a class="text-center" href="agent/notifications"> <strong>See all notifications</strong> <i class="fa fa-angle-right"></i> </a></li>
            </ul>
            <!-- /.dropdown-messages -->
          </li>

          <!-- /.dropdown -->
          
          <li class="dropdown"> <a class="dropdown-toggle waves-effect waves-light" href="<?php echo base_url(); ?>"><i class="icon-home"></i> Homepage</a>
          </li>
          
          <li class="dropdown"> <a class="top-book dropdown-toggle waves-effect waves-light" href="<?php echo base_url(); ?>artists"><i class="icon-globe"></i> Book A Talent Now</a>
          </li>
          
        </ul>
        <ul class="nav navbar-top-links navbar-right pull-right">
          <li class="dropdown"> <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <img src="<?php echo file_exists($this->session->userdata('user_image_url')) ? $this->session->userdata('user_image_url') : 'assets/agent/images/users/icon.png'; ?>" alt="User" width="36" class="img-circle"><b class="hidden-xs"><?php echo $_SESSION['user_username']; ?></b> </a>
            <ul class="dropdown-menu dropdown-user">
              <li><a href="agent/profile"><i class="ti-user"></i> My Profile</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="agent/Change-Password"><i class="ti-lock"></i> Change Password</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="agent/logout"><i class="fa fa-power-off"></i> Logout</a></li>
            </ul>
            <!-- /.dropdown-user -->
          </li>
          <!-- /.dropdown -->
        </ul>
      </div>
      <!-- /.navbar-header -->
      <!-- /.navbar-top-links -->
      <!-- /.navbar-static-side -->
    </nav>
    <div id="success-msg" style="display:none" class="myadmin-alert myadmin-alert-icon myadmin-alert-click alert3 myadmin-alert-top"></div>
    <div id="error-msg" style="display:none" class="myadmin-alert myadmin-alert-icon myadmin-alert-click alert6 myadmin-alert-top"></div>