  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Update Product</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "agent/"; ?>">Dashboard</a></li>
            <li><a href="<?php echo base_url() . "agent/"; ?>products">Products</a></li>
            <li class="active">Edit</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- row -->
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
          <div class="white-box">
            <!--<h3>Users</h3>-->
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#update" aria-controls="update" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-pencil"></i></span><span class="hidden-xs"> Update</span></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="update">
                <div class="col-md-12">
                <?php echo form_open_multipart('admin123/products/edit/' . $row['id'], 'class="form-horizontal", onsubmit="return validate();"'); ?>
                  <div class="form-group">
                    <label for="category_id" class="col-sm-2 control-label">Category: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <select class="form-control" name="category_id" id="category_id" required="required">
                        <?php foreach ($categories as $category): ?>
                        <option value="<?php echo $category['id']; ?>" <?php echo $category['id'] == $row['category_id'] ? 'selected="selected"' : ''; ?>><?php echo $category['name']; ?></option>
                        </tr>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Product Name: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="name" maxlength="8000" id="name" value="<?php echo $row['name']; ?>" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="image" class="col-sm-2 control-label">Image (650x650 px): <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                        <div class="form-control" data-trigger="fileinput">
                          <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                          <span class="fileinput-filename"></span>
                        </div>
                        <span class="input-group-addon btn btn-default btn-file">
                        <span class="fileinput-new">Select file</span>
                        <span class="fileinput-exists">Change</span>
                        <input type="file" name="image" id="image"></span>
                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                      </div>
                    </div>
                  </div>
                  <input type="hidden" name="image_url_old" value="<?php echo $row['image_url']; ?>" />
                  <input type="hidden" name="image_url_thumb_old" value="<?php echo $row['image_url_thumb']; ?>" />
                  <div class="form-group">
                    <label for="price" class="col-sm-2 control-label">Price (NGN): <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="number" min="0" class="form-control" name="price" maxlength="13" id="price" value="<?php echo $row['price']; ?>" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="duration" class="col-sm-2 control-label">Duration: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <select class="form-control" name="duration" id="duration" required="required">
                        <option value="1 Month" <?php echo strtoupper($row['duration']) == '1 MONTH' ? 'selected="selected"' : ''; ?>>1 Month</option>
                        <option value="1 Week" <?php echo strtoupper($row['duration']) == '1 WEEK' ? 'selected="selected"' : ''; ?>>1 Week</option>
                        <option value="5 Days" <?php echo strtoupper($row['duration']) == '5 DAYS' ? 'selected="selected"' : ''; ?>>5 Days</option>
                        <option value="4 Days" <?php echo strtoupper($row['duration']) == '4 DAYS' ? 'selected="selected"' : ''; ?>>4 Days</option>
                        <option value="3 Days" <?php echo strtoupper($row['duration']) == '3 DAYS' ? 'selected="selected"' : ''; ?>>3 Days</option>
                        <option value="2 Days" <?php echo strtoupper($row['duration']) == '2 DAYS' ? 'selected="selected"' : ''; ?>>2 Days</option>
                        <option value="1 Day" <?php echo strtoupper($row['duration']) == '1 DAY' ? 'selected="selected"' : ''; ?>>1 Day</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="description" class="col-sm-2 control-label">Description:</label>
                    <div class="col-sm-10">
                      <textarea class="form-control" name="description" id="description" maxlength="20000" rows="5"><?php echo $row['description']; ?></textarea>
                    </div>
                  </div>

                  <label for="codes" class="col-sm-2 control-label">Service Codes:</label>
                  <div class="col-sm-offset-2">
                    <ul class="nav nav-tabs">
                      <li class="active"><a data-toggle="tab" href="#mtn">MTN</a></li>
                      <li><a data-toggle="tab" href="#airtel">AIRTEL</a></li>
                      <li><a data-toggle="tab" href="#glo">GLO</a></li>
                      <li><a data-toggle="tab" href="#emts">EMTS</a></li>
                      <li><a data-toggle="tab" href="#ntel">NTEL</a></li>
                    </ul>

                    <div class="tab-content">
                      <div id="mtn" class="tab-pane fade in active">
                        <div class="form-group">
                          <label for="mtn_service_code" class="col-sm-2 control-label">Code:</label>
                          <div class="col-sm-5">
                            <input type="text" class="form-control" name="mtn_service_code" maxlength="255" id="mtn_service_code" value="<?php echo $row['mtn_service_code']; ?>" placeholder="">
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="mtn_service_keyword" class="col-sm-2 control-label">Keyword:</label>
                          <div class="col-sm-5">
                            <input type="text" class="form-control" name="mtn_service_keyword" maxlength="255" id="mtn_service_keyword" value="<?php echo $row['mtn_service_keyword']; ?>" placeholder="">
                          </div>
                        </div>
                      </div>
                      <div id="airtel" class="tab-pane fade">
                        <div class="form-group">
                          <label for="airtel_service_code" class="col-sm-2 control-label">Code:</label>
                          <div class="col-sm-5">
                            <input type="text" class="form-control" name="airtel_service_code" maxlength="255" id="airtel_service_code" value="<?php echo $row['airtel_service_code']; ?>" placeholder="">
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="airtel_service_keyword" class="col-sm-2 control-label">Keyword:</label>
                          <div class="col-sm-5">
                            <input type="text" class="form-control" name="airtel_service_keyword" maxlength="255" id="airtel_service_keyword" value="<?php echo $row['airtel_service_keyword']; ?>" placeholder="">
                          </div>
                        </div>
                      </div>
                      <div id="glo" class="tab-pane fade">
                        <div class="form-group">
                          <label for="glo_service_code" class="col-sm-2 control-label">Code:</label>
                          <div class="col-sm-5">
                            <input type="text" class="form-control" name="glo_service_code" maxlength="255" id="glo_service_code" value="<?php echo $row['glo_service_code']; ?>" placeholder="">
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="glo_service_keyword" class="col-sm-2 control-label">Keyword:</label>
                          <div class="col-sm-5">
                            <input type="text" class="form-control" name="glo_service_keyword" maxlength="255" id="glo_service_keyword" value="<?php echo $row['glo_service_keyword']; ?>" placeholder="">
                          </div>
                        </div>
                      </div>
                      <div id="emts" class="tab-pane fade">
                        <div class="form-group">
                          <label for="emts_service_code" class="col-sm-2 control-label">Code:</label>
                          <div class="col-sm-5">
                            <input type="text" class="form-control" name="emts_service_code" maxlength="255" id="emts_service_code" value="<?php echo $row['emts_service_code']; ?>" placeholder="">
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="emts_service_keyword" class="col-sm-2 control-label">Keyword:</label>
                          <div class="col-sm-5">
                            <input type="text" class="form-control" name="emts_service_keyword" maxlength="255" id="emts_service_keyword" value="<?php echo $row['emts_service_keyword']; ?>" placeholder="">
                          </div>
                        </div>
                      </div>
                      <div id="ntel" class="tab-pane fade">
                        <div class="form-group">
                          <label for="ntel_service_code" class="col-sm-2 control-label">Code:</label>
                          <div class="col-sm-5">
                            <input type="text" class="form-control" name="ntel_service_code" maxlength="255" id="ntel_service_code" value="<?php echo $row['ntel_service_code']; ?>" placeholder="">
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="ntel_service_keyword" class="col-sm-2 control-label">Keyword:</label>
                          <div class="col-sm-5">
                            <input type="text" class="form-control" name="ntel_service_keyword" maxlength="255" id="ntel_service_keyword" value="<?php echo $row['ntel_service_keyword']; ?>" placeholder="">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group m-b-0">
                    <div class="col-sm-offset-3 col-sm-9">
                      <button type="submit" class="btn btn-info waves-effect waves-light">Update</button>
                    </div>
                  </div>
                </form>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
      </div> 
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_agent') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n"; ?>
<script src="assets/js/jasny-bootstrap.js"></script>
<script type="text/javascript">
  function validate()
  {
    var category_id = document.getElementById("category_id").value;
    
    if(category_id == 0 ){
      alert('Please specify category.');
      return false;
    }
    else if(document.getElementById("image").files.length == 0 ){
      // no file uploaded
      return true;
    }
    else {
      var fileName = $("#image").val(); // alert('fileName: ' + fileName + "\n" + typeof(fileName));
      fileName = fileName.toLowerCase();
      
      if((fileName.lastIndexOf("jpg")===fileName.length-3) || (fileName.lastIndexOf("jpeg")===fileName.length-4) || (fileName.lastIndexOf("png")===fileName.length-3) || (fileName.lastIndexOf("gif")===fileName.length-3) || (fileName.lastIndexOf("bmp")===fileName.length-3))
      {
        //alert("OK");
        return true;
      }
      else
      {
        alert("Please upload valid image file.");
        return false;
      }
    }
  }
</script>

</body>
</html>