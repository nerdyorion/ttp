  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Profile</h4>
          <ol class="breadcrumb">
            <li><a href="./">Dashboard</a></li>
            <li class="active">Profile</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      <!-- row -->

      <!-- row -->
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
          <div class="white-box">
            <!--<h3>Users</h3>-->
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#basicInfo" aria-controls="basicInfo" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-user"></i></span><span class="hidden-xs"> Basic Info</span></a></li>
              <li role="presentation"><a href="#profilePic" aria-controls="profilePic" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-image"></i></span> <span class="hidden-xs">Profile Picture</span></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="basicInfo">
                <div class="col-md-12">
                  <?php echo form_open('agent/profile/updateBasicInfo', 'onsubmit="return validateBasicInfo();"'); ?>
                  <div class="form-group col-sm-3">
                    <label for="first_name" class="control-label">First Name: <span class="text-danger">*</span></label>
                    <div class="form-inline row">
                      <div class="form-group col-sm-12">
                        <input type="text" class="form-control" name="first_name" maxlength="255" id="first_name" value="<?php echo $row['first_name']; ?>" placeholder="" required="required">
                      </div>
                    </div>
                  </div>
                  <div class="form-group col-sm-3">
                    <label for="last_name" class="control-label">Last Name: <span class="text-danger">*</span></label>
                    <div class="form-inline row">
                      <div class="form-group col-sm-12">
                        <input type="text" class="form-control" name="last_name" maxlength="255" id="last_name" value="<?php echo $row['last_name']; ?>" placeholder="" required="required">
                      </div>
                    </div>
                  </div>

                  <!-- Second Block -->
                  <div class="form-group col-sm-3">
                    <label for="category_id" class="control-label">Category: <span class="text-danger">*</span></label>
                    <div class="form-inline row">
                      <div class="form-group col-sm-12">
                        <select class="form-control" name="category_id" id="category_id" required="required">
                          <option value="0">-- Select --</option>
                          <?php foreach ($categories as $category): ?>
                            <option value="<?php echo $category['id']; ?>" <?php echo $row['category_id'] == $category['id'] ? 'selected="selected"' : ''; ?>><?php echo $category['category_name']; ?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="form-group col-sm-3 hidden">
                    <label for="category_social_influencer_id" class="control-label">Influencer Category:</label>
                    <div class="form-inline row">
                      <div class="form-group col-sm-12">
                        <select class="form-control" name="category_social_influencer_id" id="category_social_influencer_id">
                          <!-- <option value="0" selected="selected">-- Select --</option> -->
                          <?php foreach ($categories_social_influencer as $category): ?>
                            <option value="<?php echo $category['id']; ?>" <?php echo $row['category_social_influencer_id'] == $category['id'] ? 'selected="selected"' : ''; ?>><?php echo $category['category_name']; ?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="form-group col-sm-3">
                    <label for="country_id" class="control-label">Country: <span class="text-danger">*</span></label>
                    <div class="form-inline row">
                      <div class="form-group col-sm-12">
                        <select class="form-control" name="country_id" id="country_id" required="required">
                          <option value="0">-- Select --</option>
                          <?php foreach ($countries as $country): ?>
                            <option value="<?php echo $country['id']; ?>" <?php echo $row['country_id'] == $country['id'] ? 'selected="selected"' : ''; ?>><?php echo $country['name']; ?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="form-group col-sm-3">
                    <label for="gender" class="control-label">Gender: <span class="text-danger">*</span></label>
                    <div class="form-inline row">
                      <div class="form-group col-sm-12">
                        <select class="form-control" name="gender" id="gender" required="required">
                          <option value="0">-- Select --</option>
                          <option value="Male" <?php echo strtolower($row['gender']) == "male" ? 'selected="selected"' : ''; ?>>Male</option>
                          <option value="Female" <?php echo strtolower($row['gender']) == "female" ? 'selected="selected"' : ''; ?>>Female</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="form-group col-sm-3">
                    <label for="phone" class="control-label">Phone:</label>
                    <div class="form-inline row">
                      <div class="form-group col-sm-12">
                        <input type="text" class="form-control" onkeypress='return event.charCode >= 48 && event.charCode <= 57' name="phone" maxlength="13" id="phone" value="<?php echo $row['phone']; ?>" placeholder="">
                      </div>
                    </div>
                  </div>

                  <!-- Third Block -->
                  <div class="form-group col-sm-3">
                    <label for="city" class="control-label">City:</label>
                    <div class="form-inline row">
                      <div class="form-group col-sm-12">
                        <input type="text" class="form-control" name="city" maxlength="255" id="city" value="<?php echo $row['city']; ?>" placeholder="">
                      </div>
                    </div>
                  </div>
                  <div class="form-group col-sm-3">
                    <label for="landline" class="control-label">Landline:</label>
                    <div class="form-inline row">
                      <div class="form-group col-sm-12">
                        <input type="text" class="form-control" name="landline" maxlength="255" id="landline" value="<?php echo $row['landline']; ?>" placeholder="">
                      </div>
                    </div>
                  </div>
                  <div class="form-group col-sm-3">
                    <label for="company" class="control-label">Company:</label>
                    <div class="form-inline row">
                      <div class="form-group col-sm-12">
                        <input type="text" class="form-control" name="company" maxlength="255" id="company" value="<?php echo $row['company']; ?>" placeholder="">
                      </div>
                    </div>
                  </div>
                  <div class="form-group col-sm-3">
                    <label for="position" class="control-label">Position:</label>
                    <div class="form-inline row">
                      <div class="form-group col-sm-12">
                        <input type="text" class="form-control" name="position" maxlength="255" id="position" value="<?php echo $row['position']; ?>" placeholder="">
                      </div>
                    </div>
                  </div>

                  <!-- Fourth Block -->
                  <div class="form-group col-sm-3">
                    <label for="website" class="control-label">Website:</label>
                    <div class="form-inline row">
                      <div class="form-group col-sm-12">
                        <input type="text" class="form-control" name="website" maxlength="255" id="website" value="<?php echo $row['website']; ?>" placeholder="">
                      </div>
                    </div>
                  </div>

                  <div class="form-group m-b-0">
                    <div class="col-sm-offset-3 col-sm-9">
                      <button type="submit" class="btn btn-info waves-effect waves-light">Update Profile</button>
                    </div>
                  </div>
                </form>
              </div>
              <div class="clearfix"></div>
            </div>
        <!-- TAB -->
        <div role="tabpanel" class="tab-pane" id="profilePic">
          <div class="col-md-12">
            <?php echo form_open_multipart('agent/profile/updateProfilePic', 'onsubmit="return validateProfilePic();"'); ?>
              <div class="col-sm-6 ol-md-6 col-xs-12">
                <div class="white-box">
                  <h3>Upload Image</h3>
                  <label for="input-file-now-custom-1">Images are cropped to 650px by 650px</label>
                  <input type="file" name="image" id="input-file-now-custom-1" class="dropify" data-max-file-size="10M" data-default-file="<?php echo file_exists($row['image_url']) ? $row['image_url'] : 'assets/agent/images/users/default-user.jpg'; ?>" />
                </div>
              </div>
            </div>
            <input type="hidden" name="image_url_old" value="<?php echo $row['image_url']; ?>" />
            <div class="form-group m-b-0">
              <div class="col-sm-offset-3 col-sm-9">
                <button type="submit" class="btn btn-info waves-effect waves-light">Update Profile</button>
              </div>
            </div>
          </form>
        </div>
        <div class="clearfix"></div>
      </div>    
    </div>
  </div>
</div>
</div> 
<!-- /.row -->
</div>
<!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_agent') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n"; ?>
<script src="assets/agent/js/jasny-bootstrap.js"></script>
<!-- jQuery file upload -->
<script src="assets/agent/bower_components/dropify/dist/js/dropify.min.js"></script>
        <script>
            $(document).ready(function(){
                // Basic
                $('.dropify').dropify();

                // Translated
                $('.dropify-fr').dropify({
                    messages: {
                        default: 'Glissez-déposez un fichier ici ou cliquez',
                        replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                        remove:  'Supprimer',
                        error:   'Désolé, le fichier trop volumineux'
                    }
                });

                // Used events
                var drEvent = $('#input-file-events').dropify();

                drEvent.on('dropify.beforeClear', function(event, element){
                    return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
                });

                drEvent.on('dropify.afterClear', function(event, element){
                    alert('File deleted');
                });

                drEvent.on('dropify.errors', function(event, element){
                    console.log('Has Errors');
                });

                var drDestroy = $('#input-file-to-destroy').dropify();
                drDestroy = drDestroy.data('dropify')
                $('#toggleDropify').on('click', function(e){
                    e.preventDefault();
                    if (drDestroy.isDropified()) {
                        drDestroy.destroy();
                    } else {
                        drDestroy.init();
                    }
                })
            });
        </script>
        <script type="text/javascript">


          $('#category_id').change(function(){
            var category = $("#category_id option:selected").text().toUpperCase();
            if(category == 'SOCIAL INFLUENCER')
            {
            // show influencer_category
            $("#category_social_influencer_id").parent().parent().removeClass("hidden");
          }
          else
          {
            // hide influencer category
            $("#category_social_influencer_id").parent().parent().addClass("hidden");
          }
        });
          
  function validateBasicInfo()
  {
    var category_id = document.getElementById("category_id").value;
    var country_id = document.getElementById("country_id").value;
    var gender = document.getElementById("gender").value;
    
    if(category_id == 0 ){
      $('#category_id').parent().parent().addClass('has-error');

      // clear others
      $('#country_id').parent().parent().removeClass('has-error');
      $('#gender').parent().parent().removeClass('has-error');
      return false;
    }
    else if(country_id == 0 ){
      $('#country_id').parent().parent().addClass('has-error');

      // clear others
      $('#category_id').parent().parent().removeClass('has-error');
      $('#gender').parent().parent().removeClass('has-error');

      return false;
    }
    else if(gender == 0 ){
      $('#gender').parent().parent().addClass('has-error');

      // clear others
      $('#country_id').parent().parent().removeClass('has-error');
      $('#category_id').parent().parent().removeClass('has-error');

      return false;
    }
    else {
      return true;
    }
  }

  function validateProfilePic()
  {
    if(document.getElementById("input-file-now-custom-1").files.length == 0 ){
      // no file uploaded
      return true;
    }
    else {
      var fileName = $("#input-file-now-custom-1").val(); // alert('fileName: ' + fileName + "\n" + typeof(fileName));
      fileName = fileName.toLowerCase();
      
      if((fileName.lastIndexOf("jpg")===fileName.length-3) || (fileName.lastIndexOf("jpeg")===fileName.length-4) || (fileName.lastIndexOf("png")===fileName.length-3) || (fileName.lastIndexOf("gif")===fileName.length-3))
      {
        //alert("OK");
        return true;
      }
      else
      {
        alert("Please upload valid image file.");
        return false;
      }
    }
  }
</script>

</body>
</html>
