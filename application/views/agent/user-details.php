  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">User Details</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "agent/"; ?>">Dashboard</a></li>
            <li><a href="<?php echo base_url() . "agent/"; ?>users">Users</a></li>
            <li class="active">Details</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- row -->
      <div class="row">
        <div class="col-md-12">
          <div class="white-box">
            <div class="table-responsive">
              <table class="table table-bordered table-striped">
                <tbody>
                  <tr>
                    <th>Full Name</th>
                    <td><?php echo $row['full_name']; ?></td>
                  </tr>
                  <tr>
                    <th>Role</th>
                    <td><?php echo $row['role_name']; ?></td>
                  </tr>
                  <tr>
                    <th>Email</th>
                    <td><a href="mailto:<?php echo $row['email']; ?>"><?php echo $row['email']; ?></a></td>
                  </tr>
                    <th>Phone</th>
                    <td><?php echo $row['phone']; ?></td>
                  </tr>
                  <tr>
                    <th>Gender</th>
                    <td><?php echo $row['gender']; ?></td>
                  </tr>
                  <tr>
                    <th>Last Login</th>
                    <td><?php echo $row['last_login'] == "0000-00-00 00:00:00" ? "-" : date('M d, Y h:i A', strtotime($row['last_login'])); ?></td>
                  </tr>
                </tbody>
              </table>
            </div>
            <p class="text-center">
              <button type="button" class="btn btn-info waves-effect waves-light center" onclick="history.go(-1); return false;">&larr; Back</button>
            </p>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_admin') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n"; ?>
<script src="assets/js/jasny-bootstrap.js"></script>

</body>
</html>
