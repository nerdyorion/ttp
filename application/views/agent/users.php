  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Users</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "agent/"; ?>">Dashboard</a></li>
            <li class="active">Users</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- row -->
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
          <div class="white-box">
            <!--<h3>Users</h3>-->
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#all" aria-controls="all" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-view-list"></i></span><span class="hidden-xs"> View All</span></a></li>
              <li role="presentation"><a href="#add" aria-controls="add" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-plus"></i></span> <span class="hidden-xs">Add New</span></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="all"> 
                <div class="col-md-12">
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Full Name</th>
                          <th>Email</th>
                          <th>Gender</th>
                          <th>Role</th>
                          <th>Last Login</th>
                          <th class="text-nowrap">Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php if(empty($rows)): ?>
                        <tr>
                          <td colspan="6" align="center">No data returned.</td>
                        </tr>
                      <?php else: ?>
                      <?php $sn = 1; foreach ($rows as $row): ?>
                        <tr>
                          <td><?php echo $sn++; ?></td>
                          <td><?php echo $row['full_name']; ?></td>
                          <td><a href="mailto:<?php echo $row['email']; ?>"><?php echo $row['email']; ?></a></td>
                          <td><?php echo $row['gender']; ?></td>
                          <td><?php echo $row['role_name']; ?></td>
                          <td><?php echo $row['last_login'] == "0000-00-00 00:00:00" ? "-" : date('M d, Y h:i A', strtotime($row['last_login'])); ?></td>
                          <!--<td><?php echo date('M d, Y h:i A', strtotime($row['last_login'])) == "Jan 01, 1970 12:00 AM" ? "-" : date('M d, Y h:i A', strtotime($row['last_login'])); ?></td>-->
                          <td class="text-nowrap">
                            <a href="admin123/users/edit/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a> 
                            <a href="admin123/users/delete/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Delete"> <span class="text-danger"><i class="fa fa-close text-danger m-r-10" onclick="if(confirm('Are you sure you want to delete this record?')) return true; else return false;"></i></span> </a> 
                            <a href="admin123/users/view/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="View"> <i class="fa fa-folder-open text-inverse m-r-10"></i> </a> 
                          </td>
                        </tr>
                      <?php endforeach; ?>
                      <?php endif; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
                <!--
                <div class="col-md-3 pull-right">
                  <p><a href="?page=2">Pagination</a>.</p>
                </div>
                -->
                <div class="clearfix"></div>
              </div>
              <div role="tabpanel" class="tab-pane" id="add">
                <div class="col-md-12">
                <?php echo form_open('admin123/users', 'class="form-horizontal", onsubmit="return validate();"'); ?>
                  <div class="form-group">
                    <label for="role_id" class="col-sm-2 control-label">Role: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <select class="form-control" name="role_id" id="role_id" required="required">
                        <option value="0" selected="selected">-- Select --</option>
                        <option value="1">Super Administrator</option>
                        <option value="2">Administrator</option>
                        <option value="3">Report Admin</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="full_name" class="col-sm-2 control-label">Full Name: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="full_name" maxlength="2000" id="full_name" value="" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">Email: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="email" maxlength="255" id="email" value="" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="phone" class="col-sm-2 control-label">Phone:</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" onkeypress='return event.charCode >= 48 && event.charCode <= 57' name="phone" maxlength="13" id="phone" value="" placeholder="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="password" class="col-sm-2 control-label">Password: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="password" class="form-control" name="password" maxlength="8000" id="password" value="" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="confirm_password" class="col-sm-2 control-label">Confirm Password: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="password" class="form-control" name="confirm_password" maxlength="8000" id="confirm_password" value="" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="gender" class="col-sm-2 control-label">Gender: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <select class="form-control" name="gender" id="gender" required="required">
                        <option value="0" selected="selected">-- Select --</option>
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="address" class="col-sm-2 control-label">Address:</label>
                    <div class="col-sm-10">
                      <textarea class="form-control" name="address" id="address" maxlength="8000"></textarea>
                    </div>
                  </div>
                  <div class="form-group m-b-0">
                    <div class="col-sm-offset-3 col-sm-9">
                      <button type="submit" class="btn btn-info waves-effect waves-light">Add New</button>
                    </div>
                  </div>
                </form>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
      </div> 
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_agent') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n"; ?>
<script src="assets/js/jasny-bootstrap.js"></script>
<script type="text/javascript">
  function validate()
  {
    var password = document.getElementById("password").value;
    var confirm_password = document.getElementById("confirm_password").value;
    var gender = document.getElementById("gender").value;
    var role_id = document.getElementById("role_id").value;
    if(password != confirm_password ){
      alert('Passwords do not match.');
      document.getElementById("confirm_password").focus();
      return false;
    }
    else if(gender == 0 ){
      alert('Please specify gender.');
      return false;
    }
    else if(role_id == 0 ){
      alert('Please specify role.');
      return false;
    }
    else {
      return true;
    }
  }
</script>

</body>
</html>