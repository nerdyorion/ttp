<style>
.embed-container
{
  position: relative; 
  padding-bottom: 56.25%; 
  height: 0; 
  overflow: hidden; 
  max-width: 100%; 
  height: auto;
} 
.embed-container iframe, .embed-container object, .embed-container embed
{
  position: absolute; 
  top: 0; 
  left: 0; 
  width: 100%; 
  height: 100%; 
}
.col-sm-3
{
  padding-bottom: 30px;
}

#instafeed a img {
  /*width: 50%;
  height: 50%;*/
}
</style>
<script type="text/javascript" src="assets/js/instafeed.min.js"></script>
<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <!-- <li data-target="#myCarousel" data-slide-to="0" class="active"></li> -->
  </ol>

  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <!-- <img src="assets/images/banner.png" alt="Image"> -->
      <img src="assets/images/register-banner.png" alt="Image" />
      <div class="carousel-caption">
        <h3 style="color: #f9f9f9;"><?php echo $row['stage_name']; ?> Artist Page</h3>
        <p style="color: #f9f9f9;">see all brief profile and portfolio below</p>
      </div>      
    </div>

  </div>
</div>

  <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container marketing">

      <!--
      <div class="video-responsive">
        <iframe width="420" height="315" src="http://www.youtube.com/embed/6xisazZX9bA" frameborder="0" allowfullscreen></iframe>
      </div>
    -->

    <div class="row">
      <div class="col-lg-12 text-center">
        <h3>IG Images</h3>

        <?php if(!empty($row['ig_access_token'])): ?>
          <script type="text/javascript">
            var feed = new Instafeed({
            userId: '<?php echo $row['ig_user_id']; ?>', // femy_jj 3034077079  // omotayoodupitan 2309509411
            get: 'user',
              // get: 'tagged',
              // tagName: 'awesome',
              after: function() {
                $('#instafeed a img').addClass('img-responsive col-md-3 col-lg-3');
                $('#instafeed a').attr('target', '_blank');
              },
              clientId: '<?php echo $this->config->item('ig_client_id'); ?>',
              accessToken: '<?php echo $row['ig_access_token']; ?>',
              sortBy: 'most-recent',
              limit: 20,
              resolution: 'standard_resolution'
              // 2309509411.c049152.756f9063be6a4043be933c6f625f1030
            });
            feed.run();
          </script>

          <div id="instafeed"></div>
        <?php else: ?>
          <p><code>IG access not granted by artist yet</code></p>
        <?php endif; ?>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-12 text-center">
        <h3>Videos</h3>

        <?php if(empty($portfolio)): ?>
          <p> no video(s) added yet </p>
        <?php else: ?>
          <?php $not_available = 1; ?>
          <?php foreach ($portfolio as $item): ?>
            <?php if($item['type'] == 'video'): ?>

              <?php if($item['source'] == 'youtube'): ?>
                <div class="col-sm-3">
                  <div class='embed-container'><iframe width="100%" height="235px" src="https://www.youtube.com/embed/<?php echo $item['media_id']; ?>" frameborder="0" allowfullscreen></iframe></div>
                </div>
                <?php $not_available = 0; ?>
              <?php endif; ?>

              <?php if($item['source'] == 'vimeo'): ?>
                <div class="col-sm-3">
                  <div class='embed-container'><iframe src='https://player.vimeo.com/video/<?php echo $item['media_id']; ?>' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>
                </div>
                <?php $not_available = 0; ?>
              <?php endif; ?>
            <?php else: ?>
              <?php $not_available = $not_available == 0 ? 0 : 1; ?>
            <?php endif; ?>
          <?php endforeach; ?>

          <?php if($not_available == 1): ?>
            <p> no video(s) added yet </p>
          <?php endif; ?>
        <?php endif; ?>

      </div><!-- /.col-lg-12 -->
    </div><!-- /.row -->

    <div class="row">
      <div class="col-lg-12 text-center">
        <h3>Audio</h3>

        <?php if(empty($portfolio)): ?>
          <p> no audio(s) added yet </p>
        <?php else: ?>
          <?php $not_available = 1; ?>
          <?php foreach ($portfolio as $item): ?>
            <?php if($item['type'] == 'audio'): ?>

              <?php if($item['source'] == 'youtube'): ?>
                <div class="col-sm-3">
                  <div class='embed-container'><iframe width="100%" height="235px" src="https://www.youtube.com/embed/<?php echo $item['media_id']; ?>" frameborder="0" allowfullscreen></iframe></div>
                </div>
                <?php $not_available = 0; ?>
              <?php endif; ?>

              <?php if($item['source'] == 'soundcloud'): ?>
                <div class="col-sm-3">
                  <div class='embed-container'><iframe width="100%" height="150px" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/<?php echo $item['media_id']; ?>&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe></div>
                </div>
                <?php $not_available = 0; ?>
              <?php endif; ?>

            <?php else: ?>
              <?php $not_available = $not_available == 0 ? 0 : 1; ?>
            <?php endif; ?>
          <?php endforeach; ?>

          <?php if($not_available == 1): ?>
            <p> no audio(s) added yet </p>
          <?php endif; ?>
        <?php endif; ?>

      </div><!-- /.col-lg-12 -->
    </div><!-- /.row -->






    <!-- Three columns of text below the carousel -->
    <div class="row">
      <div class="col-lg-4">
        Artist profile below
        <br />
        <!-- <?php // echo "<pre />"; var_dump($row); echo "</pre>"; ?> -->
        <ul class="list-group">
          <li class="list-group-item"><b>Fullname</b>: <?php echo $row['first_name'] . ' ' . $row['last_name']; ?></li>
          <li class="list-group-item"><b>Stage Name</b>: <?php echo $row['stage_name']; ?></li>
          <li class="list-group-item"><b>Country</b>: <?php echo $row['country_name']; ?></li>
          <li class="list-group-item"><b>Category</b>: <?php echo $row['artist_category']; ?> <?php echo strtoupper($row['artist_category']) == "SOCIAL INFLUENCER" ? '(' . $row['artist_category_social_influencer'] . ')' : ''; ?></li>
        </ul>

      </div><!-- /.col-lg-4 -->
      <div class="col-lg-4">

        <div class="thumbnail">
          <a href="<?php echo $this->config->base_url() . '@' . $row['username']; ?>" class="thumbnail link-thumbnail">
            <img src="<?php echo showImage($row['image_url']); ?>" alt="<?php echo $row['stage_name']; ?>" class="img-responsive" title="Book <?php echo $row['stage_name']; ?>"><!-- 200x170 -->
          </a>
          <div class="caption">
            <p><a href="<?php echo getURL('book', $row['username']); ?>" class="btn btn-primary" role="button">Book Now</a></p>
            <?php if(!empty($row['twitter_url'])): ?>
              <a class="twitter-follow-button" href="https://twitter.com/<?php echo $row['twitter_url']; ?>" data-size="large">Follow @<?php echo $row['twitter_url']; ?></a>
            <?php endif; ?>
          </div>
        </div>
        <ul class="list-group">
          <li class="list-group-item"><b><u>Services</u></b></li>
          <?php if(empty($services)): ?>
            <li class="list-group-item">... no services added ...</li>
          <?php endif; ?>
          <?php foreach ($services as $item): ?>
            <li class="list-group-item"><?php echo $item['name']; ?></li>
          <?php endforeach; ?>
        </ul>
      </div><!-- /.col-lg-4 -->
      <div class="col-lg-4">
        Artist Portfolio below
        <br />
        <ul class="list-group">
          <?php if(empty($portfolio)): ?>
            <li class="list-group-item">... no portfolio added ...</li>
          <?php endif; ?>
          <pre />
          <?php foreach ($portfolio as $item): ?>
            <li><a href="portfolio/<?php echo $item['id']; ?>"><?php echo $item['name']; ?></a></li>
          <?php endforeach; ?>
        </ul>

      </div><!-- /.col-lg-4 -->
    </div><!-- /.row -->

    <?php $this->load->view('footer'); echo "\n"; ?>

    <script type="text/javascript">
      $(document).ready(function()
      {
      //
    });
  </script>

</div><!-- /.container -->

</body>
</html>