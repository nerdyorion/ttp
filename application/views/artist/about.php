  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    </ol>

    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="assets/images/banner.png" alt="Image">
        <div class="carousel-caption">
          <h3 style="color: #000000;">MobileFun</h3>
          <p style="color: #000000;">Who we are...</p>
        </div>      
      </div>

    </div>
  </div>

  <div style="width: 90%; margin: auto;">

    <div class="clearfix"></div>

    <div class="container-fluid text-center bg-grey">
      <!-- <h2>MOBILE CONTENTS</h2> -->
      <!-- <h4>What we have created</h4> -->

      <div class="row text-center">

        <ol class="breadcrumb text-left">
          <li><a href="<?php echo base_url(); ?>"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Home</a></li>
          <li class="active">About</li>
        </ol>
        <div class="col-sm-12" style="border-bottom: 2px solid #feb25a; margin-bottom: 10px; ">
          <h3 class="text-left">About</h3>
        </div>

        <div class="jumbotron" style="display: inline-block; width: 100%">
          <div class="col-sm-12">
            <h3 style="color: #000000; ">Catering to your requirements, no need to remember codes and keywords :)</h3>
            <p style="color: #000000; font-size: 1em;">
              Our platform is more than just another average online retailer. We save you from the hassles of having to cram subscription codes and keywords with just a simple click as well as give you a positive online subscription shopping experience. Forget about struggling to recollect codes. Purchase the subscriptions you need in a few clicks or taps, depending on the device you use to access the Internet. We work to make your life more enjoyable.
            </p>
            <div class="clearfix"></div>
            <div class="clearfix"></div>
          </div>


          <div class="col-sm-12">


            <div class="row">
              <div class="col-lg-4">
                <button type="button" class="btn btn-warning btn-circle btn-xl"><i class="glyphicon glyphicon-ok"></i></button>
                <h2>Simple</h2>
                <p style="color: #000000; font-size: 1em;">Click desired service &mdash; Select network and enter phone number &mdash; Click Subscribe</p>
                <p><a class="btn btn-default" href="music-crbt" role="button">Explore &raquo;</a></p>
              </div>
              <div class="col-lg-4">
                <button type="button" class="btn btn-warning btn-circle btn-xl"><i class="glyphicon glyphicon-flash"></i></button>
                <h2>Fast</h2>
                <p style="color: #000000; font-size: 1em;">You receive real-time response upon subscribing and you can enjoy the service.</p>
                <p><a class="btn btn-default" href="mobile-contents" role="button">Get Started &raquo;</a></p>
              </div>
              <div class="col-lg-4">
                <button type="button" class="btn btn-warning btn-circle btn-xl"><i class="glyphicon glyphicon-star"></i></button>
                <h2>Efficient</h2>
                <p style="color: #000000; font-size: 1em;">Forget codes. Use MobileFun and click your way to subscription. Available 24/7</p>
                <p><a class="btn btn-default" href="music-crbt" role="button">Explore &raquo;</a></p>
              </div>
            </div>
          </div>

        </div>


      </div>

    </div>

    <div class="clearfix"></div>

  </div>


<?php $this->load->view('footer'); echo "\n"; ?>

</body>
</html>