  <style type="text/css">
    table.table.table-bordered tbody tr td{
       vertical-align: middle;
    }
  </style>
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Bookings</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "artist/"; ?>">Dashboard</a></li>
            <li class="active">Bookings</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- row -->
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
          <div class="white-box">
            <!--<h3>Products</h3>-->
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#all" aria-controls="all" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-view-list"></i></span><span class="hidden-xs"> View All</span></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="all"> 
                <div class="col-md-12">
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>#</th>
                          <?php if(strtoupper($user_row['artist_category_social_influencer']) == "CONTENT CREATOR"): ?>
                            <th>Brief</th>
                            <th>Industry</th>
                            <th>Requested on</th>
                          <?php elseif(strtoupper($user_row['artist_category_social_influencer']) == "AMPLIFIER"): ?>
                            <th>Industry</th>
                            <th>Budget</th>
                            <th>Duration</th>
                            <th>Tier</th>
                            <th>Requested on</th>
                          <?php else: ?>
                            <th>Event</th>
                            <th>Date</th>
                          <?php endif; ?>
                          <th>Agent</th>
                          <th>Booking Cost (N)</th>
                          <th>Status</th>
                          <th class="text-nowrap">Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php if(empty($rows)): ?>
                          <tr>
                            <td colspan="8" align="center">No data returned.</td>
                          </tr>
                        <?php else: ?>
                          <?php foreach ($rows as $row): ?>
                            <tr>
                              <td><?php echo $sn++; ?></td>
                              <?php if(strtoupper($user_row['artist_category_social_influencer']) == "CONTENT CREATOR"): ?>
                                <?php

                                $content_industry_id = getArrayKey($content_industry, 'id', trim($row['content_industry_id']));
                                $row_content_industry = isset($content_industry[$content_industry_id]) ? $content_industry[$content_industry_id]['value'] : '-';

                                ?>
                                <td title="<?php echo dashIfEmpty($row['content_brief']); ?>"><?php echo ellipsize(dashIfEmpty($row['content_brief']), 50); ?></td>
                                <td title="<?php echo dashIfEmpty($row_content_industry); ?>"><?php echo dashIfEmpty($row_content_industry); ?></td>
                                <td title="<?php echo $row['date_created'] == "0000-00-00 00:00:00" ? "-" : date('M d, Y h:i A', strtotime($row['date_created'])); ?>"><?php echo $row['date_created'] == "0000-00-00 00:00:00" ? "-" : date('M d, Y h:i A', strtotime($row['date_created'])); ?></td>
                              <?php elseif(strtoupper($user_row['artist_category_social_influencer']) == "AMPLIFIER"): ?>
                                <?php

                                $content_industry_id = getArrayKey($content_industry, 'id', trim($row['content_industry_id']));
                                $row_content_industry = isset($content_industry[$content_industry_id]) ? $content_industry[$content_industry_id]['value'] : '-';

                                $content_budget_id = getArrayKey($content_budget, 'id', trim($row['content_budget_id']));
                                $row_content_budget = isset($content_budget[$content_budget_id]) ? $content_budget[$content_budget_id]['value'] : '-';

                                $content_campaign_duration_id = getArrayKey($content_campaign_duration, 'id', trim($row['content_campaign_duration_id']));
                                $row_content_campaign_duration = isset($content_campaign_duration[$content_campaign_duration_id]) ? $content_campaign_duration[$content_campaign_duration_id]['value'] : '-';

                                $content_tier_id = getArrayKey($content_tier, 'id', trim($row['content_tier_id']));
                                $row_content_tier = isset($content_tier[$content_tier_id]) ? $content_tier[$content_tier_id]['value'] : '-';

                                ?>
                                <td title="<?php echo dashIfEmpty($row_content_industry); ?>"><?php echo dashIfEmpty($row_content_industry); ?></td>
                                <td title="<?php echo dashIfEmpty($row_content_budget); ?>"><?php echo dashIfEmpty($row_content_budget); ?></td>
                                <td title="<?php echo dashIfEmpty($row_content_campaign_duration); ?>"><?php echo dashIfEmpty($row_content_campaign_duration); ?></td>
                                <td title="<?php echo dashIfEmpty($row_content_tier); ?>"><?php echo dashIfEmpty($row_content_tier); ?></td>
                                <td title="<?php echo $row['date_created'] == "0000-00-00 00:00:00" ? "-" : date('M d, Y h:i A', strtotime($row['date_created'])); ?>"><?php echo $row['date_created'] == "0000-00-00 00:00:00" ? "-" : date('M d, Y h:i A', strtotime($row['date_created'])); ?></td>
                              <?php else: ?>
                                <td title="<?php echo dashIfEmpty($row['event_name']); ?>"><?php echo dashIfEmpty($row['event_name']); ?></td>
                                <td title="<?php echo $row['event_date'] . ' ' . $row['event_start_time'] . '-' . $row['event_end_time']; ?>"><?php echo $row['event_date'] . ' ' . $row['event_start_time'] . '-' . $row['event_end_time']; ?></td>
                              <?php endif; ?>
                          <td><?php echo $row['full_name']; ?></td>
                          <td title="<?php echo $row['booking_cost'] == 0 ? 'Pending' : number_format($row['booking_cost']); ?>"><?php echo $row['booking_cost'] == 0 ? '<a href="javascript:void(0);" class="bg-warning biga" style="color: #ffffff;">&nbsp;Pending&nbsp;</a>' : number_format($row['booking_cost']); ?></td>
                          <td>
                            <?php echo $row['accepted'] == 1 ? '<a href="javascript:void(0);" class="bg-success biga" style="color: #ffffff;">&nbsp;Accepted&nbsp;</a>' : ''; ?> 
                            <?php echo $row['rejected'] == 1 ? '<a href="javascript:void(0);" class="bg-danger biga" style="color: #ffffff;">&nbsp;Rejected&nbsp;</a>' : ''; ?> 
                            <?php echo $row['rejected'] != 1 && $row['paid'] == 0 ? '<a href="javascript:void(0);" class="bg-danger biga" style="color: #ffffff;">&nbsp;Unpaid&nbsp;</a>' : ''; ?>
                          </td>
                          <!-- <td title="<?php //echo $row['description']; ?>"><?php //echo ellipsize($row['event_description'], 50); ?></td> -->
                          <!-- <td><?php // echo $row['available_after_hours'] == 0 ? '<a href="javascript:void(0);" class="bg-danger" style="color: #ffffff;">&nbsp;No&nbsp;</a>' : '<a href="javascript:void(0);" class="bg-success" style="color: #ffffff;">&nbsp;Yes&nbsp;</a>'; ?></td> -->
                          <!-- <td><?php // echo $row['available_on_weekends'] == 0 ? '<a href="javascript:void(0);" class="bg-danger" style="color: #ffffff;">&nbsp;No&nbsp;</a>' : '<a href="javascript:void(0);" class="bg-success" style="color: #ffffff;">&nbsp;Yes&nbsp;</a>'; ?></td> -->
                          <td class="text-nowrap">
                            <!--<a href="artist/bookings/delete/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Delete"> <span class="text-danger"><i class="fa fa-trash text-danger m-r-10" onclick="if(confirm('Are you sure you want to delete this record?')) return true; else return false;"></i></span> </a>-->
                            <a href="artist/bookings/view/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="View"> <i class="fa fa-folder-open text-inverse m-r-10"></i> View booking </a> 
                          </td>
                        </tr>
                      <?php endforeach; ?>
                      <?php endif; ?>
                      </tbody>
                    </table>
                  </div>
                  <!-- <p><?php //echo $links; ?></p> -->
                </div>
                
                <div class="col-md-3 pull-right pagination">
                  <p><?php echo $links; ?></p>
                </div>
                
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
      </div> 
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_artist') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n"; ?>
<script src="assets/js/jasny-bootstrap.js"></script>

</body>
</html>