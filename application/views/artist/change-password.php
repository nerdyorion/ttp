  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Change Password</h4>
          <ol class="breadcrumb">
            <li><a href="./">Dashboard</a></li>
            <li class="active">Change Password</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      <!-- row -->
      <div class="row">
        <div class="col-sm-12">
          <div class="white-box">
            <div class="row">
              <div class="col-md-12">
                <?php echo form_open('artist/Change-Password', 'class="form-horizontal", onsubmit="return validate();"'); ?>
                  <div class="form-group">
                    <label for="current_password" class="col-sm-3 control-label">Current Password: <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                      <input type="password" class="form-control" name="current_password" maxlength="8000" id="current_password" value="" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="new_password" class="col-sm-3 control-label">New Password: <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                      <input type="password" class="form-control" name="new_password" maxlength="8000" id="new_password" value="" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="confirm_password" class="col-sm-3 control-label">Confirm Password: <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                      <input type="password" class="form-control" name="confirm_password" maxlength="8000" id="confirm_password" value="" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group m-b-0">
                    <div class="col-sm-offset-3 col-sm-9">
                      <button type="submit" class="btn btn-info waves-effect waves-light">Change Password</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        
      </div>
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_artist') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n";  // load footer view ?>
<script src="assets/js/jasny-bootstrap.js"></script>
<script type="text/javascript">
  function validate()
  {
    var current_password = document.getElementById("current_password").value;
    var new_password = document.getElementById("new_password").value;
    var confirm_password = document.getElementById("confirm_password").value;
    if(new_password != confirm_password ){
      alert('Passwords do not match.');
      document.getElementById("confirm_password").focus();
      return false;
    }
    else {
      return true;
    }
  }
</script>

</body>
</html>
