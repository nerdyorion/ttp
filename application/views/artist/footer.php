    <footer class="footer text-center">&copy; <?php echo date('Y'); ?>  </footer>
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="assets/artist/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="assets/artist/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="assets/artist/bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!--Nice scroll JavaScript -->
<script src="assets/artist/js/jquery.nicescroll.js"></script>

<!--Morris JavaScript -->
<!--<script src="assets/artist/bower_components/raphael/raphael-min.js"></script>
<script src="assets/artist/bower_components/morrisjs/morris.js"></script>-->
<!--Wave Effects -->
<script src="assets/artist/js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="assets/artist/js/myadmin.js"></script>
<!--<script src="assets/artist/js/dashboard1.js"></script>-->
<!--<script src="assets/artist/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="assets/artist/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>-->
<script type="text/javascript">
  $(document).ready(function() {
    <?php if(!empty($error_code)): ?>
      //alert("1" + '<?php echo $error; ?>');
      $("#error-msg").fadeToggle(350);
      $("#error-msg").html('<i class="ti-close"></i> <?php echo $error; ?> <a href="javascript:void(0);">Click anywhere to close</a>');
    <?php elseif((empty($error_code)) && (!empty($error))): ?>
      //alert("0" + <?php echo $error; ?>);
      $("#success-msg").fadeToggle(350);
      $("#success-msg").html('<i class="ti-check"></i> <?php echo $error; ?> <a href="javascript:void(0);">Click anywhere to close</a>');
    <?php else: ?>
    <?php endif; ?>
  });
</script>