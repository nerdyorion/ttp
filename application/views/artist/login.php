  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <!-- <li data-target="#myCarousel" data-slide-to="0" class="active"></li> -->
    </ol>

    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <!-- <img src="assets/images/banner.png" alt="Image"> -->
        <img src="assets/images/register-banner.png" alt="Image" />
        <div class="carousel-caption">
          <h3 style="color: #f9f9f9;">Registration is free!</h3>
          <p style="color: #f9f9f9;">...</p>
        </div>      
      </div>

    </div>
  </div>

  <!-- Marketing messaging and featurettes
  ================================================== -->
  <!-- Wrap the rest of the page in another container to center all the content. -->

  <div class="container marketing">

    <!-- Three columns of text below the carousel -->
    <div class="row" align="center">
      <div class="col-lg-6 col-lg-offset-3 text-center">
        <h2>Login</h2>

        <hr />

        <?php if($error_code == 0 && !empty($error)): ?>
          <div class="alert alert-success alert-dismissable fade in">
            <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Success!</strong> <?php echo $error; ?>
          </div>
        <?php elseif($error_code == 1 && !empty($error)): ?>
          <div class="alert alert-danger alert-dismissable fade in">
            <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Error!</strong> <?php echo $error; ?>
          </div>
        <?php else: ?>
        <?php endif; ?>

        <div class="clearfix"></div>

        <?php echo form_open('/login/secure', 'class="form-horizontal", onsubmit="return validate();"'); ?>
          <div class="form-group">
            <label for="email" class="col-sm-3 control-label">Email: <span class="required">*</span></label>
            <div class="col-sm-9">
              <input type="email" class="form-control" name="email" id="email" placeholder="johndoe@yahoo.com" maxlength="255" required="required" />
            </div>
          </div>
          <div class="form-group">
            <label for="password" class="col-sm-3 control-label">Password: <span class="required">*</span></label>
            <div class="col-sm-9">
              <input type="password" class="form-control" name="password" id="password" placeholder="********" required="required" />
            </div>
          </div>
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Sign in</button>
              </div>
            </div>
            <div class="pull-right"><a href="/Forgot-Password">Forgot Password?</a></div>
          </form>
        </div><!-- /.col-lg-12 -->
      </div><!-- /.row -->

      <?php $this->load->view('footer'); echo "\n"; ?>

      <script type="text/javascript">
        function validate()
        {
          $(':input[type="submit"]').prop('disabled', true);
          return true;
        }
      </script>

    </div><!-- /.container -->

  </body>
  </html>