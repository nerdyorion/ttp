  <div class="navbar-default sidebar nicescroll" role="navigation">
    <div class="sidebar-nav navbar-collapse ">
      <ul class="nav" id="side-menu">
        <li class="nav-small-cap">Menu</li>
        <li class=""> <a href="<?php echo base_url() . "artist/"; ?>" class="waves-effect"><i class="fa fa-dashboard"></i> Dashboard</a> </li>
        <li> <a href="artist/calendar" class="waves-effect"><i class="ti-calendar fa fa-calendar"></i> Calendar</a> </li>
        <li> <a href="artist/bookings" class="waves-effect"><i class="ti-location-pin fa-map-marker"></i> Bookings</a> </li>
        <li> <a href="artist/services" class="waves-effect"><i class="ti-upload fa fa-upload" aria-hidden="true"></i> Services</a> </li>
        <li> <a href="artist/portfolio" class="waves-effect"><i class="ti-user fa fa-user" aria-hidden="true"></i> Portfolio</a> </li>
        <li> <a href="artist/analytics" class="waves-effect"><i class="ti-bar-chart fa-bar-chart"></i> Analytics (RBT)</a> </li>
      </ul>
    </div>
    <!-- /.sidebar-collapse -->
  </div>

  <style type="text/css">
  div.fixed {
    position: fixed;
    bottom: 0;
    left: 0;
    width: auto;
    border: 3px solid #73AD21;
    z-index: 1;
  </style>
  <?php if(!is_null($this->session->userdata('user_id_former_logged_in')) && !is_null($this->session->userdata('user_username_former_logged_in'))): ?>
    <!-- Trigger -->
    <div class="fixed">
      <button type="button" class="bet_time btn btn-info btn-sm" onclick="location.href='<?php echo base_url() . "artist/home/switchbacktoadmin/" . $this->session->userdata('user_id_former_logged_in'); ?>';">Log back in <?php echo $this->session->userdata('user_username_former_logged_in'); ?>?</button>
    </div>
  <?php endif; ?>