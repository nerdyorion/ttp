  <style type="text/css">
    table.table.table-bordered tbody tr td{
       vertical-align: middle;
    }
  </style>
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Products</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "admin123/"; ?>">Dashboard</a></li>
            <li class="active">Products</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- row -->
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
          <div class="white-box">
            <!--<h3>Products</h3>-->
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#all" aria-controls="all" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-view-list"></i></span><span class="hidden-xs"> View All</span></a></li>
              <li role="presentation"><a href="#add" aria-controls="add" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-plus"></i></span> <span class="hidden-xs">Add New</span></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="all"> 
                <div class="col-md-12">
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th></th>
                          <th>Product Name</th>
                          <th>Category</th>
                          <th>Price (NGN)</th>
                          <th>Subscriptions</th>
                          <th>Active?</th>
                          <th class="text-nowrap">Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php if(empty($rows)): ?>
                        <tr>
                          <td colspan="6" align="center">No data returned.</td>
                        </tr>
                      <?php else: ?>
                      <?php foreach ($rows as $row): ?>
                        <tr>
                          <td><?php echo $sn++; ?></td>
                          <td><div class="media">
                            <div class="media-left">
                              <a href="assets/images/products/<?php echo $row['image_url']; ?>" target="_blank">
                              <img class="media-object" src="assets/images/products/<?php echo $row['image_url_thumb']; ?>" alt="<?php echo $row['name']; ?>">
                              </a>
                            </div>
                          </div></td>
                          <td><?php echo $row['name']; ?></td>
                          <td><?php echo $row['category_name']; ?></td>
                          <td><?php echo $row['price']; ?></td>
                          <td><?php echo $row['count_subscriptions']; ?></td>
                          <td><?php echo $row['active'] == 0 ? '<a href="javascript:void(0);" class="bg-danger" style="color: #ffffff;">&nbsp;No&nbsp;</a>' : '<a href="javascript:void(0);" class="bg-success" style="color: #ffffff;">&nbsp;Yes&nbsp;</a>'; ?></td>
                          <td class="text-nowrap">
                            <a href="admin123/Products/edit/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a> 
                            <a href="admin123/Products/delete/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Delete"> <span class="text-danger"><i class="fa fa-trash text-danger m-r-10" onclick="if(confirm('Are you sure you want to delete this record?')) return true; else return false;"></i></span> </a> 
                            <?php echo $row['active'] == 0 ? '<a href="admin123/Products/enable/' . $row['id'] . '" data-toggle="tooltip" data-original-title="Enable"> <i class="fa fa-check text-inverse m-r-10"></i> </a> ' : '<a href="admin123/Products/disable/' . $row['id'] . '" data-toggle="tooltip" data-original-title="Disable"> <i class="fa fa-close text-inverse m-r-10"></i> </a> '; ?>
                            <a href="admin123/Products/view/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="View"> <i class="fa fa-folder-open text-inverse m-r-10"></i> </a> 
                            <a href="<?php echo base_url() . $row['slug']; ?>" data-toggle="tooltip" data-original-title="View in Store Front" target="_blank"> <i class="fa fa-external-link text-inverse m-r-10"></i> </a> 
                          </td>
                        </tr>
                      <?php endforeach; ?>
                      <?php endif; ?>
                      </tbody>
                    </table>
                  </div>
                  <!-- <p><?php //echo $links; ?></p> -->
                </div>
                
                <div class="col-md-3 pull-right pagination">
                  <p><?php echo $links; ?></p>
                </div>
                
                <div class="clearfix"></div>
              </div>
              <div role="tabpanel" class="tab-pane" id="add">
                <div class="col-md-12">
                <?php echo form_open_multipart('admin123/Products', 'class="form-horizontal", onsubmit="return validate();"'); ?>
                  <div class="form-group">
                    <label for="category_id" class="col-sm-2 control-label">Category: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <select class="form-control" name="category_id" id="category_id" required="required">
                        <option value="0" selected="selected">-- Select --</option>
                        <?php foreach ($categories as $row): ?>
                        <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                        </tr>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="full_name" class="col-sm-2 control-label">Product Name: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="name" maxlength="8000" id="name" value="" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="image" class="col-sm-2 control-label">Image (650x650 px): <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                        <div class="form-control" data-trigger="fileinput">
                          <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                          <span class="fileinput-filename"></span>
                        </div>
                        <span class="input-group-addon btn btn-default btn-file">
                        <span class="fileinput-new">Select file</span>
                        <span class="fileinput-exists">Change</span>
                        <input type="file" name="image" id="image" required="required"></span>
                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="price" class="col-sm-2 control-label">Price (NGN): <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="number" min="0" class="form-control" name="price" maxlength="13" id="price" value="" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="duration" class="col-sm-2 control-label">Duration: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <select class="form-control" name="duration" id="duration" required="required">
                        <option value="1 Month" selected="selected">1 Month</option>
                        <option value="1 Week">1 Week</option>
                        <option value="5 Days">5 Days</option>
                        <option value="4 Days">4 Days</option>
                        <option value="3 Days">3 Days</option>
                        <option value="2 Days">2 Days</option>
                        <option value="1 Day">1 Day</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="description" class="col-sm-2 control-label">Description:</label>
                    <div class="col-sm-10">
                      <textarea class="form-control" name="description" id="description" maxlength="20000" rows="5"></textarea>
                    </div>
                  </div>

                  <label for="codes" class="col-sm-2 control-label">Service Codes:</label>
                  <div class="col-sm-offset-2">
                    <ul class="nav nav-tabs">
                      <li class="active"><a data-toggle="tab" href="#mtn">MTN</a></li>
                      <li><a data-toggle="tab" href="#airtel">AIRTEL</a></li>
                      <li><a data-toggle="tab" href="#glo">GLO</a></li>
                      <li><a data-toggle="tab" href="#emts">EMTS</a></li>
                      <li><a data-toggle="tab" href="#ntel">NTEL</a></li>
                    </ul>

                    <div class="tab-content">
                      <div id="mtn" class="tab-pane fade in active">
                        <div class="form-group">
                          <label for="mtn_service_code" class="col-sm-2 control-label">Code:</label>
                          <div class="col-sm-5">
                            <input type="text" class="form-control" name="mtn_service_code" maxlength="255" id="mtn_service_code" value="" placeholder="">
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="mtn_service_keyword" class="col-sm-2 control-label">Keyword:</label>
                          <div class="col-sm-5">
                            <input type="text" class="form-control" name="mtn_service_keyword" maxlength="255" id="mtn_service_keyword" value="" placeholder="">
                          </div>
                        </div>
                      </div>
                      <div id="airtel" class="tab-pane fade">
                        <div class="form-group">
                          <label for="airtel_service_code" class="col-sm-2 control-label">Code:</label>
                          <div class="col-sm-5">
                            <input type="text" class="form-control" name="airtel_service_code" maxlength="255" id="airtel_service_code" value="" placeholder="">
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="airtel_service_keyword" class="col-sm-2 control-label">Keyword:</label>
                          <div class="col-sm-5">
                            <input type="text" class="form-control" name="airtel_service_keyword" maxlength="255" id="airtel_service_keyword" value="" placeholder="">
                          </div>
                        </div>
                      </div>
                      <div id="glo" class="tab-pane fade">
                        <div class="form-group">
                          <label for="glo_service_code" class="col-sm-2 control-label">Code:</label>
                          <div class="col-sm-5">
                            <input type="text" class="form-control" name="glo_service_code" maxlength="255" id="glo_service_code" value="" placeholder="">
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="glo_service_keyword" class="col-sm-2 control-label">Keyword:</label>
                          <div class="col-sm-5">
                            <input type="text" class="form-control" name="glo_service_keyword" maxlength="255" id="glo_service_keyword" value="" placeholder="">
                          </div>
                        </div>
                      </div>
                      <div id="emts" class="tab-pane fade">
                        <div class="form-group">
                          <label for="emts_service_code" class="col-sm-2 control-label">Code:</label>
                          <div class="col-sm-5">
                            <input type="text" class="form-control" name="emts_service_code" maxlength="255" id="emts_service_code" value="" placeholder="">
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="emts_service_keyword" class="col-sm-2 control-label">Keyword:</label>
                          <div class="col-sm-5">
                            <input type="text" class="form-control" name="emts_service_keyword" maxlength="255" id="emts_service_keyword" value="" placeholder="">
                          </div>
                        </div>
                      </div>
                      <div id="ntel" class="tab-pane fade">
                        <div class="form-group">
                          <label for="ntel_service_code" class="col-sm-2 control-label">Code:</label>
                          <div class="col-sm-5">
                            <input type="text" class="form-control" name="ntel_service_code" maxlength="255" id="ntel_service_code" value="" placeholder="">
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="ntel_service_keyword" class="col-sm-2 control-label">Keyword:</label>
                          <div class="col-sm-5">
                            <input type="text" class="form-control" name="ntel_service_keyword" maxlength="255" id="ntel_service_keyword" value="" placeholder="">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="form-group m-b-0">
                    <div class="col-sm-offset-3 col-sm-9">
                      <button type="submit" class="btn btn-info waves-effect waves-light">Add New</button>
                    </div>
                  </div>
                </form>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
      </div> 
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_artist') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n";  // load footer view ?>
<script src="assets/js/jasny-bootstrap.js"></script>
<script type="text/javascript">
  function validate()
  {
    var category_id = document.getElementById("category_id").value;
    
    if(category_id == 0 ){
      alert('Please specify category.');
      return false;
    }
    else if(document.getElementById("image").files.length == 0 ){
      alert('Please upload image.');
      return false;
    }
    else {
      var fileName = $("#image").val();
      fileName = fileName.toLowerCase();
      
      if((fileName.lastIndexOf("jpg")===fileName.length-3) || (fileName.lastIndexOf("jpeg")===fileName.length-4) || (fileName.lastIndexOf("png")===fileName.length-3) || (fileName.lastIndexOf("gif")===fileName.length-3) || (fileName.lastIndexOf("bmp")===fileName.length-3))
      {
        //alert("OK");
        return true;
      }
      else
      {
        alert("Please upload valid image file.");
        return false;
      }
    }
  }
</script>

</body>
</html>