  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <!-- <li data-target="#myCarousel" data-slide-to="0" class="active"></li> -->
    </ol>

    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <!-- <img src="assets/images/banner.png" alt="Image"> -->
        <img src="assets/images/register-banner.png" alt="Image" />
        <div class="carousel-caption">
          <h3 style="color: #f9f9f9;">Registration is free!</h3>
          <p style="color: #f9f9f9;">...</p>
        </div>      
      </div>

    </div>
  </div>

  <!-- Marketing messaging and featurettes
  ================================================== -->
  <!-- Wrap the rest of the page in another container to center all the content. -->

  <div class="container marketing">

    <!-- Three columns of text below the carousel -->
    <div class="row" align="center">
      <div class="col-lg-6 col-lg-offset-3 text-center">
        <h2><?php echo $role_id == 3 ? 'Artist' : 'Booking Agent'; ?></h2>

        <hr />

        <?php if($error_code == 0 && !empty($error)): ?>
          <div class="alert alert-success alert-dismissable fade in">
            <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Success!</strong> <?php echo $error; ?>
          </div>
        <?php elseif($error_code == 1 && !empty($error)): ?>
          <div class="alert alert-danger alert-dismissable fade in">
            <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Error!</strong> <?php echo $error; ?>
          </div>
        <?php else: ?>
        <?php endif; ?>

        <div class="clearfix"></div>

        <?php echo form_open('/register/create', 'class="form-horizontal", onsubmit="return validate();"'); ?>
          <input type="hidden" name="role_id" value="<?php echo $role_id; ?>">
          <?php if($role_id == 3) : ?>
          <!-- Artist Stage Name -->
          <div class="form-group">
            <label for="stage_name" class="col-sm-3 control-label">Stage name: <span class="required">*</span></label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="stage_name" id="stage_name" placeholder="Jon Dee" maxlength="255" required="required" />
            </div>
          </div>
          <?php endif; ?>
          <div class="form-group">
            <label for="first_name" class="col-sm-3 control-label">First name: <span class="required">*</span></label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="first_name" id="first_name" placeholder="John" maxlength="255" required="required" />
            </div>
          </div>
          <div class="form-group">
            <label for="last_name" class="col-sm-3 control-label">Last name: <span class="required">*</span></label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Doe" maxlength="255" required="required" />
            </div>
          </div>
          <div class="form-group">
            <label for="email" class="col-sm-3 control-label">Email: <span class="required">*</span></label>
            <div class="col-sm-9">
              <input type="email" class="form-control" name="email" id="email" placeholder="johndoe@yahoo.com" maxlength="255" required="required" />
            </div>
          </div>
          <div class="form-group">
            <label for="username" class="col-sm-3 control-label">Username: <span class="required">*</span></label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="username" id="username" placeholder="jdoe" maxlength="255" required="required" />
              <span class="help-block text-left hidden">Username exists!</span>
            </div>
          </div>
          <div class="form-group">
            <label for="category_id" class="col-sm-3 control-label">Category: <span class="text-danger">*</span></label>
            <div class="col-sm-9">
              <select class="form-control" name="category_id" id="category_id" required="required">
                <option value="0" selected="selected">-- Select --</option>
                <?php foreach ($categories as $row): ?>
                  <option value="<?php echo $row['id']; ?>"><?php echo $row['category_name']; ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
          <div class="form-group hidden">
            <label for="category_social_influencer_id" class="col-sm-3 control-label">Influencer Category:</label>
            <div class="col-sm-9">
              <select class="form-control" name="category_social_influencer_id" id="category_social_influencer_id">
                <!-- <option value="0" selected="selected">-- Select --</option> -->
                <?php foreach ($categories_social_influencer as $row): ?>
                  <option value="<?php echo $row['id']; ?>"><?php echo $row['category_name']; ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="country_id" class="col-sm-3 control-label">Country: <span class="text-danger">*</span></label>
            <div class="col-sm-9">
              <select class="form-control" name="country_id" id="country_id" required="required">
                <option value="0" selected="selected">-- Select --</option>
                <?php foreach ($countries as $row): ?>
                  <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="password" class="col-sm-3 control-label">Password: <span class="required">*</span></label>
            <div class="col-sm-9">
              <input type="password" class="form-control" name="password" id="password" placeholder="********" required="required" />
            </div>
          </div>
          <div class="form-group">
            <label for="confirm_password" class="col-sm-3 control-label">Confirm Password: <span class="required">*</span></label>
            <div class="col-sm-9">
              <input type="password" class="form-control" name="confirm_password" id="confirm_password" placeholder="********" required="required" />
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-9">
              <div class="checkbox">
                <label for="terms">
                  <input type="checkbox" id="terms" name="terms" /> <a href="terms-and-conditions" target="_blank"> I accept the terms and conditions</a>
                </label>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Sign up</button>
              </div>
            </div>
          </form>
        </div><!-- /.col-lg-12 -->
      </div><!-- /.row -->

      <?php $this->load->view('footer'); echo "\n"; ?>

      <script type="text/javascript">

        $('#category_id').change(function(){
          var category = $("#category_id option:selected").text().toUpperCase();
          if(category == 'SOCIAL INFLUENCER')
          {
            // show influencer_category
            $("#category_social_influencer_id").parent().parent().removeClass("hidden");
          }
          else
          {
            // hide influencer category
            $("#category_social_influencer_id").parent().parent().addClass("hidden");
          }
        });

        $('#username').focusout(function(){
          var username = $('#username').val();

          if(username != '')
          {
            setTimeout(function(){
              $.ajax({
               type: 'GET',
               url: '/register/userexists/' + username,
               success: function( data ) {
                if(data == '1')
                {
                  $('#username').parent().parent().addClass('has-error');
                  $('#username').next(':hidden').removeClass('hidden').show();
                  $(':input[type="submit"]').prop('disabled', true);
                  clearTimeout();
                }
                else
                {
                  // do nothing, username is fine
                  $('#username').parent().parent().removeClass('has-error').addClass('has-success');
                  $('#username').next().addClass('hidden').hide();
                  $(':input[type="submit"]').prop('disabled', false);
                  clearTimeout();
                }
              },
              error: function(xhr, status, error) {
                // check status && error
                // console.log(status);
                // console.log(error);
              },
              dataType: 'text'
            });
          }, 1000);
        }
      });

      function validate()
        {
          var password = document.getElementById("password").value;
          var confirm_password = document.getElementById("confirm_password").value;
          var country_id = document.getElementById("country_id").value;
          var category_id = document.getElementById("category_id").value;
          if(password != confirm_password ){
            $('#confirm_password').parent().parent().addClass('has-error');

            // clear others
            $('#category_id').parent().parent().removeClass('has-error');
            $('#country_id').parent().parent().removeClass('has-error');
            $('#terms').parent().parent().removeClass('has-error');

            document.getElementById("confirm_password").focus();
            return false;
          }
          else if(category_id == 0 ){
            $('#category_id').parent().parent().addClass('has-error');

            // clear others
            $('#confirm_password').parent().parent().removeClass('has-error');
            $('#country_id').parent().parent().removeClass('has-error');
            $('#terms').parent().parent().removeClass('has-error');

            return false;
          }
          else if(country_id == 0 ){
            $('#country_id').parent().parent().addClass('has-error');

            // clear others
            $('#confirm_password').parent().parent().removeClass('has-error');
            $('#category_id').parent().parent().removeClass('has-error');
            $('#terms').parent().parent().removeClass('has-error');

            return false;
          }
          else if (document.getElementById("terms").checked != true){

            // clear others
            $('#confirm_password').parent().parent().removeClass('has-error');
            $('#category_id').parent().parent().removeClass('has-error');
            $('#country_id').parent().parent().removeClass('has-error');

            $('#terms').parent().parent().addClass('has-error');
            alert('Accept terms to continue.');
            return false;
          }
          else {
            $(':input[type="submit"]').prop('disabled', true);
            return true;
          }
        }
      </script>

    </div><!-- /.container -->

  </body>
  </html>