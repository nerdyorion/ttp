  <style type="text/css">
    table.table.table-bordered tbody tr td{
       vertical-align: middle;
    }
  </style>
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Services</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "artist/"; ?>">Dashboard</a></li>
            <li class="active">Services</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- row -->
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
          <div class="white-box">
            <!--<h3>Products</h3>-->
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#all" aria-controls="all" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-view-list"></i></span><span class="hidden-xs"> View All</span></a></li>
              <li role="presentation"><a href="#add" aria-controls="add" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-plus"></i></span> <span class="hidden-xs">Add New</span></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="all"> 
                <div class="col-md-12">
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Name</th>
                          <th>Description</th>
                          <!-- <th>Available after Hours?</th>
                          <th>Available on Weekends?</th> -->
                          <th class="text-nowrap">Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php if(empty($rows)): ?>
                        <tr>
                          <td colspan="5" align="center">No data returned.</td>
                        </tr>
                      <?php else: ?>
                      <?php foreach ($rows as $row): ?>
                        <tr>
                          <td><?php echo $sn++; ?></td>
                          <td title="<?php echo $row['name']; ?>"><?php echo $row['name']; ?></td>
                          <td title="<?php echo $row['description']; ?>"><?php echo ellipsize($row['description'], 50); ?></td>
                          <!-- <td><?php // echo $row['available_after_hours'] == 0 ? '<a href="javascript:void(0);" class="bg-danger" style="color: #ffffff;">&nbsp;No&nbsp;</a>' : '<a href="javascript:void(0);" class="bg-success" style="color: #ffffff;">&nbsp;Yes&nbsp;</a>'; ?></td>
                          <td><?php // echo $row['available_on_weekends'] == 0 ? '<a href="javascript:void(0);" class="bg-danger" style="color: #ffffff;">&nbsp;No&nbsp;</a>' : '<a href="javascript:void(0);" class="bg-success" style="color: #ffffff;">&nbsp;Yes&nbsp;</a>'; ?></td> -->
                          <td class="text-nowrap">
                            <a href="artist/services/edit/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> Edit  |</a> 
                            <a href="artist/services/delete/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Delete"> <span class="text-danger"><i class="fa fa-trash text-danger m-r-10" onclick="if(confirm('Are you sure you want to delete this record?')) return true; else return false;"></i></span> Delete </a> 
                           <!-- <a href="<?php echo base_url() . '@' . $username . '/services'; ?>" data-toggle="tooltip" data-original-title="View" target="_blank"> <i class="fa fa-external-link text-inverse m-r-10"></i> </a> -->
                          </td>
                        </tr>
                      <?php endforeach; ?>
                      <?php endif; ?>
                      </tbody>
                    </table>
                  </div>
                  <!-- <p><?php //echo $links; ?></p> -->
                </div>
                
                <div class="col-md-3 pull-right pagination">
                  <p><?php echo $links; ?></p>
                </div>
                
                <div class="clearfix"></div>
              </div>
              <div role="tabpanel" class="tab-pane" id="add">
                <div class="col-md-12">
                <?php echo form_open('artist/services/create', 'class="form-horizontal"'); ?>
                  <div class="form-group">
                    <label for="full_name" class="col-sm-2 control-label">Name: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="name" maxlength="20000" id="name" value="" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="description" class="col-sm-2 control-label">Description:</label>
                    <div class="col-sm-10">
                      <textarea class="form-control" name="description" id="description" rows="5"></textarea>
                    </div>
                  </div>
                  <!-- <div class="form-group">
                    <label for="full_name" class="col-sm-2 control-label"> </label>
                    <div class="col-sm-10">
                      <div class="checkbox checkbox-primary">
                        <input name="available_after_hours" id="available_after_hours" type="checkbox" value="1">
                        <label for="available_after_hours"> Available after hours? </label>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="full_name" class="col-sm-2 control-label"> </label>
                    <div class="col-sm-10">
                      <div class="checkbox checkbox-primary">
                        <input name="available_on_weekends" id="available_on_weekends" type="checkbox" value="1">
                        <label for="available_on_weekends"> Available on Weekends? </label>
                      </div>
                    </div>
                  </div> -->
                  <div class="form-group m-b-0">
                    <div class="col-sm-offset-3 col-sm-9">
                      <button type="submit" class="btn top-book btn-info waves-effect waves-light">Add New</button>
                    </div>
                  </div>
                </form>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
      </div> 
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_artist') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n"; ?>
<script src="assets/js/jasny-bootstrap.js"></script>

</body>
</html>