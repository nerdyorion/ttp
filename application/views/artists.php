<!-- <script src="https://use.fontawesome.com/ad9d3ef28e.js"></script> -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<style type="text/css">
.dropdown.dropdown-lg .dropdown-menu {
  margin-top: -1px;
  padding: 6px 20px;
}
.input-group-btn .btn-group {
  display: flex !important;
}
.btn-group .btn {
  border-radius: 0;
  margin-left: -1px;
}
.btn-group .btn:last-child {
  border-top-right-radius: 4px;
  border-bottom-right-radius: 4px;
}
.btn-group .form-horizontal .btn[type="submit"] {
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px;
}
.form-horizontal .form-group {
  margin-left: 0;
  margin-right: 0;
}
.form-group .form-control:last-child {
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px;
}

@media screen and (min-width: 768px) {
  #adv-search {
    width: 500px;
    margin: 0 auto;
  }
  .dropdown.dropdown-lg {
    position: static !important;
  }
  .dropdown.dropdown-lg .dropdown-menu {
    min-width: 500px;
  }
}
div.fixed {
  position: fixed;
  bottom: 0;
  right: 0;
  width: auto;
  border: 3px solid #73AD21;
  z-index: 1;
</style>
<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <!-- <li data-target="#myCarousel" data-slide-to="0" class="active"></li> -->
  </ol>

  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <!-- <img src="assets/images/banner.png" alt="Image"> -->
      <img src="assets/images/register-banner.png" alt="Image" />
      <div class="carousel-caption">
        <h3 style="color: #f9f9f9;">See All Talents!</h3>
        <p style="color: #f9f9f9;">...</p>
      </div>      
    </div>

  </div>
</div>
<div class="container">

  <!-- Trigger the modal with a button -->
  <div class="fixed">
    <button type="button" class="bet_time btn btn-info btn-sm book1" data-toggle="modal" data-target="#myModal">Not sure what you're looking for?</button>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="input-group" id="adv-search">
       <div class="row">
        <input type="text" name="artist_name" id="artist_name" value="<?php echo isset($_GET['name']) ? trim($_GET['name']) : ''; ?>" class="form-control" placeholder="Search for artist" style="border: solid 2px #337ab7 !important; border-radius: 12px; margin-bottom: 10px; text-align: center;" />
		  </div>
       
       <div class="row">
        <div class="input-group-btn">
          <div class="btn-group" role="group">
            <div class="dropdown dropdown-lg">
              <button type="button" style="width: 500px !important; color: #fff; background: #337ab7 !important; padding-top: 20px; padding-bottom: 20px; margin-right: 10px;" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">CLICK TO FILTER RESULTS <span class="caret"></span></button>
              <div class="dropdown-menu dropdown-menu-right" role="menu">
                <!-- <form class="form-horizontal" role="form" method="get" action=""> -->
                  <?php echo form_open('artists', 'class="form-horizontal", method="get", role="form"'); ?>
                  <div class="form-group">
                    <label for="filter">Filter by</label>
                    <select class="form-control" name="category" id="category">
                      <option value="0" <?php echo !isset($_GET['category']) ? 'selected="selected"' : ''; ?>>All Categories</option>
                      <?php foreach ($categories as $row): ?>
                        <option value="<?php echo $row['id']; ?>" <?php $category = isset($_GET['category']) ? trim($_GET['category']) : '-'; echo $category == $row['id'] ? 'selected="selected"' : ''; ?>><?php echo $row['category_name']; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="from">Budget</label>
                    <div class="row col-md-12">
                      <div class="col-md-5">
                        <input class="form-control col-md-3" type="number" min="0" value="<?php echo isset($_GET['from']) ? trim($_GET['from']) : '0'; ?>" step="50000" name="from" id="from" />
                      </div>
                      <div class="col-md-1">
                        to
                      </div>
                      <div class="col-md-5">
                        <input class="form-control col-md-3" type="number" min="0" value="<?php echo isset($_GET['to']) ? trim($_GET['to']) : '100000000'; ?>" step="50000" name="to" id="to" />
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="location">Location</label>
                    <select class="form-control" name="location" id="location">
                      <option value="0" <?php echo !isset($_GET['location']) ? 'selected="selected"' : ''; ?>>All</option>
                      <option value="1" <?php echo isset($_GET['location']) && $_GET['location'] == "1" ? 'selected="selected"' : ''; ?>>Lagos</option>
                      <option value="2" <?php echo isset($_GET['location']) && $_GET['location'] == "2" ? 'selected="selected"' : ''; ?>>North 1</option>
                      <option value="3" <?php echo isset($_GET['location']) && $_GET['location'] == "3" ? 'selected="selected"' : ''; ?>>North 2</option>
                      <option value="4" <?php echo isset($_GET['location']) && $_GET['location'] == "4" ? 'selected="selected"' : ''; ?>>South East</option>
                      <option value="5" <?php echo isset($_GET['location']) && $_GET['location'] == "5" ? 'selected="selected"' : ''; ?>>South South</option>
                      <option value="6" <?php echo isset($_GET['location']) && $_GET['location'] == "6" ? 'selected="selected"' : ''; ?>>South West</option>
                    </select>
                  </div>
                  <button type="submit" class="btn btn-primary" style="color: #ffffff;"><i class="fa fa-search" aria-hidden="true"></i></button>
                </form>
              </div>
            </div>
            <button type="button" class="btn btn-primary" style="color: #ffffff; margin-right: 10px" onclick="search();"><i class="fa fa-search" aria-hidden="true"></i>SEARCH</button>
            <button type="button" class="btn btn-primary" style="color: #ffffff;" onclick="location.href = '<?php echo base_url(); ?>artists';"><i class="fa fa-refresh" aria-hidden="true"></i>REFRESH</button>
          </div>
        </div>
		</div>
        
        
      </div>
    </div>
  </div>
</div>
</div>
<div class="clearfix"></div>
  <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container marketing">

      <!-- Three columns of text below the carousel -->
      <div class="row">
        <div class="col-lg-12 text-center">


          <?php if(empty($rows)): ?>
            <div class="col-sm-12">
              <p class="text-muted">... no record(s) found ...</p>
            </div>
          <?php else: ?>
            <?php foreach ($rows as $row): ?>
              <div class="col-sm-3">
                <div class="thumbnail">
                  <a href="<?php echo $this->config->base_url() . '@' . $row['username']; ?>" class="thumbnail link-thumbnail">
                    <img src="<?php echo showImage($row['image_url']); ?>" alt="<?php echo $row['stage_name']; ?>" class="img-responsive" title="Book <?php echo $row['stage_name']; ?>"><!-- 200x170 -->
                  </a>
                  <div class="caption">
                    <div style="min-height: 40px; "><h4><?php echo $row['stage_name']; ?></h4></div>
                    <p class="price"><?php echo $row['category_name']; ?></p>
                    <p><a href="<?php echo getURL('book', $row['username']); ?>" class="btn btn-primary" role="button">Book Now</a></p>
                  </div>
                </div>
              </div>
            <?php endforeach; ?>
          <?php endif; ?>

          <div class="clearfix"></div>

          <div class="row col-md-12">
            <div class="col-md-3 pull-right pagination">
              <p><?php echo $links; ?></p>
            </div>

            <div class="col-md-2 pull-left">
              <p><a href="#" class="btn btn-primary" role="button">View &rarr;</a></p>
            </div>
          </div>
        </div><!-- /.col-lg-12 -->
      </div><!-- /.row -->


      <!-- Modal -->
      <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <?php echo form_open('artists', 'class="form-horizontal", method="get", role="form"'); ?>
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Search</h4>
            </div>
            <div class="modal-body">
              <!-- <p>Some text in the modal.</p> -->
              <div class="form-group">
                <label for="category">Type of Talent</label>
                <select class="form-control" name="category" id="category">
                  <option value="0" <?php echo !isset($_GET['category']) ? 'selected="selected"' : ''; ?>>All</option>
                  <?php foreach ($categories as $row): ?>
                    <option value="<?php echo $row['id']; ?>" <?php $category = isset($_GET['category']) ? trim($_GET['category']) : '-'; echo $category == $row['id'] ? 'selected="selected"' : ''; ?>><?php echo $row['category_name']; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
              <div class="form-group">
                <label for="event">Event</label>
                <select class="form-control" name="event" id="event">
                  <option value="0" <?php echo !isset($_GET['event']) ? 'selected="selected"' : ''; ?>>All</option>
                  <?php foreach ($content_event_type as $row): ?>
                    <option value="<?php echo $row['id']; ?>" <?php $event = isset($_GET['event']) ? trim($_GET['event']) : '-'; echo $event == $row['id'] ? 'selected="selected"' : ''; ?>><?php echo $row['value']; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
              <div class="form-group">
                <label for="budget">Budget</label>
                <select class="form-control" name="budget" id="budget">
                  <option value="0" <?php echo !isset($_GET['budget']) ? 'selected="selected"' : ''; ?>>All</option>
                  <?php foreach ($content_budget as $row): ?>
                    <option value="<?php echo $row['id']; ?>" <?php $budget = isset($_GET['budget']) ? trim($_GET['budget']) : '-'; echo $budget == $row['id'] ? 'selected="selected"' : ''; ?>><?php echo $row['value']; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
              <div class="form-group">
                <label for="from">Genre</label>
                <div class="row col-md-12">
                  
              <div class="form-group col-sm-3">
                <label for="checkbox" class="control-label"> </label>
                <div class="form-inline row"><!-- col-sm-offset-2  -->
                  <div class="form-group col-sm-12">
                    <div class="checkbox checkbox-primary">
                    <!-- <input name="genre_gospel" id="genre_gospel" type="checkbox" value="1" <?php echo empty($row['genre_gospel']) ? '' : 'checked="checked"'; ?>> -->
                    <input name="genre_gospel" id="genre_gospel" type="checkbox" value="1" <?php echo isset($_GET['genre_gospel']) ? 'checked="checked"' : ''; ?>>
                    <label for="genre_gospel"> Gospel </label>
                  </div>
                  </div>
                </div>
              </div>

              <div class="form-group col-sm-3">
                <label for="checkbox" class="control-label"> </label>
                <div class="form-inline row"><!-- col-sm-offset-2  -->
                  <div class="form-group col-sm-12">
                    <div class="checkbox checkbox-primary">
                    <!-- <input name="genre_hiphop" id="genre_hiphop" type="checkbox" value="1" <?php echo empty($row['genre_hiphop']) ? '' : 'checked="checked"'; ?>> -->
                    <input name="genre_hiphop" id="genre_hiphop" type="checkbox" value="1" <?php echo isset($_GET['genre_hiphop']) ? 'checked="checked"' : ''; ?>>
                    <label for="genre_hiphop"> Hip Hop </label>
                  </div>
                  </div>
                </div>
              </div>

              <div class="form-group col-sm-3">
                <label for="checkbox" class="control-label"> </label>
                <div class="form-inline row"><!-- col-sm-offset-2  -->
                  <div class="form-group col-sm-12">
                    <div class="checkbox checkbox-primary">
                    <!-- <input name="genre_rap" id="genre_rap" type="checkbox" value="1" <?php echo empty($row['genre_rap']) ? '' : 'checked="checked"'; ?>> -->
                    <input name="genre_rap" id="genre_rap" type="checkbox" value="1" <?php echo isset($_GET['genre_rap']) ? 'checked="checked"' : ''; ?>>
                    <label for="genre_rap"> Rap </label>
                  </div>
                  </div>
                </div>
              </div>

              <div class="form-group col-sm-3">
                <label for="checkbox" class="control-label"> </label>
                <div class="form-inline row"><!-- col-sm-offset-2  -->
                  <div class="form-group col-sm-12">
                    <div class="checkbox checkbox-primary">
                    <input name="genre_afro_pop" id="genre_afro_pop" type="checkbox" value="1" <?php echo isset($_GET['genre_afro_pop']) ? 'checked="checked"' : ''; ?>>
                    <label for="genre_afro_pop"> Afro Pop </label>
                  </div>
                  </div>
                </div>
              </div>

              <div class="form-group col-sm-3">
                <label for="checkbox" class="control-label"> </label>
                <div class="form-inline row"><!-- col-sm-offset-2  -->
                  <div class="form-group col-sm-12">
                    <div class="checkbox checkbox-primary">
                    <input name="genre_fuji" id="genre_fuji" type="checkbox" value="1" <?php echo isset($_GET['genre_fuji']) ? 'checked="checked"' : ''; ?>>
                    <label for="genre_fuji"> Fuji </label>
                  </div>
                  </div>
                </div>
              </div>

              <div class="form-group col-sm-3">
                <label for="checkbox" class="control-label"> </label>
                <div class="form-inline row"><!-- col-sm-offset-2  -->
                  <div class="form-group col-sm-12">
                    <div class="checkbox checkbox-primary">
                    <input name="genre_r_and_b" id="genre_r_and_b" type="checkbox" value="1" <?php echo isset($_GET['genre_r_and_b']) ? 'checked="checked"' : ''; ?>>
                    <label for="genre_r_and_b"> R &amp; B </label>
                  </div>
                  </div>
                </div>
              </div>

              <div class="form-group col-sm-3">
                <label for="checkbox" class="control-label"> </label>
                <div class="form-inline row"><!-- col-sm-offset-2  -->
                  <div class="form-group col-sm-12">
                    <div class="checkbox checkbox-primary">
                    <input name="genre_jazz" id="genre_jazz" type="checkbox" value="1" <?php echo isset($_GET['genre_jazz']) ? 'checked="checked"' : ''; ?>>
                    <label for="genre_jazz"> Jazz </label>
                  </div>
                  </div>
                </div>
              </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i> Search</button>
              <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>
            </div>
          </form>
        </div>

      </div>
    </div>

    <?php $this->load->view('footer'); echo "\n"; ?>

  </div><!-- /.container -->

  <script type="text/javascript">
    document.getElementById("artist_name")
    .addEventListener("keyup", function(event) {
      event.preventDefault();
      if (event.keyCode === 13) {
        // document.getElementById("id_of_button").click();
        search();
      }
    });

    function search()
    {
      var this_url = '<?php echo base_url(); ?>artists';
      var artist_name = document.getElementById("artist_name").value;
    // alert(artist_name);
    if(artist_name == "" || artist_name == null)
    {
      // do nothing
    }
    else
    {
      location.href = this_url + '?name=' + artist_name;
    }
  }
</script>
</body>
</html>