
      <!-- FOOTER -->
      <footer>
        <p class="pull-right"><a href="<?php echo current_url(); ?>#"><span class="glyphicon glyphicon-circle-arrow-up" aria-hidden="true"></span> Back to top</a></p>
        <p>&copy; <?php echo date('Y'); ?> Company, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
      </footer>
  <!-- <footer class="container-fluid text-center">
    <p>&copy; <?php echo date('Y'); ?></p>
  </footer> -->
  
 <!-- Twitter -->
<script>window.twttr = (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0],
    t = window.twttr || {};
  if (d.getElementById(id)) return t;
  js = d.createElement(s);
  js.id = id;
  js.src = "https://platform.twitter.com/widgets.js";
  fjs.parentNode.insertBefore(js, fjs);

  t._e = [];
  t.ready = function(f) {
    t._e.push(f);
  };

  return t;
}(document, "script", "twitter-wjs"));</script>
<!-- jQuery -->
<script src="assets/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>