<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<base href="<?php echo base_url();?>">
<meta name="description" content="Africa's #1 Artist Booking and Entertainment Management Platform">
<meta name="author" content="nerdyorion">
<!-- For Google -->
<meta name="description" content="Africa's #1 Artist Booking and Entertainment Management Platform" />
<meta name="keywords" content="book, artist, singer, musician, dj, book artist" />

<meta name="author" content="nerdyorion" />
<meta name="copyright" content="<?php echo $this->config->base_url(); ?>terms" />
<meta name="application-name" content="US Uber" />

<!-- For Facebook -->
<meta property="og:title" content="US Uber" />
<meta property="og:type" content="article" />
<meta property="og:image" content="<?php echo $this->config->base_url(); ?>assets/images/logo-social.png" />
<meta property="og:url" content="<?php echo $this->config->base_url(); ?>" />
<meta property="og:description" content="Africa's #1 Artist Booking and Entertainment Management Platform" />

<!-- For Twitter -->
<meta name="twitter:card" content="summary" />
<meta name="twitter:title" content="US Uber" />
<meta name="twitter:description" content="Africa's #1 Artist Booking and Entertainment Management Platform" />
<meta name="twitter:image" content="<?php echo $this->config->base_url(); ?>assets/images/logo-social.png" />
<link href="assets/images/favicon.png" rel="shortcut icon" type="image/png" />
<title><?php echo $page_title; ?> | <?php echo $this->config->item('app_name'); ?></title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="assets/css/style1.css" rel="stylesheet">
  <!-- <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" /> -->
  
  <style>

    /* Font */
    body {
      font: "Roboto", sans-serif;
    }

    /* Text Color */
    h1, h2, h3, h4, h5, h6, span {
      color: #337ab7;
    }

    /* Link Color */
    a {
      color: #337ab7;
    }
    a:hover {
      color: #1a93e6;
    }

    /* Pagination Color */
    .pagination, .pagination li a, .pagination li a:visited, .pagination li a:link {
      color: #337ab7;
    }
    .pagination li a:active, .pagination li span {
      color: #337ab7;
    }
    .pagination li a:hover {
      color: #1a93e6;
    }

    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #101010;
      padding: 25px;
    }

    /*
    .carousel {
      min-height:200px;
    }
    */
    
    .carousel-inner img {
      width: 100%; /* Set width to 100% */
      margin: auto;
      min-height:400px;
      max-height:400px;
    }

    .carousel h3, .carousel p {
      color: #ffffff;
    }

    /* Hide the carousel text when the screen is less than 600 pixels wide */
  /*
  @media (max-width: 600px) {
    .carousel-caption {
      display: none; 
    }
  }
  */

  /* Spacing */
  .clearfix {
    margin-top: 20px;
  }

  /* Link Thumbnail */
  .link-thumbnail {
    margin-bottom: 0px;
  }

  /* Change button color */
  /*
  */
  .btn-primary,
  .btn-primary:link,
  /*.btn-primary:hover,*/
  .btn-primary:active,
  .btn-primary:visited,
  .btn-primary.active,
  .btn-primary:focus {
    background-color: #EF5023 !important;
    border-color: #EF5023;
  }
  .btn-primary:hover {
    background-color: #1a93e6 !important;
    border-color: #F05522;
  }
  .btn-primary {
    outline: 1px solid #ffffff;
  }

  /* Circle Glyphicon */
  .btn-circle.btn-xl {
    width: 150px;
    height: 150px;
    padding: 10px 16px;
    font-size: 24px;
    line-height: 1.33;
    border-radius: 75px;
  }

  /* Clean Google Map */
  html, body {
  overflow-x:hidden;
}
.google-maps {
  height: 420px;
  width: 100%;
  position: relative;
  overflow: hidden;
}
.google-maps iframe {
  position: absolute;
  top: 0;
  left: 0;
  width: 100% !important;
  height: 100% !important;
}
/*
.caption h4 p del:after{
  color: #F05522;
}*/
</style>
<link href="assets/css/carousel.css" rel="stylesheet" />
</head>
<body>
    <div class="navbar-wrapper">
      <div class="">

        <nav class="navbar navbar-inverse navbar-static-top">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="assets/images/logo2.png" style="margin-top: -29px;" /></a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav navbar-center" style="margin-left: 100px;">
                <li <?php echo $this->uri->segment(1) == '' ? 'class="active"' : '' ; ?>><a href="<?php echo base_url(); ?>">Home</a></li>
                <li><a href="#about">About</a></li>
                <li><a href="#about">Clients</a></li>
                <li <?php echo $this->uri->segment(1) == 'artists' ? 'class="active"' : '' ; ?>><a href="artists">Talent</a></li>
                <li><a href="#about">Blogs</a></li>
                <li><a href="#about">Contact</a></li>
               <!-- <li><a href="#contact">Contact</a></li> -->
             <!--   <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li role="separator" class="divider"></li>
                    <li class="dropdown-header">Nav header</li>
                    <li><a href="#">Separated link</a></li>
                    <li><a href="#">One more separated link</a></li>
                  </ul>
                </li>-->
                
              </ul>
              <ul class="nav navbar-nav navbar-right">
                <?php if(is_logged_in()): ?>
                  <li class="reg1" style="background-color: #fff;"><a href="dashboard"><span class="glyphicon glyphicon-home"></span> Dashboard</a></li>
                <?php else: ?>
                  <li><a href="register">Register</a></li>
                  <li><a href="login"><span class="glyphicon glyphicon-log-in"></span> Sign In</a></li>
                <?php endif; ?>
              </ul>
            </div>
          </div>
        </nav>

      </div>
    </div>
<!--   <nav class="navbar navbar-inverse">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
        <a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="assets/images/logo-white.png" style="margin-top: -10px;" /></a>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav">
          <li class="active"><a href="<?php echo base_url(); ?>">Home</a></li>
          <li><a href="about">About</a></li>
          <li><a href="contact">Contact</a></li>
        </ul>
      </div>
    </div>
  </nav> -->
