<style type="text/css">


/* CUSTOMIZE THE CAROUSEL
-------------------------------------------------- */

/* Carousel base class */
.carousel {
  height: 410px;
  margin-bottom: 60px;
}
/* Since positioning the image, we need to help out the caption */
.carousel-caption {
  z-index: 10;
}

/* Declare heights because of positioning of img element */
.carousel .item {
  height: 400px;
  background-color: #777;
}
.carousel-inner > .item > img {
  position: absolute;
  top: 0;
  left: 0;
  min-width: 100%;
  height: 400px;
}
	
	.carousel-inner img {
    width: 100%;
    margin: auto;
    min-height: 400px !important;
    max-height: 400px !important;
}


.carousel-inner {
    position: relative;
    width: 100%;
    overflow:visible !important;
    min-height: 400px !important;
}
</style>
    <!-- Carousel
      ================================================== -->
      <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
     <!--   <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
          <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>-->
        <div class="carousel-inner" role="listbox">
          <div class="item active">
            <img class="first-slide" src="<?php echo base_url(); ?>assets/images/slide111.jpg" alt="First slide">
         <div class="container">
            <div class="carousel-caption">
              <h1 style="font-weight: 700; font-size: 130px; letter-spacing: -8px;">#BookedbyUS</h1>
             <p style="font-weight: 300; letter-spacing: 2px;">Artiste Management | Corporate Entertainment | Talent Booking</p>
              <!-- <p><a class="btn btn-lg btn-primary" href="register" role="button">Sign up today</a></p>-->
            </div>
          </div> 
          
        </div>
      
       <!-- <div class="item">
          <img class="third-slide" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Third slide">
          <div class="container">
            <div class="carousel-caption">
              <h1>One more for good measure.</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a class="btn btn-lg btn-primary" href="discover" role="button">Browse artists</a></p>
            </div>
          </div>
        </div>-->
        
      </div>
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div><!-- /.carousel -->

  <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container marketing">
      
      
      

      <!-- Three columns of text below the carousel -->
      <h2 align="center" style="color: #153d70; font-weight: 300; letter-spacing: -2px; text-transform: uppercase;">Catergory</h2>
      <!--<h5 align="center" style="color: #153d70; font-weight: 300; letter-spacing: 2px; ">Artist Management • Corporate Entertainment • Talent Booking</h5>-->
      <div class="row">
        <div class="col-lg-4">
         
          <h2>Heading</h2>
          <!-- <p>One place to get booked or make bookings. 
            Artist? Get booked!
          Agent? Make bookings!.</p>
                   <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>-->
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <h2>Heading</h2>
         
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
         
          <h2>Heading</h2>
         
        </div><!-- /.col-lg-4 -->
      </div><!-- /.row -->

    </div>
    <!-- START THE FEATURETTES -->
    <h2 align="center" style="color: #153d70; font-weight: 700; text-transform: uppercase;">Featured Artists</h2>
    
    <div style="background-color: #061c2f ;">

     <div class="" style="padding-bottom: 50px; padding-top: 50px;">

      <div class="col-lg-12" > 
        <?php if(!empty($featured)): ?>
          <?php foreach($featured as $row): ?>
            <div class="col-lg-3" style=" background-color: #fff; border: 1px solid #fff;-webkit-box-shadow: 0px 10px 21px -4px rgba(0,0,0,0.75);
            -moz-box-shadow: 0px 10px 21px -4px rgba(0,0,0,0.75);
            box-shadow: 0px 10px 21px -4px rgba(0,0,0,0.75); margin 10px  ">
            <p align="center" style="margin: 10px 0 0 0; font-size: 20px; color: #153d70;"><a href="@<?php echo $row['username']; ?>"><?php echo $row['stage_name']; ?></a></p>
            <!-- <img src="https://placehold.it/650x650?text=650x650" alt="..." class="img-rounded img-responsive"> -->
            <div style="margin:  0 0 10px 0">
				<div class="col-lg-4" style="padding: 5px;">
					<div class="square" style="background-image:url('http://bookedbyus.com.ng/<?php echo showImage($row['image_url']); ?>');
    width: 100%;
    padding-bottom : 100%; /* = width for a 1:1 aspect ratio */
    margin:1.66%;
    background-position:center center;
    background-repeat:no-repeat;
    background-size:cover; 
"></div>
				</div>
          
           <div class="col-lg-8" style="padding: 5px; font-weight: lighter;">Born in Rio de Janeiro, Brazil, Monique is a resident DJ at New York City's most popular clubs, like the Box, The Darby and Southside. She also spins at private events with legends DJ Cassidy and Q</div>
           <div class="clearfix"></div>
				</div>
		
           
				<p align="center"><a title="View <?php echo $row['stage_name']; ?>" href="@<?php echo $row['username']; ?>" style="background-color: #000; width: 100%; padding: 10px 41%; margin: 0 auto;">Book Now</a></p>
            
            </div>
            <!--<p><a href="@<?php echo $row['username']; ?>"><u>View Profile &rarr;</u></a></p>-->
          <?php endforeach; ?>
        <?php endif; ?>
      </div>

      <div class="clearfix"></div>
      
    </div></div></div>

    <!-- /END THE FEATURETTES -->


    <!-- START THE FEATURETTES -->
    
    
    <div class="marketing" style="background-color: #040f19;">

      <!-- START THE FEATURETTES

      <hr class="featurette-divider">

      <div class="row featurette">
        <div class="col-md-7">
          <h2 class="featurette-heading">First featurette heading. <span class="text-muted">It'll blow your mind.</span></h2>
          <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
        </div>
        <div class="col-md-5">
          <img class="featurette-image img-responsive center-block" src="https://placehold.it/500x500?text=500x500" alt="Generic placeholder image">
        </div>
      </div>

      <hr class="featurette-divider">

      <div class="row featurette">
        <div class="col-md-7 col-md-push-5">
          <h2 class="featurette-heading">Oh yeah, it's that good. <span class="text-muted">See for yourself.</span></h2>
          <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
        </div>
        <div class="col-md-5 col-md-pull-7">
          <img class="featurette-image img-responsive center-block" src="https://placehold.it/500x500?text=500x500" alt="Generic placeholder image">
        </div>
      </div>

      <hr class="featurette-divider">

      <div class="row featurette">
        <div class="col-md-7">
          <h2 class="featurette-heading">And lastly, this one. <span class="text-muted">Checkmate.</span></h2>
          <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
        </div>
        <div class="col-md-5">
          <img class="featurette-image img-responsive center-block" src="https://placehold.it/500x500?text=500x500" alt="Generic placeholder image">
        </div>
      </div>

      <hr class="featurette-divider">

      /END THE FEATURETTES -->

      <?php $this->load->view('footer'); echo "\n"; ?>

    </div><!-- /.container -->

  </body>
  </html>