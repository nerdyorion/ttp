  <div style="width: 90%; margin: auto;">

    <div class="clearfix"></div>

    <div class="container-fluid text-center bg-grey">
      <!-- <h2>MOBILE CONTENTS</h2> -->
      <!-- <h4>What we have created</h4> -->

      <div class="row text-center">

        <ol class="breadcrumb text-left">
          <li><a href="<?php echo base_url(); ?>"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Home</a></li>
          <!-- <li><a href="#">Library</a></li> -->
          <li class="active"><?php echo $row['name']; ?></li>
        </ol>
        <div class="col-sm-12" style="border-bottom: 2px solid #feb25a; margin-bottom: 10px; ">
          <h3 class="text-left"><?php echo $row['name']; ?></h3>
        </div>

        <div class="jumbotron" style="display: inline-block; width: 100%; ">
          <div class="col-sm-6">
            <div class="thumbnail">
              <a href="<?php echo current_url();?>#" class="thumbnail link-thumbnail">
                <img src="assets/images/products/<?php echo $row['image_url']; ?>" alt="<?php echo $row['name']; ?>" class="img-responsive" title="Subscribe to <?php echo $row['name']; ?>">
              </a>
            </div>
          </div>

          <div class="col-sm-1">
          </div>

          <div class="col-sm-5">
            <a name="subscribe"></a>
            <h3 style="color: #000000;"><u>Subscribe Now!</u></h3>

            <div class="clearfix"></div>

            <?php if($error_code == 0 && !empty($error)): ?>
              <div class="alert alert-success alert-dismissable fade in">
                <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Success!</strong> <?php echo $error; ?>
              </div>
            <?php elseif($error_code == 1 && !empty($error)): ?>
              <div class="alert alert-danger alert-dismissable fade in">
                <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Error!</strong> <?php echo $error; ?>
              </div>
            <?php else: ?>
            <?php endif; ?>

            <div class="clearfix"></div>
            <?php echo form_open(base_url() . 'Products/subscribe/' . $row['id'], 'class="form-horizontal", onsubmit="return validate();"'); ?>
              <div class="form-group text-left">
                <label for="network">Network</label>
                <select class="form-control" id="network" name="network" required="required">
                  <option value="0">-- Select --</option>
                  <option value="MTN">MTN</option>
                  <option value="GLO">GLO</option>
                  <option value="AIRTEL">AIRTEL</option>
                  <option value="EMTS">ETISALAT</option>
                  <option value="NTEL">NTEL</option>
                </select>
              </div>
              <input type="hidden" name="id" value="<?php echo $row['id']; ?>" />
              <div class="form-group text-left">
                <label for="phone">Phone Number</label>
                <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone Number" required="required">
              </div>
              <div class="clearfix"></div>
              <button type="submit" class="btn btn-primary btn-lg">Confirm Subscription</button>
            </form>
          </div>
          
          <div class="clearfix"></div>

          <div class="col-sm-12 text-left">
            <h4>Description</h4>
            <p style="color: #000000; font-size: 1em;">
              <?php echo $row['description']; ?>
            </p>
            <p style="color: #000000; font-size: 1em;">
              <b>Price</b>: <del>N</del><?php echo $row['price']; ?>
            </p>
            <p style="color: #000000; font-size: 1em;">
              <b>Duration</b>: <?php echo $row['duration']; ?>
            </p>
          </div>

        </div>


      </div>

    </div>

    <div class="clearfix"></div>

  </div>


<?php $this->load->view('footer'); echo "\n"; ?>
<script type="text/javascript">
  $(document).ready(function() {
    location.href = "<?php echo current_url();?>#subscribe";
  });
</script>
</body>
</html>