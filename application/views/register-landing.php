  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <!-- <li data-target="#myCarousel" data-slide-to="0" class="active"></li> -->
    </ol>

    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <!-- <img src="assets/images/banner.png" alt="Image"> -->
        <img src="assets/images/register-banner.png" alt="Image" />
        <div class="carousel-caption">
          <h3 style="color: #f9f9f9;">Registration is free!</h3>
    
        </div>      
      </div>

    </div>
  </div>

  <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container marketing">

      <!-- Three columns of text below the carousel -->
      <div class="row" style="margin-bottom: 30px;">
        <div class="col-lg-6" style="border-right: solid 1px #337ab7;">
			<p align="center" ><img class="img-circle" src="assets/images/artist-mic.svg" alt="Register as an Artist" width="140" height="140"></p>
          <h2 align="center" style="font-size: 40px; font-weight: 500; margin-bottom: 30px;" >I am a Talent</h2>
          <p align="center" style="font-weight: lighter; font-size: 21px; margin-bottom: 30px;">Showcase your work<br/>
Take bookings<br/>
Receive payments<Br/></p>
          <p align="center" style="margin-bottom: 30px;"><a class="btn btn-default book1" href="register/artist" role="button">Sign up &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
       
        <div class="col-lg-6">
			<p align="center"> <img class="img-circle" src="assets/images/agent-cal.svg" alt="Register as an Agent" width="140" height="140"></p>
          <h2 align="center" style="font-size: 40px; font-weight: 500; margin-bottom: 30px;" >I am an Agent</h2>
          <p align="center" style="font-weight: lighter; font-size: 21px; margin-bottom: 30px;">Discover artists and submit booking requests<Br/>
Communicate with booked artists<br/>
Make secure payments</p>
            <p align="center" style="margin-bottom: 30px;"><a class="btn btn-default book1" href="register/agent" role="button">Sign up &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
      </div><!-- /.row -->

<?php $this->load->view('footer'); echo "\n"; ?>

</div><!-- /.container -->

</body>
</html>