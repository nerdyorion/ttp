  <style type="text/css">
    del:after {
      /*border-color: #F05522;*/
      border-color: #000000;
    }
    .price {
      color: #000000;
    }
  </style>
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <!-- <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li> -->
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <!-- <img src="https://placehold.it/1200x400?text=IMAGE" alt="Image"> -->
        <img src="assets/images/banner1.jpg" alt="Banner">
        <div class="carousel-caption">
          <h3>Great Tunes</h3>
          <!-- <p style="color: #111;">daily...</p> -->
          <p>daily...</p>
        </div>      
      </div>

    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>

  <div style="width: 70%; margin: auto;">

    <div class="clearfix"></div>

    <div class="container-fluid text-center bg-grey">
      <!-- <h2>MOBILE CONTENTS</h2> -->
      <h4>
        <!-- <a href="<?php // echo 'http://stagingdoubleoptin.9mobile.com.ng/?username=etisalat&serviceID=116&destinationURL=LWlf4m0m0%2B62%2BawNE254lk%2BZlVAV3acluCueR%2FCdVbM%3D&amount=5000&validity=5'; ?>">
          GOTO 9MOBILE DOUBLE OPT-IN
        </a>
        <a href="<?php // echo $double_opt_in_url . 'username=' . $sp_username . '&serviceID=' . $service_id . '&destinationURL=' . $destination_url . '&amount=' . $amount . '&validity=' . $validity; ?>"> -->
        <!-- <a href="<?php // echo 'http://doubleoptin.9mobile.com.ng/?serviceID=Atmosphere%20for%20worship&destinationURL=6xwQWyXpZxE2AdO1Bmm5GZo8%2BIpeBUmh2CnqiGDH%2B3h5hrXXtSECQ27UA2BRwBOb&username=HOLLATAGS&amount=5000&validity=5'; ?>">
          GOTO 9MOBILE DOUBLE OPT-IN
        </a> -->
      </h4>
      <div class="row text-center">
        <div class="col-sm-12" style="border-bottom: 2px solid #feb25a; margin-bottom: 10px; ">
          <h3 class="text-left">Mobile Contents</h3>
        </div>
        <?php if(empty($mobile_contents)): ?>
          <div class="col-sm-12">
            <p class="text-muted">No services yet, please check back.</p>
          </div>
        <?php else: ?>
          <?php foreach ($mobile_contents as $row): ?>
            <?php $confirm = "Click OK to continue."; 
            // $confirm = "You are about to subscribe to " . htmlspecialchars_decode($row['name']) . ", service costs " . $row['price'] . ", click OK to continue.";
            $service_code = '';
            $service_keyword = '';
            $service_code_text = strtoupper($HTTP_TELCO);
            $service_code_error = true;
            $service_keyword_error = true;

            switch ($service_code_text) {
                case 'MTN':
                    $service_code = $row['mtn_service_code'];
                    $service_keyword = $row['mtn_service_keyword'];
                    $service_code_error = $service_code == '' ? true : false;
                    $service_keyword_error = $service_keyword == '' ? true : false;
                    break;
                
                case 'AIRTEL':
                    $service_code = $row['airtel_service_code'];
                    $service_keyword = $row['airtel_service_keyword'];
                    $service_code_error = $service_code == '' ? true : false;
                    $service_keyword_error = $service_keyword == '' ? true : false;
                    break;
                
                case 'GLO':
                    $service_code = $row['glo_service_code'];
                    $service_keyword = $row['glo_service_keyword'];
                    $service_code_error = $service_code == '' ? true : false;
                    $service_keyword_error = $service_keyword == '' ? true : false;
                    break;
                
                case 'EMTS':
                    $service_code = $row['emts_service_code'];
                    $service_keyword = $row['emts_service_keyword'];
                    $service_code_error = $service_code == '' ? true : false;
                    $service_keyword_error = $service_keyword == '' ? true : false;
                    break;
                
                case 'NTEL':
                    $service_code = $row['ntel_service_code'];
                    $service_keyword = $row['ntel_service_keyword'];
                    $service_code_error = $service_code == '' ? true : false;
                    $service_keyword_error = $service_keyword == '' ? true : false;
                    break;
                
                default:
                    # code...
                    break;
            }
            if(($service_code_error == true) || ($service_keyword_error == true))
            {
              // "Service not yet availabe for selected network!!! Please check back later. Thank you.";
              // OR Header not enriched
            }
            ?><?php //echo getURL($row['name'], $row['slug'], $row['price'], $row['duration']); ?>
            <div class="col-sm-3">
              <div class="thumbnail">
                <a href="<?php echo getURL($row['name'], $row['slug'], $row['price'], $row['duration']); ?>" class="thumbnail link-thumbnail" <?php echo (!empty($HTTP_TELCO)) && (!empty($HTTP_MSISDN)) ? 'onclick="return preSubscribe(\'' . $SUBSCRIBE_API .'\', \'' . $HTTP_MSISDN .'\', \'' . $service_keyword .'\', \'' . $service_code .'\', \'' . $service_code_text .'\');"' : ''; ?>>
                  <img src="assets/images/products/<?php echo $row['image_url']; ?>" alt="<?php echo $row['name']; ?>" class="img-responsive" title="Subscribe to <?php echo $row['name']; ?>"><!-- 200x170 -->
                </a>
                <div class="caption">
                  <div style="min-height: 40px; "><h4><?php echo $row['name']; ?></h4></div>
                  <p class="price"><del>N</del><?php echo $row['price']; ?> / <?php echo $row['duration'] == '1 Day' ? 'Daily' : ($row['duration'] == '1 Week' ? 'Weekly' : ($row['duration'] == '1 Month' ? 'Monthly' : $row['duration'] )); ?></p>
                  <p><a href="<?php echo getURL($row['name'], $row['slug'], $row['price'], $row['duration']); ?>" class="btn btn-primary" role="button" <?php echo (!empty($HTTP_TELCO)) && (!empty($HTTP_MSISDN)) ? 'onclick="return preSubscribe(\'' . $SUBSCRIBE_API .'\', \'' . $HTTP_MSISDN .'\', \'' . $service_keyword .'\', \'' . $service_code .'\', \'' . $service_code_text .'\');"' : ''; ?>>Subscribe</a></p>
                </div>
              </div>
            </div>
          <?php endforeach; ?>
        <?php endif; ?>

        <?php if(count($mobile_contents) == 8): ?>
          <p class="text-center" style="clear: both;"><a href="mobile-contents" class="btn btn-default" role="button">View More &rarr;</a></p>
        <?php endif; ?>

        <div class="col-sm-12" style="border-bottom: 2px solid #feb25a; margin-bottom: 10px; ">
          <h3 class="text-left">Music / CRBT</h3>
        </div>
        <?php if(empty($music_crbt)): ?>
          <div class="col-sm-12">
            <p class="text-muted">No services yet, please check back.</p>
          </div>
        <?php else: ?>
          <?php foreach ($music_crbt as $row): ?>
            <?php $confirm = "Click OK to continue."; 
            // $confirm = "You are about to subscribe to " . htmlspecialchars_decode($row['name']) . ", service costs " . $row['price'] . ", click OK to continue.";
            $service_code = '';
            $service_keyword = '';
            $service_code_text = strtoupper($HTTP_TELCO);
            $service_code_error = true;
            $service_keyword_error = true;

            switch ($service_code_text) {
                case 'MTN':
                    $service_code = $row['mtn_service_code'];
                    $service_keyword = $row['mtn_service_keyword'];
                    $service_code_error = $service_code == '' ? true : false;
                    $service_keyword_error = $service_keyword == '' ? true : false;
                    break;
                
                case 'AIRTEL':
                    $service_code = $row['airtel_service_code'];
                    $service_keyword = $row['airtel_service_keyword'];
                    $service_code_error = $service_code == '' ? true : false;
                    $service_keyword_error = $service_keyword == '' ? true : false;
                    break;
                
                case 'GLO':
                    $service_code = $row['glo_service_code'];
                    $service_keyword = $row['glo_service_keyword'];
                    $service_code_error = $service_code == '' ? true : false;
                    $service_keyword_error = $service_keyword == '' ? true : false;
                    break;
                
                case 'EMTS':
                    $service_code = $row['emts_service_code'];
                    $service_keyword = $row['emts_service_keyword'];
                    $service_code_error = $service_code == '' ? true : false;
                    $service_keyword_error = $service_keyword == '' ? true : false;
                    break;
                
                case 'NTEL':
                    $service_code = $row['ntel_service_code'];
                    $service_keyword = $row['ntel_service_keyword'];
                    $service_code_error = $service_code == '' ? true : false;
                    $service_keyword_error = $service_keyword == '' ? true : false;
                    break;
                
                default:
                    # code...
                    break;
            }
            if(($service_code_error == true) || ($service_keyword_error == true))
            {
              // "Service not yet availabe for selected network!!! Please check back later. Thank you.";
              // OR Header not enriched
            }
            ?>
            <div class="col-sm-3">
              <div class="thumbnail">
                <a href="<?php echo getURL($row['name'], $row['slug'], $row['price'], $row['duration']); ?>" class="thumbnail link-thumbnail" <?php echo (!empty($HTTP_TELCO)) && (!empty($HTTP_MSISDN)) ? 'onclick="return preSubscribe(\'' . $SUBSCRIBE_API .'\', \'' . $HTTP_MSISDN .'\', \'' . $service_keyword .'\', \'' . $service_code .'\', \'' . $service_code_text .'\');"' : ''; ?>>
                  <img src="assets/images/products/<?php echo $row['image_url']; ?>" alt="<?php echo $row['name']; ?>" class="img-responsive" title="Subscribe to <?php echo $row['name']; ?>"><!-- 200x170 -->
                </a>
                <div class="caption">
                  <div style="min-height: 40px; "><h4><?php echo $row['name']; ?></h4></div>
                  <p class="price"><del>N</del><?php echo $row['price']; ?> / <?php echo $row['duration'] == '1 Day' ? 'Daily' : ($row['duration'] == '1 Week' ? 'Weekly' : ($row['duration'] == '1 Month' ? 'Monthly' : $row['duration'] )); ?></p>
                  <p><a href="<?php echo getURL($row['name'], $row['slug'], $row['price'], $row['duration']); ?>" class="btn btn-primary" role="button" <?php echo (!empty($HTTP_TELCO)) && (!empty($HTTP_MSISDN)) ? 'onclick="return preSubscribe(\'' . $SUBSCRIBE_API .'\', \'' . $HTTP_MSISDN .'\', \'' . $service_keyword .'\', \'' . $service_code .'\', \'' . $service_code_text .'\');"' : ''; ?>>Subscribe</a></p>
                </div>
              </div>
            </div>
          <?php endforeach; ?>
        <?php endif; ?>

        <?php if(count($music_crbt) == 8): ?>
          <p class="text-center" style="clear: both;"><a href="music-crbt" class="btn btn-default" role="button">View More &rarr;</a></p>
        <?php endif; ?>


      </div>

    </div>

    <div class="clearfix"></div>

  </div>


<?php $this->load->view('footer'); echo "\n"; ?>

<?php if(!empty($HTTP_TELCO) && !empty($HTTP_MSISDN)): ?>
<!-- MD5 -->
<!-- <script src="assets/js/md5.js"></script> -->

<script type="text/javascript">
  function preSubscribe(SUBSCRIBE_API, HTTP_MSISDN, service_keyword, service_code, service_code_text)
  {
    // category removed
    // category 1 -- Mobile Contents
    // category 2 -- Music / CRBT (does not need to call url subscribe, but we call it still to have their numbers ;) )
    // alert("SUBSCRIBE_API: " + SUBSCRIBE_API);
    // alert("HTTP_MSISDN: " + HTTP_MSISDN);
    // alert("service_keyword: " + service_keyword);
    // alert("service_code: " + service_code);
    // alert("service_code_text: " + service_code_text);

    var url = SUBSCRIBE_API + "msisdn=" + HTTP_MSISDN + "&keyword=" + service_keyword + "&shortcode=" + service_code + "&operator=" + service_code_text;

    // alert("url: " + url);

    $.get(url, function(data, status){
        // alert("Data: " + data + "\nStatus: " + status);
        // return true;
    });

    if(confirm('Click OK to continue.'))
    {
      return true;
    }
    else
    {
      return false;
    }
  }
</script>

<?php endif; ?>

</body>
</html>